package com.azserve.azframework.vaadinflow;

import static java.util.stream.Collectors.joining;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.IntStream;

import com.vaadin.flow.component.radiobutton.RadioButtonGroup;

public class FluencyGenerator {

	public static void main(final String[] args) {
		final Class<?> sourceClass = RadioButtonGroup.class;
		System.out.println("@SuppressWarnings(\"unchecked\")");
		System.out.println("public class AZ" + sourceClass.getSimpleName() + " extends " + sourceClass.getSimpleName() + " implements "
				+ Arrays.stream(sourceClass.getInterfaces()).map(ife -> "Fluent" + ife.getSimpleName()).collect(joining(", "))
				+ " {");
		for (final Method method : sourceClass.getDeclaredMethods()) {
			if (method.getName().startsWith("set")) {
				final String methodDec = method.toGenericString();
				final String methodParamsDec = methodDec.substring(methodDec.indexOf('('));
				final String[] params = methodParamsDec.substring(1, methodParamsDec.length() - 1).split(",");
				System.out.println("\tpublic AZ" + sourceClass.getSimpleName() + " with" + method.getName().substring(3) + "("
						+ IntStream.range(0, params.length)
								.mapToObj(i -> params[i] + " arg" + i)
								.collect(joining(", "))
						+ ") {");
				System.out.println("\t\tthis." + method.getName() + "("
						+ IntStream.range(0, params.length)
								.mapToObj(i -> "arg" + i)
								.collect(joining(", "))
						+ ");");
				System.out.println("\t\treturn this;");
				System.out.println("\t}");
			}
		}
		System.out.println("}");

		for (final Class<?> superInterface : sourceClass.getInterfaces()) {
			System.out.println("@SuppressWarnings(\"unchecked\")");
			System.out.println("public interface Fluent" + superInterface.getSimpleName() + "<I extends Fluent" + superInterface.getSimpleName() + "> extends " + superInterface.getName() + " {");
			for (final Method method : superInterface.getMethods()) {
				if (method.getName().startsWith("set")) {
					final String methodDec = method.toGenericString();
					final String methodParamsDec = methodDec.substring(methodDec.indexOf('('));
					final String[] params = methodParamsDec.substring(1, methodParamsDec.length() - 1).split(",");
					System.out.println("\tdefault I with" + method.getName().substring(3) + "("
							+ IntStream.range(0, params.length)
									.mapToObj(i -> params[i] + " arg" + i)
									.collect(joining(", "))
							+ ") {");
					System.out.println("\t\tthis." + method.getName() + "("
							+ IntStream.range(0, params.length)
									.mapToObj(i -> "arg" + i)
									.collect(joining(", "))
							+ ");");
					System.out.println("\t\treturn (I) this;");
					System.out.println("\t}");
				}
			}
			System.out.println("}");
		}
	}
}
