import {PolymerElement,html} from '@polymer/polymer/polymer-element.js';

class AZGeoLocation extends PolymerElement {

    static get properties() {
        return {
            highAccuracy: Boolean,
            watch: Boolean,
            timeout: Number,
            maxAge: Number
        };
    }
    static get observers() {
        return [
            '_start(highAccuracy, watch, timeout, maxAge)'
        ]
    }

    request() {
        this._start()
    }

    _start(highAccuracy, watch, timeout, maxAge) {
        if(this._id) {
            navigator.geolocation.clearWatch(this._id);
            this._id = null
        }
        if(watch) {
            let self = this;
            let pos = function (p) {
            	self.location = p;
            	self.error = null;
            	self.dispatchEvent(new CustomEvent('location', {detail: p}));
            };
            let err = function (e) {
            	self.location = null;
            	self.error = e;
            	self.dispatchEvent(new CustomEvent('error', {detail: e}));
            };
            let options = {
                enableHighAccuracy: highAccuracy,
                timeout: timeout,
                maximumAge: maxAge
            };
            this._id = navigator.geolocation.watchPosition(pos, err, options);
        }
    }

    static get is() {
          return 'az-geo-location';
    }
}

customElements.define(AZGeoLocation.is, AZGeoLocation);