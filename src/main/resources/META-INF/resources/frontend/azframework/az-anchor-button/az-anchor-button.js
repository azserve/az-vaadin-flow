const documentContainer = document.createElement('template');

documentContainer.innerHTML = `
<style>
  a {
    text-decoration: none;
    cursor: default;
  }
</style>
<dom-module id="anchor-button" theme-for="vaadin-button">
    <template>
        <style>
            :host([anchor]) {
                margin: 0px;
            }
        </style>
    </template>
</dom-module>`;

document.head.appendChild(documentContainer.content);