import { DatePickerElement } from	'@vaadin/vaadin-date-picker/src/vaadin-date-picker.js';

class AZDatePickerITElement extends DatePickerElement {

	ready() {
		super.ready();
	}

	initCustomParser() {
		let picker = this;

		const formatter = function(d) {
			 return [("0" + d.day).slice(-2), ("0" + (d.month+1)).slice(-2), d.year].join('/');
		};

		let parse = this.i18n.parseDate;
		const parser = function (dateString) {
			if (dateString.length > 0) {
				dateString = dateString.trim();
				let first = dateString.charAt(0);
				if (first == '0' && dateString.length == 1) {
					let today = new Date();
					return {
						day: today.getDate(),
						month: today.getMonth(),
						year: today.getFullYear()
					};
				}
				const sign = first == '+' ? 1 : (first == '-' ? -1 : 0);
				if (sign) {
					// + / -
					const val = parseInt(dateString);
					if (!isNaN(val)) {
						let today = new Date();
						today.setDate(today.getDate() + val);
						let res = {
							day: today.getDate(),
							month: today.getMonth(),
							year: today.getFullYear()
						};
						return res;
					}
				} else {
					// try parse as date
					const dateParts = dateString.replace(/\D/g, "/").split('\/');
					const day = parseInt(dateParts[0]);
					const month = parseInt(dateParts[1]);
					let year = parseInt(dateParts[2]);
					const today = new Date();
					if (isNaN(day)) {
						return {
							day: today.getDate(),
							month: today.getMonth(),
							year: today.getFullYear()
						};
					}
					if (year >= 0 && year < 100) {
						year = picker._year2(year,today.getFullYear() + 10);
					}
					return {
						day: day,
						month: isNaN(month) ? today.getMonth() : month - 1,
						year: isNaN(year) ? today.getFullYear() : year
					};
				}
			}
			// fallback to locale-based parsing
			return parse(dateString);
		};

		queueMicrotask(() => {
			this.i18n = Object.assign({}, this.i18n, {
				formatDate: formatter,
            	parseDate: parser
            });
		});
	}

	_year2(year,thresholdYear) {
		if (year >= 0 && year < 100) {
			const c = thresholdYear % 100;
			if (year <= c) {
				// 16 + 2017 - 17 = 2016
				return year + thresholdYear - c;
			}
			return year + thresholdYear - c - 100;
		}
		return year;
	}

	// overriding this method to make tab key behaving like enter key (commit value)
	_eventKey(e) {
		let key = super._eventKey(e);
		if (key == 'tab') {
			return 'enter';
		}
		return key;
	}

	static get is() {
		return 'az-date-picker-it';
	}
}
customElements.define(AZDatePickerITElement.is, AZDatePickerITElement);