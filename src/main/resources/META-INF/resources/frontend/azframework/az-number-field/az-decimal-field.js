import { TextFieldElement } from  '@vaadin/vaadin-text-field/src/vaadin-text-field.js';

class AZDecimalFieldElement extends TextFieldElement {
	static get is() {
          return 'az-decimal-field';
    }

	ready() {
      super.ready();
      this.inputElement.setAttribute('inputmode','numeric');
	  this.inputElement.style.textAlign = 'right';
	  this.inputElement.style["-webkit-mask-image"] = 'none';
	}
}
customElements.define(AZDecimalFieldElement.is, AZDecimalFieldElement);