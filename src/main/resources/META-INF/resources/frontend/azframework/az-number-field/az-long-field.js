import { TextFieldElement } from  '@vaadin/vaadin-text-field/src/vaadin-text-field.js';

class AZLongFieldElement extends TextFieldElement {
	static get is() {
          return 'az-long-field';
    }

	ready() {
      super.ready();
      this.inputElement.setAttribute('inputMode','numeric');
	  this.inputElement.style.textAlign = 'right';
	  this.inputElement.style["-webkit-mask-image"] = 'none';
	}
}
customElements.define(AZLongFieldElement.is, AZLongFieldElement);