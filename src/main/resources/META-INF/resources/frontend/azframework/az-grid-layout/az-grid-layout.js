import { LitElement, html, css, unsafeCSS } from 'lit-element';

class AZGridLayout extends LitElement {

	static get properties() {
		return {}
	}

	constructor() {
		super();
	}

	static get styles() {
		const maxColRows = 12;
		const arr = [ ...Array(maxColRows) ].map((_, i) => i + 1);
		const result = css`
			:host {
				display: grid;
				box-sizing: border-box;
			}
			:host([theme~='spacing']) {
				gap: var(--lumo-space-m);
			}
			:host([theme~='padding']) {
				padding: var(--lumo-space-m);
			}
			:host([theme~='margin']) {
				margin: var(--lumo-space-m);
			}
			::slotted(.az-fcp) {
				grid-column: 1 / -1;
			}
			${unsafeCSS(arr.map(n => `
			:host([cols="${n}"]) { grid-template-columns: repeat(${n}, minmax(0, 1fr)); }
			:host([rows="${n}"]) { grid-template-rows: repeat(${n}, minmax(0, 1fr)); }
			::slotted(.az-cp-${n}) { grid-column: span ${n} / span ${n}; }
			::slotted(._row-span-${n}) { grid-row: span ${n} / span ${n}; }
			`).join(''))}
			::slotted(.az-h) {
				display: none;
			}
			@media (max-width: 639px) {
				::slotted(.\\30\\:az-h) {
					display: none;
				}
			}
			@media (min-width: 640px) {
				${unsafeCSS(arr.map(n => `
				:host([cols-sm="${n}"]) { grid-template-columns: repeat(${n}, minmax(0, 1fr)); }
				::slotted(.sm\\:az-cp-${n}) { grid-column: span ${n} / span ${n}; }
				::slotted(.sm\\:az-cs-${n}) { grid-column-start: ${n}; }
				::slotted(.sm\\:az-ce-${n}) { grid-column-end: ${n}; }
				::slotted(.sm\\:az-rs-${n}) { grid-row-start: ${n}; }
				::slotted(.sm\\:az-re-${n}) { grid-row-end: ${n}; }
				`).join(''))}
			}
			@media (min-width: 640px) and (max-width: 767px) {
				::slotted(.sm\\:az-h) { display: none; }
				${unsafeCSS(arr.map(n => `
				`).join(''))}
			}
			@media (min-width: 768px) {
				${unsafeCSS(arr.map(n => `
				:host([cols-md="${n}"]) { grid-template-columns: repeat(${n}, minmax(0, 1fr)); }
				::slotted(.md\\:az-cp-${n}) { grid-column: span ${n} / span ${n}; }
				::slotted(.md\\:az-cs-${n}) { grid-column-start: ${n}; }
				::slotted(.md\\:az-ce-${n}) { grid-column-end: ${n}; }
				::slotted(.md\\:az-rs-${n}) { grid-row-start: ${n}; }
				::slotted(.md\\:az-re-${n}) { grid-row-end: ${n}; }
				`).join(''))}
			}
			@media (min-width: 768px) and (max-width: 1023px) {
				::slotted(.md\\:az-h) { display: none; }
				${unsafeCSS(arr.map(n => `
				`).join(''))}
			}
			@media (min-width: 1024px) {
				${unsafeCSS(arr.map(n => `
				:host([cols-lg="${n}"]) { grid-template-columns: repeat(${n}, minmax(0, 1fr)); }
				::slotted(.lg\\:az-cp-${n}) { grid-column: span ${n} / span ${n}; }
				::slotted(.lg\\:az-cs-${n}) { grid-column-start: ${n}; }
				::slotted(.lg\\:az-ce-${n}) { grid-column-end: ${n}; }
				::slotted(.lg\\:az-rs-${n}) { grid-row-start: ${n}; }
				::slotted(.lg\\:az-re-${n}) { grid-row-end: ${n}; }
				`).join(''))}
			}
			@media (min-width: 1024px) and (max-width: 1279px) {
				::slotted(.lg\\:az-h) { display: none; }
				${unsafeCSS(arr.map(n => `
				`).join(''))}
			}
			@media (min-width: 1280px) {
				${unsafeCSS(arr.map(n => `
				:host([cols-xl="${n}"]) { grid-template-columns: repeat(${n}, minmax(0, 1fr)); }
				::slotted(.xl\\:az-cp-${n}) { grid-column: span ${n} / span ${n}; }
				::slotted(.xl\\:az-cs-${n}) { grid-column-start: ${n}; }
				::slotted(.xl\\:az-ce-${n}) { grid-column-end: ${n}; }
				::slotted(.xl\\:az-rs-${n}) { grid-row-start: ${n}; }
				::slotted(.xl\\:az-re-${n}) { grid-row-end: ${n}; }
				`).join(''))}
			}
			@media (min-width: 1280px) and (max-width: 1535px) {
				::slotted(.xl\\:az-h) { display: none; }
				${unsafeCSS(arr.map(n => `
				`).join(''))}
			}
			@media (min-width: 1536px) {
				${unsafeCSS(arr.map(n => `
				:host([cols-2xl="${n}"]) { grid-template-columns: repeat(${n}, minmax(0, 1fr)); }
				::slotted(.\\32xl\\:az-cp-${n}) { grid-column: span ${n} / span ${n}; }
				::slotted(.\\32xl\\:az-cs-${n}) { grid-column-start: ${n}; }
				::slotted(.\\32xl\\:az-ce-${n}) { grid-column-end: ${n}; }
				::slotted(.\\32xl\\:az-rs-${n}) { grid-row-start: ${n}; }
				::slotted(.\\32xl\\:az-re-${n}) { grid-row-end: ${n}; }
				`).join(''))}
			}
		}`;
		console.debug(result.cssText);
		return result;
	}

	render() {
		return html`
			<slot></slot>
		`;
	}
}

customElements.define('az-grid-layout', AZGridLayout);
