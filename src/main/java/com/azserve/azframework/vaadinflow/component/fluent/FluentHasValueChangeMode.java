package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.data.value.HasValueChangeMode;
import com.vaadin.flow.data.value.ValueChangeMode;

@SuppressWarnings("unchecked")
public interface FluentHasValueChangeMode<I extends FluentHasValueChangeMode<I>>
		extends HasValueChangeMode {

	default I withValueChangeMode(final ValueChangeMode valueChangeMode) {
		this.setValueChangeMode(valueChangeMode);
		return (I) this;
	}

	default I withValueChangeTimeout(final int valueChangeTimeout) {
		this.setValueChangeTimeout(valueChangeTimeout);
		return (I) this;
	}
}
