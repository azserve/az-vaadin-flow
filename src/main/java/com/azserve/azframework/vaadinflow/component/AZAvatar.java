package com.azserve.azframework.vaadinflow.component;

import com.azserve.azframework.vaadinflow.component.fluent.FluentAvatar;
import com.vaadin.flow.component.avatar.Avatar;

@SuppressWarnings("serial")
public class AZAvatar extends Avatar implements FluentAvatar<AZAvatar> {

	public AZAvatar() {
		super();
	}

	public AZAvatar(final String name, final String url) {
		super(name, url);
	}

	public AZAvatar(final String name) {
		super(name);
	}
}
