package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Focusable;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyModifier;

@SuppressWarnings("unchecked")
public interface FluentFocusable<T extends Component, I extends FluentFocusable<T, I>>
		extends Focusable<T>, FluentHasEnabled<I> {

	default I withBlurListener(final ComponentEventListener<BlurEvent<T>> listener) {
		this.addBlurListener(listener);
		return (I) this;
	}

	default I withFocusListener(final ComponentEventListener<FocusEvent<T>> listener) {
		this.addFocusListener(listener);
		return (I) this;
	}

	default I withFocusShortcut(final Key key, final KeyModifier... keyModifiers) {
		this.addFocusShortcut(key, keyModifiers);
		return (I) this;
	}

	default I withTabIndex(final int tabIndex) {
		this.setTabIndex(tabIndex);
		return (I) this;
	}
}
