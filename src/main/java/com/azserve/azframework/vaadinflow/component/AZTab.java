package com.azserve.azframework.vaadinflow.component;

import com.azserve.azframework.vaadinflow.component.fluent.FluentTab;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.tabs.Tab;

@SuppressWarnings("serial")
public class AZTab extends Tab implements FluentTab<AZTab> {

	public AZTab() {
		super();
	}

	public AZTab(final Component... components) {
		super(components);
	}

	public AZTab(final String label) {
		super(label);
	}
}
