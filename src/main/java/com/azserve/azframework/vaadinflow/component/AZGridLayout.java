package com.azserve.azframework.vaadinflow.component;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.azserve.azframework.common.lang.NumberUtils;
import com.azserve.azframework.vaadinflow.component.fluent.FluentGridComponent;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasComponents;
import com.azserve.azframework.vaadinflow.component.fluent.FluentThemableLayout;
import com.azserve.azframework.vaadinflow.theme.lumo.LumoMediaSize;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.dom.ClassList;

@Tag("az-grid-layout")
@JsModule("./azframework/az-grid-layout/az-grid-layout.js")
@SuppressWarnings("serial")
public class AZGridLayout extends Div implements
		FluentGridComponent<AZGridLayout>,
		FluentHasComponents<AZGridLayout>,
		FluentThemableLayout<AZGridLayout> {

	public static class ChildrenConstraints {

		private final List<ChildConstraint> children = new ArrayList<>();

		public ChildrenConstraints add(final Component component) {
			return this.addAt(component, -1, -1);
		}

		public ChildrenConstraints add(final Component... components) {
			for (final Component component : components) {
				this.add(component);
			}
			return this;
		}

		public ChildrenConstraints add(final Component component, final int colSpan) {
			checkValidSpan(colSpan, "colSpan");
			return this.add(component, -1, -1, -1, -1, colSpan);
		}

		public ChildrenConstraints addAt(final Component component, final int col, final int row) {
			Objects.checkIndex(col, MAX_COLS_ROWS);
			Objects.checkIndex(row, MAX_COLS_ROWS);
			return this.addAt(component, col, row, col, row);
		}

		public ChildrenConstraints addAt(final Component component, final int colStart, final int rowStart, final int colEnd, final int rowEnd) {
			Objects.checkIndex(colStart, MAX_COLS_ROWS);
			Objects.checkIndex(rowStart, MAX_COLS_ROWS);
			Objects.checkIndex(colEnd, MAX_COLS_ROWS);
			Objects.checkIndex(rowEnd, MAX_COLS_ROWS);
			return this.add(component, colStart, rowStart, colEnd + 1, rowEnd + 1, -1);
		}

		private ChildrenConstraints add(final Component component, final int colStart, final int rowStart, final int colEnd, final int rowEnd, final int colSpan) {
			this.children.add(new ChildConstraint(component, false, false, colStart + 1, rowStart + 1, colEnd + 1, rowEnd + 1, colSpan));
			return this;
		}

		public ChildrenConstraints hide(final Component component) {
			this.children.add(new ChildConstraint(component, true, false, -1, -1, -1, -1, -1));
			return this;
		}

		public ChildrenConstraints hideUntil(final Component component) {
			this.children.add(new ChildConstraint(component, true, true, -1, -1, -1, -1, -1));
			return this;
		}
	}

	private static class ChildConstraint {

		private final Component component;
		private final boolean hidden;
		private final boolean hiddenUntil;
		private final int colStart;
		private final int rowStart;
		private final int colEnd;
		private final int rowEnd;
		private final int colSpan;

		ChildConstraint(final Component component, final boolean hidden, final boolean hiddenUntil, final int colStart, final int rowStart, final int colEnd, final int rowEnd, final int colSpan) {
			this.component = component;
			this.hidden = hidden;
			this.hiddenUntil = hiddenUntil;
			this.colStart = colStart;
			this.rowStart = rowStart;
			this.colEnd = colEnd;
			this.rowEnd = rowEnd;
			this.colSpan = colSpan;
		}
	}

	private static final LumoMediaSize[] MEDIA_SIZES = LumoMediaSize.values();
	private static final int MAX_COLS_ROWS = 12;
	private static final int FULL_SPAN_VALUE = Integer.MAX_VALUE;
	private static final String COL_SPAN_DATA_KEY = "grid-layout-col-span";
	private static final String ROW_SPAN_DATA_KEY = "grid-layout-row-span";

	private final int defaultColumnCount;
	private final int[] columnCount = new int[MEDIA_SIZES.length];

	public AZGridLayout() {
		this.setSpacing(true);
		this.defaultColumnCount = -1;
	}

	public AZGridLayout(final int defaultColumnCount) {
		if (defaultColumnCount < 1 || defaultColumnCount > MAX_COLS_ROWS) {
			throw new IllegalArgumentException("Maximum " + MAX_COLS_ROWS + " columns supported in GridLayout");
		}
		this.defaultColumnCount = defaultColumnCount;
		this.setSpacing(true);
		this.getElement().setAttribute("cols", "" + defaultColumnCount);
	}

	public AZGridLayout withConstraints(final ChildrenConstraints constraints) {
		setConstraints(constraints, null);
		return this;
	}

	public AZGridLayout withConstraints(final ChildrenConstraints constraints, final LumoMediaSize... sizes) {
		for (final LumoMediaSize size : sizes) {
			setConstraints(constraints, size);
		}
		return this;
	}

	private static void setConstraints(final ChildrenConstraints constraints, final LumoMediaSize size) {
		final String prefix = size == null ? "" : size.prefix() + ":";
		for (final ChildConstraint childConstraint : constraints.children) {
			final Component component = childConstraint.component;
			component.getElement().getClassList().removeIf(c -> c.startsWith(prefix + "az-"));
			if (childConstraint.hidden) {
				if (childConstraint.hiddenUntil && size != null) {
					component.getElement().getClassList().add("0:az-h");
					for (int i = 0; i < size.ordinal(); i++) {
						component.getElement().getClassList().add(MEDIA_SIZES[i].prefix() + ":az-h");
					}
				}
				component.getElement().getClassList().add(prefix + "az-h");
			} else {
				if (childConstraint.colSpan > 0) {
					component.getElement().getClassList().add(prefix + "az-cp-" + childConstraint.colSpan);
				}
				if (childConstraint.colStart > 0) {
					component.getElement().getClassList().add(prefix + "az-cs-" + childConstraint.colStart);
				}
				if (childConstraint.colEnd > 0) {
					component.getElement().getClassList().add(prefix + "az-ce-" + childConstraint.colEnd);
				}
				if (childConstraint.rowStart > 0) {
					component.getElement().getClassList().add(prefix + "az-rs-" + childConstraint.rowStart);
				}
				if (childConstraint.rowEnd > 0) {
					component.getElement().getClassList().add(prefix + "az-re-" + childConstraint.rowEnd);
				}
			}
		}
	}

	/**
	 * Sets the number of columns only for the given page size.
	 *
	 * @param mediaSize page size
	 * @param columns number of columns (up to {@value #MAX_COLS_ROWS})
	 */
	public void setColumns(final LumoMediaSize mediaSize, final int columns) {
		requireNonNull(mediaSize);
		final int[] columnsArray = new int[MEDIA_SIZES.length];
		columnsArray[mediaSize.ordinal()] = columns;
		this.setColumns(columnsArray);
	}

	/**
	 * Sets the number of columns only for the given page sizes.
	 *
	 * @param mediaSize1 a page size
	 * @param columns1 number of columns (up to {@value #MAX_COLS_ROWS})
	 * @param mediaSize2 another page size
	 * @param columns2 number of columns (up to {@value #MAX_COLS_ROWS})
	 */
	public void setColumns(final LumoMediaSize mediaSize1, final int columns1, final LumoMediaSize mediaSize2, final int columns2) {
		requireNonNull(mediaSize1);
		requireNonNull(mediaSize2);
		final int[] columnsArray = new int[MEDIA_SIZES.length];
		columnsArray[mediaSize1.ordinal()] = columns1;
		columnsArray[mediaSize2.ordinal()] = columns2;
		this.setColumns(columnsArray);
	}

	/**
	 * Sets the number of columns only for the given page sizes.
	 *
	 * @param mediaSize1 a page size
	 * @param columns1 number of columns (up to {@value #MAX_COLS_ROWS})
	 * @param mediaSize2 another page size
	 * @param columns2 number of columns (up to {@value #MAX_COLS_ROWS})
	 * @param mediaSize3 another page size
	 * @param columns3 number of columns (up to {@value #MAX_COLS_ROWS})
	 */
	public void setColumns(final LumoMediaSize mediaSize1, final int columns1, final LumoMediaSize mediaSize2, final int columns2, final LumoMediaSize mediaSize3, final int columns3) {
		requireNonNull(mediaSize1);
		requireNonNull(mediaSize2);
		requireNonNull(mediaSize3);
		final int[] columnsArray = new int[MEDIA_SIZES.length];
		columnsArray[mediaSize1.ordinal()] = columns1;
		columnsArray[mediaSize2.ordinal()] = columns2;
		columnsArray[mediaSize3.ordinal()] = columns3;
		this.setColumns(columnsArray);
	}

	/**
	 * Sets the number of columns only for the given page sizes.
	 *
	 * @param mediaSize1 a page size
	 * @param columns1 number of columns (up to {@value #MAX_COLS_ROWS})
	 * @param mediaSize2 another page size
	 * @param columns2 number of columns (up to {@value #MAX_COLS_ROWS})
	 * @param mediaSize3 another page size
	 * @param columns3 number of columns (up to {@value #MAX_COLS_ROWS})
	 * @param mediaSize4 another page size
	 * @param columns4 number of columns (up to {@value #MAX_COLS_ROWS})
	 */
	public void setColumns(final LumoMediaSize mediaSize1, final int columns1, final LumoMediaSize mediaSize2, final int columns2, final LumoMediaSize mediaSize3, final int columns3, final LumoMediaSize mediaSize4, final int columns4) {
		requireNonNull(mediaSize1);
		requireNonNull(mediaSize2);
		requireNonNull(mediaSize3);
		requireNonNull(mediaSize4);
		final int[] columnsArray = new int[MEDIA_SIZES.length];
		columnsArray[mediaSize1.ordinal()] = columns1;
		columnsArray[mediaSize2.ordinal()] = columns2;
		columnsArray[mediaSize3.ordinal()] = columns3;
		columnsArray[mediaSize4.ordinal()] = columns4;
		this.setColumns(columnsArray);
	}

	/**
	 * Sets the number of columns for every page size
	 *
	 * @param small number of columns (up to {@value #MAX_COLS_ROWS}) for {@linkplain LumoMediaSize#SMALL}
	 * @param medium number of columns (up to {@value #MAX_COLS_ROWS}) for {@linkplain LumoMediaSize#MEDIUM}
	 * @param large number of columns (up to {@value #MAX_COLS_ROWS}) for {@linkplain LumoMediaSize#LARGE}
	 * @param xlarge number of columns (up to {@value #MAX_COLS_ROWS}) for {@linkplain LumoMediaSize#XLARGE}
	 * @param xxlarge number of columns (up to {@value #MAX_COLS_ROWS}) for {@linkplain LumoMediaSize#XXLARGE}
	 */
	public void setColumns(final int small, final int medium, final int large, final int xlarge, final int xxlarge) {
		if (small < 0 || medium < 0 || large < 0 || xlarge < 0 || xxlarge < 0) {
			throw new IllegalArgumentException("Negative number of columns");
		}
		if (small > MAX_COLS_ROWS || medium > MAX_COLS_ROWS || large > MAX_COLS_ROWS || xlarge > MAX_COLS_ROWS || xxlarge > MAX_COLS_ROWS) {
			throw new IllegalArgumentException("Maximum " + MAX_COLS_ROWS + " columns supported in GridLayout");
		}
		this.setColumns(0, small);
		this.setColumns(1, medium);
		this.setColumns(2, large);
		this.setColumns(3, xlarge);
		this.setColumns(4, xxlarge);
		this.getChildren().forEach(c -> this.applyResponsiveColspan(c));
	}

	/**
	 * @see #setColumns(int, int, int, int, int)
	 */
	public AZGridLayout withColumns(final int small, final int medium, final int large, final int xlarge, final int xxlarge) {
		this.setColumns(small, medium, large, xlarge, xxlarge);
		return this;
	}

	/**
	 * @see #setColumns(LumoMediaSize, int)
	 */
	public AZGridLayout withColumns(final LumoMediaSize mediaSize, final int columns) {
		this.setColumns(mediaSize, columns);
		return this;
	}

	/**
	 * @see #setColumns(LumoMediaSize, int, LumoMediaSize, int)
	 */
	public AZGridLayout withColumns(final LumoMediaSize mediaSize1, final int columns1, final LumoMediaSize mediaSize2, final int columns2) {
		this.setColumns(mediaSize1, columns1, mediaSize2, columns2);
		return this;
	}

	/**
	 * @see #setColumns(LumoMediaSize, int, LumoMediaSize, int, LumoMediaSize, int)
	 */
	public AZGridLayout withColumns(final LumoMediaSize mediaSize1, final int columns1, final LumoMediaSize mediaSize2, final int columns2, final LumoMediaSize mediaSize3, final int columns3) {
		this.setColumns(mediaSize1, columns1, mediaSize2, columns2, mediaSize3, columns3);
		return this;
	}

	/**
	 * @see #setColumns(LumoMediaSize, int, LumoMediaSize, int, LumoMediaSize, int, LumoMediaSize, int)
	 */
	public AZGridLayout withColumns(final LumoMediaSize mediaSize1, final int columns1, final LumoMediaSize mediaSize2, final int columns2, final LumoMediaSize mediaSize3, final int columns3, final LumoMediaSize mediaSize4, final int columns4) {
		this.setColumns(mediaSize1, columns1, mediaSize2, columns2, mediaSize3, columns3, mediaSize4, columns4);
		return this;
	}

	/**
	 * Adds the given component as children of this grid
	 * in a new row
	 *
	 * @param component the component to add
	 */
	public void addNewLine(final Component component) {
		this.add(component);
		setComponentInFirstColumn(component);
	}

	/**
	 * Adds the given component as children of this grid
	 * with the given column span
	 *
	 * @param component the component to add
	 * @param colSpan colspan of the child component
	 */
	public void add(final Component component, final int colSpan) {
		this.setColSpan(component, colSpan);
		this.add(component);
	}

	/**
	 * Adds the given component as children of this grid
	 * in a new row with the given column span
	 *
	 * @param component the component to add
	 * @param colSpan colspan of the child component
	 */
	public void addNewLine(final Component component, final int colSpan) {
		this.add(component, colSpan);
		setComponentInFirstColumn(component);
	}

	/**
	 * Adds the given component as children of this grid
	 * with the given column and row span
	 *
	 * @param component the component to add
	 * @param colSpan colspan of the child component
	 * @param rowSpan rowspan of the child component
	 */
	public void add(final Component component, final int colSpan, final int rowSpan) {
		this.setColSpan(component, colSpan);
		this.setRowSpan(component, rowSpan);
		this.add(component);
	}

	/**
	 * Adds the given component as children of this grid
	 * in a new row with the given column and row span
	 *
	 * @param component the component to add
	 * @param colSpan colspan of the child component
	 * @param rowSpan rowspan of the child component
	 */
	public void addNewLine(final Component component, final int colSpan, final int rowSpan) {
		this.add(component, colSpan, rowSpan);
		setComponentInFirstColumn(component);
	}

	/**
	 * Adds the given component as children of this grid
	 * which fills an entire row
	 *
	 * @param component the component to add
	 */
	public void addColSpanFull(final Component component) {
		this.add(component);
		this.setColSpanFull(component);
	}

	/**
	 * @see #add(Component, int)
	 */
	public AZGridLayout withChild(final Component component, final int colspan) {
		this.add(component, colspan);
		return this;
	}

	/**
	 * @see #add(Component, int, int)
	 */
	public AZGridLayout withChild(final Component component, final int colspan, final int rowspan) {
		this.add(component, colspan, rowspan);
		return this;
	}

	/**
	 * @see #addNewLine(Component)
	 */
	public AZGridLayout withChildNewLine(final Component component) {
		this.addNewLine(component);
		return this;
	}

	/**
	 * @see #addNewLine(Component, int)
	 */
	public AZGridLayout withChildNewLine(final Component component, final int colspan) {
		this.addNewLine(component, colspan);
		return this;
	}

	/**
	 * @see #addNewLine(Component, int, int)
	 */
	public AZGridLayout withChildNewLine(final Component component, final int colspan, final int rowspan) {
		this.addNewLine(component, colspan, rowspan);
		return this;
	}

	public AZGridLayout withChildColSpanFull(final Component component) {
		this.addColSpanFull(component);
		return this;
	}

	/**
	 * Set colspan of the given child, overrides previous setting
	 *
	 * @param component child component (must be an immediate child)
	 * @param colSpan colspan of the child component
	 */
	public void setColSpan(final Component component, final int colSpan) {
		checkValidSpan(colSpan, "colSpan");
		ComponentUtil.setData(component, COL_SPAN_DATA_KEY, colSpan > 1 ? Integer.valueOf(colSpan) : null);
		this.applyResponsiveColspan(component);
	}

	public void setColSpanFull(final Component component) {
		ComponentUtil.setData(component, COL_SPAN_DATA_KEY, Integer.valueOf(FULL_SPAN_VALUE));
		this.applyResponsiveColspan(component);
	}

	/**
	 * Get the default column span of the child component.
	 *
	 * @param child a child, must be immediate child of GridLayout.
	 */
	public int getColSpan(final Component child) {
		return NumberUtils.ifNull((Integer) ComponentUtil.getData(child, COL_SPAN_DATA_KEY), 1);
	}

	/**
	 * Get the default row span of the child component.
	 *
	 * @param child a child, must be immediate child of GridLayout.
	 */
	public int getRowSpan(final Component child) {
		return NumberUtils.ifNull((Integer) ComponentUtil.getData(child, ROW_SPAN_DATA_KEY), 1);
	}

	/**
	 * Set rowspan of the given child, overrides previous setting
	 *
	 * @param component child component (must be an immediate child)
	 * @param rowSpan rowspan of the child component
	 */
	public void setRowSpan(final Component component, final int rowSpan) {
		checkValidSpan(rowSpan, "rowSpan");
		final ClassList classList = component.getElement().getClassList();
		classList.removeIf(c -> c.startsWith("_row-span-"));
		if (rowSpan > 1) {
			classList.add("_row-span-" + rowSpan);
		}
		ComponentUtil.setData(component, ROW_SPAN_DATA_KEY, rowSpan > 1 ? Integer.valueOf(rowSpan) : null);
	}

	private void applyResponsiveColspan(final Component component) {
		final ClassList classList = component.getElement().getClassList();
		for (final LumoMediaSize element : MEDIA_SIZES) {
			final String mediaSizePrefix = element.prefix();
			classList.removeIf(str -> str.startsWith(mediaSizePrefix + ":az-cp-"));
		}
		classList.removeIf(str -> str.startsWith("az-cp-") || str.equals("az-fcp"));
		final int columnSpan = this.getColSpan(component);
		if (columnSpan == 1) {
			return;
		}
		if (FULL_SPAN_VALUE == columnSpan) {
			classList.add("az-fcp");
			return;
		}
		boolean allZero = true;
		if (this.defaultColumnCount > 0) {
			classList.add("az-cp-" + Math.min(columnSpan, this.defaultColumnCount));
			allZero = false;
		}
		for (int i = 0; i < MEDIA_SIZES.length; i++) {
			final String mediaSizePrefix = MEDIA_SIZES[i].prefix();
			final int mediaSizeColumnCount = this.columnCount[i];
			if (mediaSizeColumnCount > 0) {
				allZero = false;
				classList.add(mediaSizePrefix + ":az-cp-" + Math.min(columnSpan, mediaSizeColumnCount));
			}
		}
		if (allZero) {
			classList.add("az-cp-" + columnSpan);
		}
	}

	private void setColumns(final int[] columns) {
		this.setColumns(columns[0], columns[1], columns[2], columns[3], columns[4]);
	}

	private void setColumns(final int sizeIx, final int count) {
		this.columnCount[sizeIx] = count;
		if (count > 0) {
			this.getElement().setAttribute("cols-" + MEDIA_SIZES[sizeIx].prefix(), "" + count);
		} else {

			this.getElement().removeAttribute("cols-" + MEDIA_SIZES[sizeIx].prefix());
		}
	}

	private static void setComponentInFirstColumn(final Component component) {
		component.getElement().getStyle().set("grid-column-start", "1");
	}

	private static void checkValidSpan(final int span, final String description) {
		if (span < 1 || span > MAX_COLS_ROWS) {
			throw new IllegalArgumentException(description + " needs to be 1 .. " + MAX_COLS_ROWS);
		}
	}
}