package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.html.Span;

public interface FluentSpan<I extends Span & FluentSpan<I>>
		extends
		FluentComponent<I>,
		FluentHasComponents<I>,
		FluentHasSize<I>,
		FluentHasStyle<I>,
		FluentHasText<I>,
		FluentHasTheme<I>,
		FluentClickNotifier<Span, I> {

	default I withInnerHTML(final String innerHTML) {
		this.getElement().setProperty("innerHTML", innerHTML);
		return this._castAsComponent();
	}
}
