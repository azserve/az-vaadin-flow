package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.shared.HasThemeVariant;
import com.vaadin.flow.component.shared.ThemeVariant;

@SuppressWarnings("unchecked")
public interface FluentHasThemeVariant<I extends FluentHasThemeVariant<I, V>, V extends ThemeVariant>
		extends HasThemeVariant<V>, FluentHasTheme<I> {

	default I withThemeVariants(final V... variants) {
		this.addThemeVariants(variants);
		return (I) this;
	}
}
