package com.azserve.azframework.vaadinflow.component.grid;

import java.util.stream.Stream;

import com.azserve.azframework.vaadinflow.data.provider.CallbackHierarchicalDataProvider;
import com.azserve.azframework.vaadinflow.data.provider.CallbackHierarchicalDataProvider.ChildrenCountCallback;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery;
import com.vaadin.flow.function.SerializableFunction;
import com.vaadin.flow.function.SerializablePredicate;
import com.vaadin.flow.function.ValueProvider;

public class AZTreeGrid<T> extends TreeGrid<T> implements IGrid<T> {

	private static final long serialVersionUID = 5757214917465426319L;

	public AZTreeGrid() {
		super();
	}

	public AZTreeGrid(final Class<T> beanType) {
		super(beanType);
	}

	public AZTreeGrid(final HierarchicalDataProvider<T, ?> dataProvider) {
		super(dataProvider);
	}

	@Override
	public Column<T> addColumn(final ValueProvider<T, ?> valueProvider) {
		return IGrid.fixDefaultsForColumn(super.addColumn(valueProvider), valueProvider);
	}

	@Override
	public <V extends Comparable<? super V>> Column<T> addColumn(final ValueProvider<T, V> valueProvider, final String... sortingProperties) {
		return IGrid.fixDefaultsForColumn(super.addColumn(valueProvider, sortingProperties), valueProvider);
	}

	public <F> void setDataProvider(
			final boolean inMemory,
			final ChildrenCountCallback<T, F> childrenCountCallback,
			final SerializableFunction<HierarchicalQuery<T, F>, Stream<T>> childrenFetchCallback,
			final SerializablePredicate<T> hasChildrenCallback) {
		this.setDataProvider(new CallbackHierarchicalDataProvider<>(
				inMemory, childrenCountCallback, childrenFetchCallback, hasChildrenCallback));
	}
}
