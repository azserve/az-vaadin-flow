package com.azserve.azframework.vaadinflow.component.geolocation;

import com.vaadin.flow.component.HasValue;

@SuppressWarnings("serial")
public class PositionValueChangeEvent implements HasValue.ValueChangeEvent<Position> {

	private final Position oldValue;
	private final Position value;
	private final GeoLocation source;

	public PositionValueChangeEvent(final Position oldValue, final Position value, final GeoLocation source) {
		this.oldValue = oldValue;
		this.value = value;
		this.source = source;
	}

	@Override
	public HasValue<?, Position> getHasValue() {
		return this.source;
	}

	@Override
	public boolean isFromClient() {
		return true;
	}

	@Override
	public Position getOldValue() {
		return this.oldValue;
	}

	@Override
	public Position getValue() {
		return this.value;
	}
}