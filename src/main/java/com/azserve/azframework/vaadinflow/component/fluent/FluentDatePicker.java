package com.azserve.azframework.vaadinflow.component.fluent;

import java.time.LocalDate;

import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datepicker.DatePicker.DatePickerI18n;

public interface FluentDatePicker<I extends DatePicker & FluentDatePicker<I>>
		extends
		FluentCommonField<DatePicker, LocalDate, I>,
		FluentFocusable<DatePicker, I> {

	default I withAutoOpen(final boolean autoOpen) {
		final I instance = this._castAsComponent();
		instance.setAutoOpen(autoOpen);
		return instance;
	}

	default I withI18n(final DatePickerI18n i18n) {
		final I instance = this._castAsComponent();
		instance.setI18n(i18n);
		return instance;
	}

	default I withInitialPosition(final LocalDate initialPosition) {
		final I instance = this._castAsComponent();
		instance.setInitialPosition(initialPosition);
		return instance;
	}

	default I withMax(final LocalDate max) {
		final I instance = this._castAsComponent();
		instance.setMax(max);
		return instance;
	}

	default I withMin(final LocalDate min) {
		final I instance = this._castAsComponent();
		instance.setMin(min);
		return instance;
	}

	default I withWeekNumbersVisible(final boolean weekNumbersVisible) {
		final I instance = this._castAsComponent();
		instance.setWeekNumbersVisible(weekNumbersVisible);
		return instance;
	}

}
