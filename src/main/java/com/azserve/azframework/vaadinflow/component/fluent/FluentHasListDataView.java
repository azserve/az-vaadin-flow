package com.azserve.azframework.vaadinflow.component.fluent;

import java.util.Collection;

import com.vaadin.flow.data.provider.HasListDataView;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.provider.ListDataView;

@SuppressWarnings("unchecked")
public interface FluentHasListDataView<I extends FluentHasListDataView<I, T, V>, T, V extends ListDataView<T, ?>>
		extends HasListDataView<T, V> {

	default I withItems(final T... items) {
		this.setItems(items);
		return (I) this;
	}

	default I withItems(final Collection<T> items) {
		this.setItems(items);
		return (I) this;
	}

	default I withItems(final ListDataProvider<T> dataProvider) {
		this.setItems(dataProvider);
		return (I) this;
	}
}