package com.azserve.azframework.vaadinflow.component.fluent;

import java.util.Collection;
import java.util.Objects;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.dom.Element;

@SuppressWarnings("unchecked")
public interface FluentHasComponents<I extends FluentHasComponents<I>>
		extends HasComponents, FluentHasEnabled<I> {

	default I withChild(final String text) {
		this.add(text);
		return (I) this;
	}

	default I withChild(final Component component) {
		this.add(component);
		return (I) this;
	}

	default I withChildren(final Component... components) {
		this.add(components);
		return (I) this;
	}

	default I withChildren(final Collection<Component> components) {
		this.add(components);
		return (I) this;
	}

	default I withChildAsFirst(final Component component) {
		this.addComponentAsFirst(component);
		return (I) this;
	}

	default I withChildAt(final int index, final Component component) {
		this.addComponentAtIndex(index, component);
		return (I) this;
	}

	default I withChildrenAt(final int index, final Component... components) {
		Objects.requireNonNull(components, "Components array should not be null");
		if (index < 0) {
			throw new IllegalArgumentException("Cannot add components with a negative index");
		}
		final Element[] elements = new Element[components.length];
		for (int i = 0; i < components.length; i++) {
			elements[i] = components[i].getElement();
		}
		this.getElement().insertChild(index, elements);
		return (I) this;
	}
}
