package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.contextmenu.HasMenuItems;
import com.vaadin.flow.component.contextmenu.MenuItem;

@SuppressWarnings("unchecked")
public interface FluentHasMenuItems<I extends FluentHasMenuItems<I>>
		extends HasMenuItems {

	default I withItem(final Component component, final ComponentEventListener<ClickEvent<MenuItem>> clickListener) {
		this.addItem(component, clickListener);
		return (I) this;
	}

	default I withItem(final String text, final ComponentEventListener<ClickEvent<MenuItem>> clickListener) {
		this.addItem(text, clickListener);
		return (I) this;
	}
}
