package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.shared.HasAllowedCharPattern;

@SuppressWarnings("unchecked")
public interface FluentHasAllowedCharPattern<I extends FluentHasAllowedCharPattern<I>>
		extends HasAllowedCharPattern, FluentHasElement<I> {

	default I withAllowedCharPattern(final String pattern) {
		this.setAllowedCharPattern(pattern);
		return (I) this;
	}
}
