package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.icon.Icon;

public interface FluentIcon<I extends Icon & FluentIcon<I>>
		extends
		FluentComponent<I>,
		FluentHasStyle<I>,
		FluentClickNotifier<Icon, I> {

	default I withColor(final String color) {
		final I thisComponent = this._castAsComponent();
		thisComponent.setColor(color);
		return thisComponent;
	}

	default I withSize(final String size) {
		final I thisComponent = this._castAsComponent();
		thisComponent.setSize(size);
		return thisComponent;
	}

	default I withSize(final float size, final Unit unit) {
		return this.withSize(HasSize.getCssSize(size, unit));
	}
}
