package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ClickNotifier;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyModifier;

@SuppressWarnings("unchecked")
public interface FluentClickNotifier<I extends Component, S extends FluentClickNotifier<I, S>>
		extends ClickNotifier<I> {

	default S withClickListener(final ComponentEventListener<ClickEvent<I>> listener) {
		this.addClickListener(listener);
		return (S) this;
	}

	default S withSingleClickListener(final ComponentEventListener<ClickEvent<I>> listener) {
		this.addSingleClickListener(listener);
		return (S) this;
	}

	default S withDoubleClickListener(final ComponentEventListener<ClickEvent<I>> listener) {
		this.addDoubleClickListener(listener);
		return (S) this;
	}

	default S withClickShortcut(final Key key, final KeyModifier... keyModifiers) {
		this.addClickShortcut(key, keyModifiers);
		return (S) this;
	}
}
