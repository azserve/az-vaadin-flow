package com.azserve.azframework.vaadinflow.component.geolocation;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Position implements Serializable {

	private final Double latitude;
	private final Double longitude;
	private final Double altitude;
	private final Double accuracy;
	private final Double altitudeAccuracy;
	private final Double heading;
	private final Double speed;
	private final long timestamp;

	public Position(final long timestamp, final Double latitude, final Double longitude, final Double altitude, final Double accuracy, final Double altitudeAccuracy, final Double heading, final Double speed) {
		this.timestamp = timestamp;
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
		this.accuracy = accuracy;
		this.altitudeAccuracy = altitudeAccuracy;
		this.heading = heading;
		this.speed = speed;
	}

	public long getTimestamp() {
		return this.timestamp;
	}

	public Double getLatitude() {
		return this.latitude;
	}

	public Double getLongitude() {
		return this.longitude;
	}

	public Double getAltitude() {
		return this.altitude;
	}

	public Double getAccuracy() {
		return this.accuracy;
	}

	public Double getAltitudeAccuracy() {
		return this.altitudeAccuracy;
	}

	public Double getHeading() {
		return this.heading;
	}

	public Double getSpeed() {
		return this.speed;
	}

	@Override
	public String toString() {
		return "Position{" +
				"[" + this.latitude +
				";  " + this.longitude +
				"] +-" + this.accuracy +
				"; alt: " + this.altitude +
				" +-" + this.altitudeAccuracy +
				"; hdg: " + this.heading +
				"; v: " + this.speed +
				"m/s; timestamp:" + new Date(this.timestamp).toString() +
				'}';
	}
}