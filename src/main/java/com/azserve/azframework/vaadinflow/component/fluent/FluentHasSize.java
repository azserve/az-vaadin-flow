package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.Unit;

@SuppressWarnings("unchecked")
public interface FluentHasSize<I extends FluentHasSize<I>>
		extends HasSize, FluentHasElement<I> {

	default I withHeightUndefined() {
		return this.withHeight(null);
	}

	default I withHeight(final String height) {
		this.setHeight(height);
		return (I) this;
	}

	default I withHeight(final float height, final Unit unit) {
		this.setHeight(height, unit);
		return (I) this;
	}

	default I withMinHeight(final String height) {
		this.setMinHeight(height);
		return (I) this;
	}

	default I withMinHeight(final float minHeight, final Unit unit) {
		this.setMinHeight(minHeight, unit);
		return (I) this;
	}

	default I withMaxHeight(final String height) {
		this.setMaxHeight(height);
		return (I) this;
	}

	default I withMaxHeight(final float maxHeight, final Unit unit) {
		this.setMaxHeight(maxHeight, unit);
		return (I) this;
	}

	default I withHeightFull() {
		return this.withHeight("100%");
	}

	default I withWidthUndefined() {
		return this.withWidth(null);
	}

	default I withWidth(final String width) {
		this.setWidth(width);
		return (I) this;
	}

	default I withWidth(final float width, final Unit unit) {
		this.setWidth(width, unit);
		return (I) this;
	}

	default I withMinWidth(final String width) {
		this.setMinWidth(width);
		return (I) this;
	}

	default I withMinWidth(final float minWidth, final Unit unit) {
		this.setMinWidth(minWidth, unit);
		return (I) this;
	}

	default I withMaxWidth(final String width) {
		this.setMaxWidth(width);
		return (I) this;
	}

	default I withMaxWidth(final float maxWidth, final Unit unit) {
		this.setMaxWidth(maxWidth, unit);
		return (I) this;
	}

	default I withWidthFull() {
		return this.withWidth("100%");
	}

	default I withSizeUndefined() {
		this.setSizeUndefined();
		return (I) this;
	}

	default I withSize(final String width, final String height) {
		this.setWidth(width);
		this.setHeight(height);
		return (I) this;
	}

	default I withSize(final float width, final float height, final Unit unit) {
		this.setWidth(width, unit);
		this.setHeight(height, unit);
		return (I) this;
	}

	default I withSize(final float width, final Unit widthUnit, final float height, final Unit heightUnit) {
		this.setWidth(width, widthUnit);
		this.setHeight(height, heightUnit);
		return (I) this;
	}

	default I withSizeFull() {
		this.setSizeFull();
		return (I) this;
	}
}
