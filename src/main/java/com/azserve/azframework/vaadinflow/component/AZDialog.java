package com.azserve.azframework.vaadinflow.component;

import com.azserve.azframework.vaadinflow.component.fluent.FluentDialog;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.dialog.Dialog;

public class AZDialog extends Dialog implements FluentDialog<AZDialog> {

	private static final long serialVersionUID = 2337409836476678129L;

	public AZDialog() {
		super();
	}

	public AZDialog(final Component... components) {
		super(components);
	}
}
