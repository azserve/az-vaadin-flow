package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.customfield.CustomField;

@SuppressWarnings("rawtypes")
public interface FluentCustomField<T, I extends Component & FluentCustomField<T, I>>
		extends
		FluentComponent<I>,
		FluentFocusable<CustomField, I>,
		// FluentFocusable<CustomField<T>, I>,
		FluentHasHelper<I>,
		FluentHasLabel<I>,
		FluentHasSize<I>,
		FluentHasStyle<I>,
		FluentHasTheme<I>,
		FluentHasValueAndElement<I, ComponentValueChangeEvent<CustomField<T>, T>, T>,
		FluentHasValidation<I> {}
