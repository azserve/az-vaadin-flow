package com.azserve.azframework.vaadinflow.component.geolocation;

import java.io.Serializable;

@SuppressWarnings("serial")
public class PositionErrorEvent implements Serializable {

	// https://developer.mozilla.org/en-US/docs/Web/API/GeolocationPositionError
	public enum PositionError {

		/**
		 * Other unknown error codes
		 */
		OTHER,
		/**
		 * The acquisition of the geolocation information failed because the page didn't have the permission to do it
		 */
		PERMISSION_DENIED,
		/**
		 * The acquisition of the geolocation failed because at least one internal source of position returned an internal error
		 */
		POSITION_UNAVAILABLE,
		/**
		 * The time allowed to acquire the geolocation was reached before the information was obtained
		 */
		TIMEOUT;

		public static PositionError valueOfCode(final int code) {
			switch (code) {
				case 1:
					return PERMISSION_DENIED;
				case 2:
					return POSITION_UNAVAILABLE;
				case 3:
					return TIMEOUT;
			}
			return OTHER;
		}
	}

	private final PositionError error;
	private final String message;

	public PositionErrorEvent(final PositionError error, final String message) {
		this.error = error;
		this.message = message;
	}

	/**
	 * @return the type of occurred error.
	 */
	public PositionError getError() {
		return this.error;
	}

	/**
	 * @return an error message for debugging purposes.
	 */
	public String getMessage() {
		return this.message;
	}

	@Override
	public String toString() {
		return "PositionErrorEvent{" + this.error +
				": " + this.message + '}';
	}
}