package com.azserve.azframework.vaadinflow.component.grid;

import java.util.Objects;

import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.shared.Registration;

/**
 * Enhanced grid using an underlying {@code ArrayList} by default.
 *
 * @author filippo.ortolan
 * @since 29/11/2018
 *
 * @param <T> the bean type.
 */
public class AZGrid<T> extends Grid<T> implements IGrid<T> {

	private static final long serialVersionUID = 1170625422672011338L;

	public AZGrid() {
		super();
	}

	public AZGrid(final int pageSize) {
		super(pageSize);
	}

	public AZGrid(final Class<T> beanType, final boolean autoCreateColumns) {
		super(beanType, autoCreateColumns);
	}

	public AZGrid(final Class<T> beanType) {
		super(beanType);
	}

	@Override
	public Column<T> addColumn(final ValueProvider<T, ?> valueProvider) {
		return IGrid.fixDefaultsForColumn(super.addColumn(valueProvider), valueProvider);
	}

	@Override
	public <V extends Comparable<? super V>> Column<T> addColumn(final ValueProvider<T, V> valueProvider, final String... sortingProperties) {
		return IGrid.fixDefaultsForColumn(super.addColumn(valueProvider, sortingProperties), valueProvider);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Registration addDataProviderChangeListener(final ComponentEventListener<GridDataProviderChangeEvent<T>> listener) {
		return this.addListener(GridDataProviderChangeEvent.class,
				(ComponentEventListener) Objects.requireNonNull(listener));
	}

	@Override
	protected void onDataProviderChange() {
		super.onDataProviderChange();
		this.fireEvent(new GridDataProviderChangeEvent<>(this, this.getDataProvider()));
	}
}
