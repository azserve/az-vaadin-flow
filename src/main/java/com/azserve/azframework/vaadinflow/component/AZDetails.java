package com.azserve.azframework.vaadinflow.component;

import com.azserve.azframework.vaadinflow.component.fluent.FluentDetails;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Synchronize;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.details.Details;

@Tag("az-details")
@JsModule("./azframework/az-details/az-details.js")
public class AZDetails extends Details implements FluentDetails<AZDetails> {

	private static final long serialVersionUID = 6750194767136511423L;

	public AZDetails() {
		super();
	}

	public AZDetails(final Component summary, final Component content) {
		super(summary, content);
	}

	public AZDetails(final String summary, final Component content) {
		super(summary, content);
	}

	@Synchronize(property = "blocked", value = "blocked-changed")
	public boolean isBlocked() {
		return this.getElement().getProperty("blocked", false);
	}

	public void setBlocked(final boolean blocked) {
		this.getElement().setProperty("blocked", blocked);
	}

	public AZDetails withBlocked(final boolean blocked) {
		this.setBlocked(blocked);
		return this;
	}

	@Synchronize(property = "disableToggleOnSummaryClick", value = "disable-toggle-on-summary-click-changed")
	public boolean isDisableToggleOnSummaryClick() {
		return this.getElement().getProperty("disableToggleOnSummaryClick", false);
	}

	public void setDisableToggleOnSummaryClick(final boolean disableToggleOnSummaryClick) {
		this.getElement().setProperty("disableToggleOnSummaryClick", disableToggleOnSummaryClick);
	}

	public AZDetails withDisableToggleOnSummaryClick(final boolean disableToggleOnSummaryClick) {
		this.setDisableToggleOnSummaryClick(disableToggleOnSummaryClick);
		return this;
	}
}
