package com.azserve.azframework.vaadinflow.component;

import com.azserve.azframework.vaadinflow.component.fluent.FluentDiv;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.html.Div;

@Tag(Tag.DIV)
public class AZDiv extends Div implements FluentDiv<AZDiv> {

	private static final long serialVersionUID = 3686150639216443746L;

	public AZDiv() {
		super();
	}

	public AZDiv(final Component... components) {
		super(components);
	}

	public AZDiv(final String text) {
		super();
		this.setText(text);
	}
}
