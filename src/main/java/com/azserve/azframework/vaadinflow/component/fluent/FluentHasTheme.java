package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.HasTheme;

@SuppressWarnings("unchecked")
public interface FluentHasTheme<I extends FluentHasTheme<I>>
		extends HasTheme, FluentHasElement<I> {

	default I withThemeName(final String themeName) {
		this.addThemeName(themeName);
		return (I) this;
	}

	default I withThemeName(final String themeName, final boolean set) {
		this.getThemeNames().set(themeName, set);
		return (I) this;
	}

	default I withThemeNames(final String... themeNames) {
		this.addThemeNames(themeNames);
		return (I) this;
	}
}
