package com.azserve.azframework.vaadinflow.component;

import java.util.Optional;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.HasEnabled;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.HasText;
import com.vaadin.flow.component.HasTheme;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.Router;
import com.vaadin.flow.router.RouterLink;

@JsModule("./azframework/az-router-link-button/az-router-link-button.js")
public class AZRouterLinkButton extends Composite<RouterLink> implements HasEnabled, HasTheme, HasStyle, HasText {

	private static final long serialVersionUID = 4083872969700595196L;

	// TODO make a custom component instead of a theme override?

	private final Button button;

	public AZRouterLinkButton() {
		this.button = new Button();
		this.button.setWidthFull();
		this.button.setHeight(null);
		this.button.getElement().setAttribute("anchor", "");
		this.getElement().setAttribute("tabindex", "-1");
	}

	public AZRouterLinkButton(final String text) {
		this();
		this.button.setText(text);
	}

	public AZRouterLinkButton(final String text, final Component icon) {
		this();
		this.button.setText(text);
		this.button.setIcon(icon);
	}

	public AZRouterLinkButton(final String text, final Class<? extends Component> navigationTarget) {
		this();
		this.setText(text);
		this.setRoute(navigationTarget);
	}

	public AZRouterLinkButton(final String text, final Component icon, final Class<? extends Component> navigationTarget) {
		this();
		this.setText(text);
		this.setIcon(icon);
		this.setRoute(navigationTarget);
	}

	public <T, C extends Component & HasUrlParameter<T>> AZRouterLinkButton(final String text, final Class<? extends C> navigationTarget, final T parameter) {
		this();
		this.setText(text);
		this.setRoute(navigationTarget, parameter);
	}

	public <T, C extends Component & HasUrlParameter<T>> AZRouterLinkButton(final String text, final Component icon, final Class<? extends C> navigationTarget, final T parameter) {
		this();
		this.setText(text);
		this.setIcon(icon);
		this.setRoute(navigationTarget, parameter);
	}

	@Override
	protected RouterLink initContent() {
		final RouterLink link = super.initContent();
		link.add(this.button);
		return link;
	}

	public void setRoute(final Router router, final Class<? extends Component> navigationTarget) {
		this.getContent().setRoute(router, navigationTarget);
	}

	public <T, C extends Component & HasUrlParameter<T>> void setRoute(final Router router, final Class<? extends C> navigationTarget, final T parameter) {
		this.getContent().setRoute(router, navigationTarget, parameter);
	}

	public void setRoute(final Class<? extends Component> navigationTarget) {
		this.getContent().setRoute(navigationTarget);
	}

	public <T, C extends Component & HasUrlParameter<T>> void setRoute(final Class<? extends C> navigationTarget, final T parameter) {
		this.getContent().setRoute(navigationTarget, parameter);
	}

	public Optional<QueryParameters> getQueryParameters() {
		return this.getContent().getQueryParameters();
	}

	public void setQueryParameters(final QueryParameters queryParameters) {
		this.getContent().setQueryParameters(queryParameters);
	}

	@Override
	public String getText() {
		return this.button.getText();
	}

	@Override
	public void setText(final String text) {
		this.button.setText(text);
	}

	@Override
	public void addThemeName(final String themeName) {
		this.button.addThemeName(themeName);
	}

	@Override
	public boolean removeThemeName(final String themeName) {
		return this.button.removeThemeName(themeName);
	}

	public void addThemeVariants(final ButtonVariant... variants) {
		this.button.addThemeVariants(variants);
	}

	@Override
	public void addThemeNames(final String... themeNames) {
		this.button.addThemeNames(themeNames);
	}

	public void removeThemeVariants(final ButtonVariant... variants) {
		this.button.removeThemeVariants(variants);
	}

	@Override
	public void removeThemeNames(final String... themeNames) {
		this.button.removeThemeNames(themeNames);
	}

	public void setIcon(final Component icon) {
		this.button.setIcon(icon);
	}

	public void setIconAfterText(final boolean iconAfterText) {
		this.button.setIconAfterText(iconAfterText);
	}

	public boolean isIconAfterText() {
		return this.button.isIconAfterText();
	}

	@Override
	public void setThemeName(final String themeName) {
		this.button.setThemeName(themeName);
	}

	protected Button getButton() {
		return this.button;
	}
}
