package com.azserve.azframework.vaadinflow.component.grid;

import java.util.Arrays;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.HasOrderedComponents;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.HasStyle;

/**
 * A component which implements css Grid.
 */
public interface GridComponent extends HasOrderedComponents, HasStyle, HasSize {

	/**
	 * Enum with the possible values for the content alignment inside the
	 * layout. It correlates to the <code>align-content</code> CSS property.
	 */
	public enum AlignContentMode {

		START("start"),
		END("end"),
		CENTER("center"),
		STRETCH("stretch"),
		BETWEEN("between"),
		EVENLY("evenly");

		private final String flexValue;

		AlignContentMode(final String flexValue) {
			this.flexValue = flexValue;
		}

		String getFlexValue() {
			return this.flexValue;
		}

		static AlignContentMode toAlignContentMode(final String flexValue, final AlignContentMode defaultValue) {
			return Arrays.stream(values()).filter(
					content -> content.getFlexValue().equals(flexValue))
					.findFirst().orElse(defaultValue);
		}
	}

	/**
	 * Enum with the possible values for the component alignment inside the
	 * layout. It correlates to the <code>align-items</code> CSS property.
	 */
	public enum Alignment {

		/**
		 * Items are positioned at the beginning of the container.
		 */
		START("start"),

		/**
		 * Items are positioned at the end of the container.
		 */
		END("end"),

		/**
		 * Items are positioned at the center of the container.
		 */
		CENTER("center"),

		/**
		 * Items are stretched to fit the container.
		 */
		STRETCH("stretch"),

		/**
		 * Items are positioned at the baseline of the container.
		 */
		BASELINE("baseline"),

		/**
		 * The element inherits its parent container's align-items property, or
		 * "stretch" if it has no parent container.
		 */
		AUTO("auto");

		private final String flexValue;

		Alignment(final String flexValue) {
			this.flexValue = flexValue;
		}

		String getFlexValue() {
			return this.flexValue;
		}

		static Alignment toAlignment(final String flexValue, final Alignment defaultValue) {
			return Arrays.stream(values()).filter(
					alignment -> alignment.getFlexValue().equals(flexValue))
					.findFirst().orElse(defaultValue);
		}
	}

	/**
	 * Enum with the possible values for the way the extra space inside the
	 * layout is distributed among the components. It correlates to the
	 * <code>justify-content</code> CSS property.
	 */
	public enum JustifyContentMode {

		/**
		 * Items are positioned at the beginning of the container.
		 */
		START("start"),

		/**
		 * Items are positioned at the end of the container.
		 */
		END("end"),

		/**
		 * Items are positioned at the center of the container.
		 */
		CENTER("center"),

		/**
		 * Items are positioned with space between the lines.
		 */
		BETWEEN("space-between"),

		/**
		 * Items are positioned with space before, between, and after the lines.
		 */
		AROUND("space-around"),

		/**
		 * Items have equal space around them.
		 */
		EVENLY("space-evenly");

		private final String flexValue;

		JustifyContentMode(final String flexValue) {
			this.flexValue = flexValue;
		}

		String getFlexValue() {
			return this.flexValue;
		}

		static JustifyContentMode toJustifyContentMode(final String flexValue,
				final JustifyContentMode defaultValue) {
			return Arrays.stream(values())
					.filter(justifyContent -> justifyContent.getFlexValue()
							.equals(flexValue))
					.findFirst().orElse(defaultValue);
		}
	}

	/**
	 * Sets the default alignment to be used by all components without
	 * individual alignments inside the layout. Individual components can be
	 * aligned by using the {@link #setAlignSelf(Alignment, HasElement...)}
	 * method.
	 * <p>
	 * It effectively sets the {@code "alignItems"} style value.
	 * <p>
	 * The default alignment is {@link Alignment#STRETCH}.
	 *
	 * @param alignment
	 *            the alignment to apply to the components. Setting
	 *            <code>null</code> will reset the alignment to its default
	 */
	default void setAlignItems(final Alignment alignment) {
		if (alignment == null) {
			this.getStyle().remove("alignItems");
		} else {
			this.getStyle().set("alignItems", alignment.getFlexValue());
		}
	}

	/**
	 * Gets the default alignment used by all components without individual
	 * alignments inside the layout.
	 * <p>
	 * The default alignment is {@link Alignment#STRETCH}.
	 *
	 * @return the general alignment used by the layout, never <code>null</code>
	 */
	default Alignment getAlignItems() {
		return Alignment.toAlignment(
				this.getStyle().get("alignItems"), Alignment.STRETCH);
	}

	/**
	 * Sets an alignment for individual element container inside the layout.
	 * This individual alignment for the element container overrides any
	 * alignment set at the {@link #setAlignItems(Alignment)}.
	 * <p>
	 * It effectively sets the {@code "alignSelf"} style value.
	 * <p>
	 * The default alignment for individual components is
	 * {@link Alignment#AUTO}.
	 *
	 * @param alignment
	 *            the individual alignment for the children components. Setting
	 *            <code>null</code> will reset the alignment to its default
	 * @param elementContainers
	 *            The element containers (components) to which the individual
	 *            alignment should be set
	 */
	default void setAlignSelf(final Alignment alignment,
			final HasElement... elementContainers) {
		if (alignment == null) {
			for (final HasElement container : elementContainers) {
				container.getElement().getStyle().remove("alignSelf");
			}
		} else {
			for (final HasElement container : elementContainers) {
				container.getElement().getStyle().set(
						"alignSelf",
						alignment.getFlexValue());
			}
		}
	}

	/**
	 * Gets the individual alignment of a given element container.
	 * <p>
	 * The default alignment for individual element containers is
	 * {@link Alignment#AUTO}.
	 *
	 * @param container
	 *            The element container (component) which individual layout
	 *            should be read
	 * @return the alignment of the container, never <code>null</code>
	 */
	default Alignment getAlignSelf(final HasElement container) {
		return Alignment.toAlignment(container.getElement().getStyle()
				.get("alignSelf"), Alignment.AUTO);
	}

	/**
	 * Sets the {@link AlignContentMode} used by this layout.
	 * <p>
	 * The default align-content mode is {@link AlignContentMode#START}.
	 *
	 * @param content
	 *            the align-content mode of the layout, never
	 *            <code>null</code>
	 */
	default void setAlignContentMode(final AlignContentMode content) {
		if (content == null) {
			throw new IllegalArgumentException("The 'content' argument can not be null");
		}
		this.getElement().getStyle().set("content", content.getFlexValue());
	}

	/**
	 * Gets the current align-content mode of the layout.
	 * <p>
	 * The default align-content mode is {@link AlignContentMode#START}.
	 *
	 * @return the align-content mode used by the layout, never
	 *         <code>null</code>
	 */
	default AlignContentMode getAlignContentMode() {
		return AlignContentMode.toAlignContentMode(this.getElement().getStyle().get("align-content"), AlignContentMode.START);
	}

	/**
	 * Sets the {@link JustifyContentMode} used by this layout.
	 * <p>
	 * The default justify content mode is {@link JustifyContentMode#START}.
	 *
	 * @param justifyContentMode
	 *            the justify content mode of the layout, never
	 *            <code>null</code>
	 */
	default void setJustifyContentMode(final JustifyContentMode justifyContentMode) {
		if (justifyContentMode == null) {
			throw new IllegalArgumentException("The 'justifyContentMode' argument can not be null");
		}
		this.getElement().getStyle().set("justifyContent", justifyContentMode.getFlexValue());
	}

	/**
	 * Gets the current justify content mode of the layout.
	 * <p>
	 * The default justify content mode is {@link JustifyContentMode#START}.
	 *
	 * @return the justify content mode used by the layout, never
	 *         <code>null</code>
	 */
	default JustifyContentMode getJustifyContentMode() {
		return JustifyContentMode.toJustifyContentMode(this.getElement().getStyle().get("justifyContent"), JustifyContentMode.START);
	}

	@Override
	default void replace(final Component oldComponent, final Component newComponent) {
		Alignment alignSelf = null;
		if (oldComponent != null) {
			alignSelf = this.getAlignSelf(oldComponent);
		}
		HasOrderedComponents.super.replace(oldComponent, newComponent);
		if (newComponent != null && oldComponent != null) {
			this.setAlignSelf(alignSelf, newComponent);
		}
	}
}