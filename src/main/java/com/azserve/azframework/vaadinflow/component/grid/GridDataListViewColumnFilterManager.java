package com.azserve.azframework.vaadinflow.component.grid;

import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Predicate;

import com.azserve.azframework.common.lang.StringUtils;
import com.azserve.azframework.vaadinflow.view.Callbacks;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.HasValue.ValueChangeEvent;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.function.SerializableBiPredicate;
import com.vaadin.flow.function.ValueProvider;

/**
 * Grid utility class for managing column filters for data views in memory
 *
 * @param <T> the grid bean type
 */
public class GridDataListViewColumnFilterManager<T> {

	private final HeaderRow filterHeaderRow;
	private final Map<Column<T>, Predicate<T>> filters;
	private final AZGrid<T> grid;

	protected GridDataListViewColumnFilterManager(final AZGrid<T> grid) {
		this.grid = grid;
		this.filterHeaderRow = grid.appendHeaderRow();
		this.filters = new IdentityHashMap<>();

		this.applyFilters();
		grid.addDataProviderChangeListener(e -> this.applyFilters());
	}

	/**
	 * Install a new {@code GridDataListViewFilterManager} on the specified grid.
	 *
	 * @param <T> the grid bean type
	 * @param grid the grid
	 */
	public static <T> GridDataListViewColumnFilterManager<T> install(final AZGrid<T> grid) {
		return new GridDataListViewColumnFilterManager<>(grid);
	}

	/**
	 * Adds a text filter for the specified column.
	 *
	 * @param column the column
	 * @param valueProvider the value provider
	 * @return this filter configurator
	 */
	public GridDataListViewColumnFilterManager<T> withStringFilter(final Column<T> column, final ValueProvider<T, String> valueProvider) {
		return this.withFilter(column, new TextField(),
				(item, value) -> value.isEmpty() || StringUtils.containsIgnoreCase(valueProvider.apply(item), value));
	}

	/**
	 * Adds a filter component for the specified column.
	 *
	 * @param column the column
	 * @param filterField the filter component
	 * @param function the filter function
	 * @return this filter configurator
	 */
	public <K extends Component & HasValue<E, V>, E extends ValueChangeEvent<V>, V> GridDataListViewColumnFilterManager<T> withFilter(
			final Column<T> column, final K filterField, final SerializableBiPredicate<T, V> function) {
		this.filterHeaderRow.getCell(column).setComponent(filterField);
		filterField.getElement().getStyle().set("width", "100%");
		this.filters.put(column, t -> function.test(t, filterField.getValue()));
		Callbacks.setOnValueChange(filterField, () -> this.grid.getListDataView().refreshAll());
		return this;
	}

	/**
	 * Applies the current filters.
	 */
	public void applyFilters() {
		this.grid.getListDataView().setFilter(this::applyFilter);
	}

	private boolean applyFilter(final T item) {
		final var iterator = this.filters.entrySet().iterator();
		while (iterator.hasNext()) {
			final Entry<Column<T>, Predicate<T>> entry = iterator.next();
			if (entry.getKey().getGrid() != this.grid) {
				// column has been removed from the parent grid
				iterator.remove();
			} else if (!entry.getValue().test(item)) {
				return false;
			}
		}
		return true;
	}
}
