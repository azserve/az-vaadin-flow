package com.azserve.azframework.vaadinflow.component;

import com.azserve.azframework.vaadinflow.component.fluent.FluentHorizontalLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class AZHorizontalLayout extends HorizontalLayout implements FluentHorizontalLayout<AZHorizontalLayout> {

	private static final long serialVersionUID = -61096511349878352L;

	public AZHorizontalLayout() {
		super();
	}

	public AZHorizontalLayout(final Component... children) {
		super(children);
	}
}
