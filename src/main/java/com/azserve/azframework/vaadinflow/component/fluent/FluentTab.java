package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.TabVariant;

public interface FluentTab<I extends Tab & FluentTab<I>>
		extends
		FluentComponent<I>,
		FluentHasLabel<I>,
		FluentHasStyle<I>,
		FluentHasTheme<I>,
		// FluentHasThemeVariant<I, TabVariant>,
		FluentHasComponents<I> {

	default I withThemeVariants(final TabVariant... variants) {
		final I thisComponent = this._castAsComponent();
		thisComponent.addThemeVariants(variants);
		return thisComponent;
	}

	default I withFlexGrow(final double flexGrow) {
		final I thisComponent = this._castAsComponent();
		thisComponent.setFlexGrow(flexGrow);
		return thisComponent;
	}

	default I withSelected(final boolean selected) {
		final I thisComponent = this._castAsComponent();
		thisComponent.setSelected(selected);
		return thisComponent;
	}
}
