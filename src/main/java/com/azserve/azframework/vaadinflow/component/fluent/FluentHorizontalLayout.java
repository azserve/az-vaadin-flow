package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public interface FluentHorizontalLayout<I extends HorizontalLayout & FluentHorizontalLayout<I>>
		extends
		FluentFlexComponent<I>,
		FluentHasComponents<I>,
		FluentThemableLayout<I> {}
