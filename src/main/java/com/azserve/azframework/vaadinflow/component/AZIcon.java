package com.azserve.azframework.vaadinflow.component;

import com.azserve.azframework.vaadinflow.component.fluent.FluentIcon;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;

public class AZIcon
		extends Icon
		implements
		FluentIcon<AZIcon> {

	private static final long serialVersionUID = -6437230583811266238L;

	public AZIcon() {
		super();
	}

	public AZIcon(final String collection, final String icon) {
		super(collection, icon);
	}

	public AZIcon(final String icon) {
		super(icon);
	}

	public AZIcon(final VaadinIcon icon) {
		super(icon);
	}
}
