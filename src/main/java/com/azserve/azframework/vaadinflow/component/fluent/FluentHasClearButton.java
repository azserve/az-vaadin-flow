package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.shared.HasClearButton;

@SuppressWarnings("unchecked")
public interface FluentHasClearButton<I extends FluentHasClearButton<I>>
		extends HasClearButton, FluentHasElement<I> {

	default I withClearButtonVisible(final boolean clearButtonVisible) {
		this.setClearButtonVisible(clearButtonVisible);
		return (I) this;
	}
}
