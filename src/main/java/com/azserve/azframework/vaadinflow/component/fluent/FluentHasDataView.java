package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.DataView;
import com.vaadin.flow.data.provider.HasDataView;
import com.vaadin.flow.data.provider.InMemoryDataProvider;

@SuppressWarnings("unchecked")
public interface FluentHasDataView<I extends FluentHasDataView<I, T, F, V>, T, F, V extends DataView<T>>
		extends HasDataView<T, F, V> {

	default I withItems(final DataProvider<T, F> dataProvider) {
		this.setItems(dataProvider);
		return (I) this;
	}

	default I withItems(final InMemoryDataProvider<T> dataProvider) {
		this.setItems(dataProvider);
		return (I) this;
	}
}