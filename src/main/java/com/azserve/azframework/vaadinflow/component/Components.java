package com.azserve.azframework.vaadinflow.component;

import com.azserve.azframework.common.annotation.Nullable;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.shared.SlotUtils;

public final class Components {

	/**
	 * Gets the component in the prefix slot of the specified field.
	 *
	 * @return the prefix component of the field, or {@code null} if no prefix
	 *         component has been set
	 * @see #setPrefixComponent(Component, Component)
	 */
	public static Component getPrefixComponent(final Component hostComponent) {
		return SlotUtils.getChildInSlot(hostComponent, "prefix");
	}

	/**
	 * Adds a component into the specified field before the content, replacing
	 * any existing prefix component.
	 * <p>
	 * This is most commonly used to add a simple icon or static text into the
	 * field.
	 *
	 * @param hostComponent the component to add the prefix to
	 * @param prefixComponent
	 *            the component to set as prefix, can be {@code null} to remove existing
	 *            prefix component
	 */
	public static void setPrefixComponent(final Component hostComponent, final @Nullable Component prefixComponent) {
		setPrefixSuffixComponent(hostComponent, prefixComponent, "prefix");
	}

	/**
	 * Gets the component in the prefix slot of the specified field.
	 *
	 * @return the suffix component of the field, or {@code null} if no prefix
	 *         component has been set
	 * @see #setSuffixComponent(Component, Component)
	 */
	public static Component getSuffixComponent(final Component hostComponent) {
		return SlotUtils.getChildInSlot(hostComponent, "suffix");
	}

	/**
	 * Adds a component into the specified field after the content, replacing
	 * any existing suffix component.
	 * <p>
	 * This is most commonly used to add a simple icon or static text into the
	 * field.
	 *
	 * @param hostComponent the component to add the prefix to
	 * @param suffixComponent
	 *            the component to set as suffix, can be {@code null} to remove existing
	 *            suffix component
	 */
	public static void setSuffixComponent(final Component hostComponent, final @Nullable Component suffixComponent) {
		setPrefixSuffixComponent(hostComponent, suffixComponent, "suffix");
	}

	private static void setPrefixSuffixComponent(final Component hostComponent, final @Nullable Component prefixSuffixComponent, final String slotName) {
		SlotUtils.clearSlot(hostComponent, slotName);

		if (prefixSuffixComponent != null) {
			prefixSuffixComponent.getElement().setAttribute("slot", slotName);
			hostComponent.getElement().appendChild(prefixSuffixComponent.getElement());
		}
	}

	private Components() {
		throw new AssertionError();
	}
}
