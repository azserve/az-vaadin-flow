package com.azserve.azframework.vaadinflow.component.fluent;

import com.azserve.azframework.vaadinflow.component.grid.GridComponent;
import com.vaadin.flow.component.Component;

@SuppressWarnings("unchecked")
public interface FluentGridComponent<I extends Component & FluentGridComponent<I>>
		extends GridComponent, FluentComponent<I>, FluentHasSize<I>, FluentHasStyle<I> {

	default I withChildrenAligned(final Alignment alignment, final Component... components) {
		this.add(components);
		this.setAlignSelf(alignment, components);
		return (I) this;
	}

	default I withAlignItems(final Alignment aligment) {
		this.setAlignItems(aligment);
		return (I) this;
	}

	default I withAlignContentMode(final AlignContentMode alignContentMode) {
		this.setAlignContentMode(alignContentMode);
		return (I) this;
	}

	default I withJustifyContentMode(final JustifyContentMode justifyContentMode) {
		this.setJustifyContentMode(justifyContentMode);
		return (I) this;
	}
}
