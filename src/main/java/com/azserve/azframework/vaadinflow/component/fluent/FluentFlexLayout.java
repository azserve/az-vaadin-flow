package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.FlexLayout.ContentAlignment;
import com.vaadin.flow.component.orderedlayout.FlexLayout.FlexDirection;
import com.vaadin.flow.component.orderedlayout.FlexLayout.FlexWrap;

public interface FluentFlexLayout<I extends FlexLayout & FluentFlexLayout<I>>
		extends FluentFlexComponent<I>, FluentHasComponents<I> {

	default I withAlignContent(final ContentAlignment alignment) {
		final I component = this._castAsComponent();
		component.setAlignContent(alignment);
		return component;
	}

	default I withFlexBasis(final String width, final HasElement... elementContainers) {
		final I component = this._castAsComponent();
		component.setFlexBasis(width, elementContainers);
		return component;
	}

	default I withFlexDirection(final FlexDirection direction) {
		final I component = this._castAsComponent();
		component.setFlexDirection(direction);
		return component;
	}

	default I withFlexShrink(final double flexShrink, final HasElement... elementContainers) {
		final I component = this._castAsComponent();
		component.setFlexShrink(flexShrink, elementContainers);
		return component;
	}

	default I withFlexWrap(final FlexWrap flexWrap) {
		final I component = this._castAsComponent();
		component.setFlexWrap(flexWrap);
		return component;
	}

	default I withOrder(final int order, final HasElement elementContainer) {
		final I component = this._castAsComponent();
		component.setOrder(order, elementContainer);
		return component;
	}
}
