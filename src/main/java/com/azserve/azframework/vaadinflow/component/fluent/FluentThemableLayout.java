package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.orderedlayout.BoxSizing;
import com.vaadin.flow.component.orderedlayout.ThemableLayout;

@SuppressWarnings("unchecked")
public interface FluentThemableLayout<I extends FluentThemableLayout<I>>
		extends ThemableLayout, FluentHasElement<I> {

	default I withBoxSizing(final BoxSizing boxSizing) {
		this.setBoxSizing(boxSizing);
		return (I) this;
	}

	default I withMargin(final boolean margin) {
		this.setMargin(margin);
		return (I) this;
	}

	default I withPadding(final boolean padding) {
		this.setPadding(padding);
		return (I) this;
	}

	default I withSpacing(final boolean spacing) {
		this.setSpacing(spacing);
		return (I) this;
	}
}
