package com.azserve.azframework.vaadinflow.component.grid;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.data.provider.DataProvider;

/**
 * An event fired when a {@code DataProvider} is set for a grid.
 *
 * @see GridDataProviderChangeListener
 *
 * @param <T> the data type
 */
public class GridDataProviderChangeEvent<T> extends ComponentEvent<AZGrid<T>> {

	private static final long serialVersionUID = 5770453882692938000L;

	private final DataProvider<T, ?> dataProvider;

	/**
	 * Creates a new {@code GridDataProviderChangeEvent} event originating from the given
	 * grid.
	 *
	 * @param source the grid, not null
	 * @param dataProvider the dataprovider set, not null
	 */
	public GridDataProviderChangeEvent(final AZGrid<T> source, final DataProvider<T, ?> dataProvider) {
		super(source, false);
		this.dataProvider = dataProvider;
	}

	public DataProvider<T, ?> getDataProvider() {
		return this.dataProvider;
	}
}