package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Label;

public interface FluentLabel<I extends Label & FluentLabel<I>>
		extends
		FluentComponent<I>,
		FluentHasComponents<I>,
		FluentHasSize<I>,
		FluentHasStyle<I>,
		FluentHasText<I> {

	default I withFor(final Component component) {
		final I thisComponent = this._castAsComponent();
		thisComponent.setFor(component);
		return thisComponent;
	}

	default I withFor(final String id) {
		final I thisComponent = this._castAsComponent();
		thisComponent.setFor(id);
		return thisComponent;
	}

	default I withInnerHTML(final String innerHTML) {
		this.getElement().setProperty("innerHTML", innerHTML);
		return this._castAsComponent();
	}
}
