package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.Component;

public interface FluentCommonField<C extends Component, T, I extends Component & FluentCommonField<C, T, I>>
		extends
		FluentComponent<I>,
		FluentHasHelper<I>,
		FluentHasLabel<I>,
		FluentHasSize<I>,
		FluentHasStyle<I>,
		FluentHasTheme<I>,
		FluentHasTooltip<I>,
		FluentHasValueAndElement<I, ComponentValueChangeEvent<C, T>, T>,
		FluentHasValidation<I> {}
