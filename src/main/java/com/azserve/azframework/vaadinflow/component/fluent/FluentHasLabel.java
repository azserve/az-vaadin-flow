package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.HasLabel;

@SuppressWarnings("unchecked")
public interface FluentHasLabel<I extends FluentHasLabel<I>>
		extends HasLabel, FluentHasElement<I> {

	default I withLabel(final String label) {
		this.setLabel(label);
		return (I) this;
	}
}
