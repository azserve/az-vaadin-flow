package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.details.Details.OpenedChangeEvent;
import com.vaadin.flow.component.details.DetailsVariant;

public interface FluentDetails<I extends Details & FluentDetails<I>>
		extends
		FluentComponent<I>,
		FluentHasEnabled<I>,
		FluentHasSize<I>,
		FluentHasStyle<I>,
		FluentHasTheme<I>,
		FluentHasTooltip<I> {

	default I withSummary(final Component summary) {
		final I component = this._castAsComponent();
		component.setSummary(summary);
		return component;
	}

	default I withSummaryText(final String summary) {
		final I component = this._castAsComponent();
		component.setSummaryText(summary);
		return component;
	}

	default I withContent(final Component content) {
		final I component = this._castAsComponent();
		component.setContent(content);
		return component;
	}

	default I withChild(final Component content) {
		final I component = this._castAsComponent();
		component.addContent(content);
		return component;
	}

	default I withOpened(final boolean opened) {
		final I component = this._castAsComponent();
		component.setOpened(opened);
		return component;
	}

	default I withThemeVariants(final DetailsVariant... variants) {
		final I component = this._castAsComponent();
		component.addThemeVariants(variants);
		return component;
	}

	default I withOpenedChangeListener(final ComponentEventListener<OpenedChangeEvent> listener) {
		final I component = this._castAsComponent();
		component.addOpenedChangeListener(listener);
		return component;
	}
}
