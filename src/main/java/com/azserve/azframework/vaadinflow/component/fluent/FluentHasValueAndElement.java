package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.HasValue.ValueChangeEvent;

@SuppressWarnings("unchecked")
public interface FluentHasValueAndElement<I extends FluentHasValueAndElement<I, E, V>, E extends ValueChangeEvent<V>, V>
		extends HasValue<E, V>, FluentHasEnabled<I> {

	default I withReadOnly(final boolean readOnly) {
		this.setReadOnly(readOnly);
		return (I) this;
	}

	default I withRequiredIndicatorVisible(final boolean requiredIndicatorVisible) {
		this.setRequiredIndicatorVisible(requiredIndicatorVisible);
		return (I) this;
	}

	default I withValue(final V value) {
		this.setValue(value);
		return (I) this;
	}

	default I withValueChangeListener(final ValueChangeListener<? super ValueChangeEvent<V>> listener) {
		this.addValueChangeListener(listener);
		return (I) this;
	}
}