package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.Tabs.Orientation;
import com.vaadin.flow.component.tabs.Tabs.SelectedChangeEvent;
import com.vaadin.flow.component.tabs.TabsVariant;

public interface FluentTabs<I extends Tabs & FluentTabs<I>>
		extends
		FluentComponent<I>,
		FluentHasComponents<I>,
		FluentHasStyle<I>,
		// FluentHasThemeVariant<I, TabsVariant>,
		FluentHasTheme<I>,
		FluentHasSize<I> {

	default I withAutoselect(final boolean autoselect) {
		final I thisComponent = this._castAsComponent();
		thisComponent.setAutoselect(autoselect);
		return thisComponent;
	}

	default I withFlexGrowForEnclosedTabs(final double flexGrow) {
		final I thisComponent = this._castAsComponent();
		thisComponent.setFlexGrowForEnclosedTabs(flexGrow);
		return thisComponent;
	}

	default I withOrientation(final Orientation orientation) {
		final I thisComponent = this._castAsComponent();
		thisComponent.setOrientation(orientation);
		return thisComponent;
	}

	default I withSelectedChangeListener(final ComponentEventListener<SelectedChangeEvent> listener) {
		final I thisComponent = this._castAsComponent();
		thisComponent.addSelectedChangeListener(listener);
		return thisComponent;
	}

	default I withSelectedIndex(final int selectedIndex) {
		final I thisComponent = this._castAsComponent();
		thisComponent.setSelectedIndex(selectedIndex);
		return thisComponent;
	}

	default I withThemeVariants(final TabsVariant... variants) {
		final I thisComponent = this._castAsComponent();
		thisComponent.addThemeVariants(variants);
		return thisComponent;
	}
}
