package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.data.provider.BackEndDataProvider;
import com.vaadin.flow.data.provider.CallbackDataProvider;
import com.vaadin.flow.data.provider.HasLazyDataView;
import com.vaadin.flow.data.provider.LazyDataView;

@SuppressWarnings("unchecked")
public interface FluentHasLazyDataView<I extends FluentHasLazyDataView<I, T, F, V>, T, F, V extends LazyDataView<T>>
		extends HasLazyDataView<T, F, V> {

	default I withItems(final CallbackDataProvider.FetchCallback<T, F> fetchCallback) {
		this.setItems(fetchCallback);
		return (I) this;
	}

	default I withItems(final CallbackDataProvider.FetchCallback<T, F> fetchCallback,
			final CallbackDataProvider.CountCallback<T, F> countCallback) {
		this.setItems(fetchCallback, countCallback);
		return (I) this;
	}

	default I withItems(final BackEndDataProvider<T, F> dataProvider) {
		this.setItems(dataProvider);
		return (I) this;
	}
}