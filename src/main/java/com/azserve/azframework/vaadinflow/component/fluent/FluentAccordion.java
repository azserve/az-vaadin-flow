package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.accordion.AccordionPanel;

public interface FluentAccordion<I extends Accordion & FluentAccordion<I>>
		extends
		FluentComponent<I>,
		FluentHasSize<I> {

	public interface FluentAccordionPanel<I extends AccordionPanel & FluentAccordionPanel<I>>
			extends
			FluentComponent<I>,
			FluentHasEnabled<I>,
			FluentHasTheme<I> {}

	default I withPanel(final String summary, final Component content) {
		final I instance = this._castAsComponent();
		instance.add(summary, content);
		return instance;
	}

	default I withPanel(final FluentAccordionPanel<?> panel) {
		final I instance = this._castAsComponent();
		instance.add(panel._castAsComponent());
		return instance;
	}

	default I withPanelOpened(final FluentAccordionPanel<?> panel) {
		final I instance = this._castAsComponent();
		instance.add(panel._castAsComponent());
		instance.open(panel._castAsComponent());
		return instance;
	}

	default I withPanelOpenedAt(final int index) {
		final I instance = this._castAsComponent();
		instance.open(index);
		return instance;
	}
}
