package com.azserve.azframework.vaadinflow.component.fluent;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.FormItem;
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep;
import com.vaadin.flow.dom.ElementFactory;

public interface FluentFormLayout<I extends FormLayout & FluentFormLayout<I>>
		extends
		FluentFlexComponent<I>,
		FluentHasComponents<I> {

	public interface FluentFormItem<I extends FormItem & FluentFormItem<I>>
			extends
			FluentComponent<I>,
			FluentHasStyle<I>,
			FluentHasComponents<I> {}

	default I withResponsiveSteps(final ResponsiveStep... steps) {
		final I thisComponent = this._castAsComponent();
		thisComponent.setResponsiveSteps(steps);
		return thisComponent;
	}

	default I withResponsiveSteps(final List<ResponsiveStep> steps) {
		final I thisComponent = this._castAsComponent();
		thisComponent.setResponsiveSteps(steps);
		return thisComponent;
	}

	default I withResponsiveStep(final String minWidth, final int columns) {
		return this.withResponsiveSteps(new ResponsiveStep(minWidth, columns));
	}

	default I withResponsiveStep(final float minWidth, final Unit unit, final int columns) {
		return this.withResponsiveSteps(new ResponsiveStep(HasSize.getCssSize(minWidth, unit), columns));
	}

	default I withResponsiveSteps(final String... minWidths) {
		return this.withResponsiveSteps(IntStream.range(0, minWidths.length)
				.mapToObj(i -> new ResponsiveStep(minWidths[i], i + 1))
				.collect(Collectors.toList()));
	}

	default I withResponsiveSteps(final Unit unit, final float... minWidths) {
		return this.withResponsiveSteps(IntStream.range(0, minWidths.length)
				.mapToObj(i -> new ResponsiveStep(HasSize.getCssSize(minWidths[i], unit), i + 1))
				.collect(Collectors.toList()));
	}

	default I withChild(final Component component, final int colspan) {
		final I thisComponent = this._castAsComponent();
		thisComponent.add(component, colspan);
		return thisComponent;
	}

	default I withFormItem(final Component component, final String label, final int colspan) {
		final I thisComponent = this._castAsComponent();
		thisComponent.setColspan(thisComponent.addFormItem(component, label), colspan);
		return thisComponent;
	}

	default I withNewLine() {
		final I thisComponent = this._castAsComponent();
		thisComponent.getElement().appendChild(ElementFactory.createBr());
		return thisComponent;
	}
}
