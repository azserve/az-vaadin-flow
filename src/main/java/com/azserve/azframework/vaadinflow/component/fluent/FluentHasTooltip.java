package com.azserve.azframework.vaadinflow.component.fluent;

import java.util.function.Consumer;

import com.vaadin.flow.component.shared.HasTooltip;
import com.vaadin.flow.component.shared.Tooltip;

@SuppressWarnings("unchecked")
public interface FluentHasTooltip<I extends FluentHasTooltip<I>> extends HasTooltip {

	default I withTooltipText(final String text) {
		this.setTooltipText(text);
		return (I) this;
	}

	default I withTooltipText(final String text, final Consumer<Tooltip> tooltipConfigurator) {
		tooltipConfigurator.accept(this.setTooltipText(text));
		return (I) this;
	}
}
