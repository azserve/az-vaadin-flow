package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.HasPrefixAndSuffix;

@SuppressWarnings("unchecked")
public interface FluentHasPrefixAndSuffix<I extends FluentHasPrefixAndSuffix<I>>
		extends HasPrefixAndSuffix, FluentHasElement<I> {

	default I withPrefixComponent(final Component component) {
		this.setPrefixComponent(component);
		return (I) this;
	}

	default I withSuffixComponent(final Component component) {
		this.setSuffixComponent(component);
		return (I) this;
	}
}
