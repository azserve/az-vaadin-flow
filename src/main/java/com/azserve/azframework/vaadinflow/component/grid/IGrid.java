package com.azserve.azframework.vaadinflow.component.grid;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.common.funct.Functions;
import com.azserve.azframework.common.util.BeanProperty;
import com.azserve.azframework.vaadinflow.data.provider.DataProviderUtils;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.HasValue.ValueChangeEvent;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.GridMultiSelectionModel;
import com.vaadin.flow.component.grid.GridSelectionModel;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.data.binder.PropertyDefinition;
import com.vaadin.flow.data.binder.PropertySet;
import com.vaadin.flow.data.binder.Setter;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.SortDirection;
import com.vaadin.flow.function.SerializableComparator;
import com.vaadin.flow.function.SerializableFunction;
import com.vaadin.flow.function.ValueProvider;

public interface IGrid<T> {

	Column<T> addColumn(String propertyName);

	default <V> Column<T> addColumn(final @NonNull BeanProperty<? super T, ?, ?, V> property) {
		return this.addColumn(property.getNestedName());
	}

	<V extends Component> Column<T> addComponentColumn(ValueProvider<T, V> componentProvider);

	default <E extends ValueChangeEvent<V>, C extends Component & HasValue<E, V>, V> Column<T> addFieldColumn(
			final ValueProvider<T, C> fieldProvider,
			final SerializableFunction<T, V> getter,
			final Setter<T, V> setter) {
		return this.addFieldColumn(fieldProvider, getter, setter, Functions.nullBiConsumer());
	}

	default <E extends ValueChangeEvent<V>, C extends Component & HasValue<E, V>, V> Column<T> addFieldColumn(
			final ValueProvider<T, C> fieldProvider,
			final SerializableFunction<T, V> getter,
			final Setter<T, V> setter,
			final boolean refreshItemOnClientChange) {

		final BiConsumer<T, E> valueChangedCallback = refreshItemOnClientChange
				? (item, event) -> {
					if (event.isFromClient()) {
						this.getDataProvider().refreshItem(item);
					}
				}
				: Functions.nullBiConsumer();
		return this.addFieldColumn(fieldProvider, getter, setter, valueChangedCallback);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	default <V, E extends ValueChangeEvent<V>, C extends Component & HasValue<E, V>> Column<T> addColumn(
			final @NonNull Supplier<C> fieldProvider, final @NonNull BeanProperty<? super T, ?, ?, V> property) {
		final PropertyDefinition propertyDef;
		final PropertySet<T> propertySet = this.getPropertySet();
		final String propertyName = property.getNestedName();
		try {
			propertyDef = propertySet.getProperty(propertyName).get();
		} catch (@SuppressWarnings("unused") NoSuchElementException | IllegalArgumentException exception) {
			throw new IllegalArgumentException("Can't resolve property name '"
					+ propertyName + "' from '" + propertySet + "'");
		}

		// TODO set column key and sortable from property name and type

		return this.<E, C, V> addFieldColumn(
				item -> fieldProvider.get(),
				item -> (V) propertyDef.getGetter().apply(item),
				(item, value) -> propertyDef.getSetter().ifPresent(setter -> ((BiConsumer) setter).accept(item, value)));
	}

	default <E extends ValueChangeEvent<V>, C extends Component & HasValue<E, V>, V> Column<T> addFieldColumn(
			final ValueProvider<T, C> fieldProvider,
			final SerializableFunction<T, V> getter,
			final Setter<T, V> setter,
			final BiConsumer<T, E> valueChangedCallback) {

		return this.addComponentColumn(dto -> {
			final C field = fieldProvider.apply(dto);
			if (field != null) {
				field.setValue(getter.apply(dto));
				field.addValueChangeListener(event -> {
					final V value = event.getValue();
					setter.accept(dto, value);
					valueChangedCallback.accept(dto, event);
				});
			}
			return field;
		});
	}

	// selection

	/**
	 * Selects all items available if the selection model
	 * permits multi selection, otherwise does nothing.
	 */
	default void selectAll() {
		final GridSelectionModel<T> selectionModel = this.getSelectionModel();
		if (selectionModel instanceof GridMultiSelectionModel) {
			((GridMultiSelectionModel<T>) selectionModel).selectAll();
		}
	}

	/**
	 * Selects the given item causing other items
	 * to be deselected.
	 *
	 * @param item the item to be selected.
	 */
	default void setSelectedItem(final @Nullable T item) {
		final GridSelectionModel<T> selectionModel = this.getSelectionModel();
		if (item == null) {
			selectionModel.deselectAll();
		} else {
			this.setSelectedItems(Collections.singleton(item));
		}
	}

	/**
	 * Selects the given items causing other items
	 * to be deselected.
	 *
	 * @param items the items to be selected.
	 * @throws IllegalArgumentException if the grid selection model
	 *             is not a {@code GridMultiSelectionModel} instance and
	 *             multiple items are about to be selected.
	 */
	default void setSelectedItems(final Collection<T> items) {
		final GridSelectionModel<T> selectionModel = this.getSelectionModel();
		if (items.isEmpty()) {
			selectionModel.deselectAll();
			return;
		}
		if (selectionModel instanceof GridMultiSelectionModel) {
			final GridMultiSelectionModel<T> multiSelectionModel = (GridMultiSelectionModel<T>) selectionModel;
			multiSelectionModel.updateSelection(new HashSet<>(items), multiSelectionModel.getSelectedItems()
					.stream().filter(item -> !items.contains(item)).collect(Collectors.toCollection(HashSet::new)));
		} else if (items.size() > 1) {
			throw new IllegalArgumentException("Selection model must be a GridMultiSelectionModel instance to select multiple items");
		}
		selectionModel.select(items.iterator().next());
	}

	/**
	 * Fetches all items from the underlying data provider.
	 *
	 * @return stream on items from the underlying data provider
	 */
	default Stream<T> fetchItems() {
		return DataProviderUtils.fetchAll(this.getDataProvider());
	}

	/**
	 * Fetches all items from the underlying data provider
	 * applying the current sort orders.
	 *
	 * @return stream on sorted items from the underlying data provider
	 */
	default Stream<T> fetchSortedItems() {
		final List<GridSortOrder<T>> sortOrders = this.getSortOrder();
		final Iterator<GridSortOrder<T>> iterator = sortOrders.iterator();
		if (iterator.hasNext()) {
			GridSortOrder<T> sortOrder = iterator.next();
			Comparator<T> comparator = sortOrder.getSorted().getComparator(sortOrder.getDirection());
			while (iterator.hasNext()) {
				sortOrder = iterator.next();
				comparator = comparator.thenComparing(sortOrder.getSorted().getComparator(sortOrder.getDirection()));
			}
			return this.fetchItems().sorted(comparator);
		}
		return this.fetchItems();
	}

	// accessors

	GridSelectionModel<T> getSelectionModel();

	DataProvider<T, ?> getDataProvider();

	PropertySet<T> getPropertySet();

	List<GridSortOrder<T>> getSortOrder();

	// utility

	static <T> Column<T> fixDefaultsForColumn(final Column<T> column, final ValueProvider<T, ?> valueProvider) {
		final SerializableComparator<T> comparator = column.getComparator(SortDirection.ASCENDING);
		column.setComparator((o1, o2) -> {
			final Object v1 = valueProvider.apply(o1);
			final Object v2 = valueProvider.apply(o2);
			return v1 instanceof String || v2 instanceof String
					? String.CASE_INSENSITIVE_ORDER.compare(Objects.toString(v1, ""), Objects.toString(v2, ""))
					: comparator.compare(o1, o2);
		});
		return column;
	}
}
