package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.FlexComponent;

@SuppressWarnings("unchecked")
public interface FluentFlexComponent<I extends Component & FluentFlexComponent<I>>
		extends FlexComponent, FluentComponent<I>, FluentHasSize<I>, FluentHasStyle<I> {

	default I withChildrenAligned(final Alignment alignment, final Component... components) {
		this.add(components);
		this.setAlignSelf(alignment, components);
		return (I) this;
	}

	default I withChildrenExpanded(final Component... components) {
		this.add(components);
		this.expand(components);
		return (I) this;
	}

	default I withChildrenExpanded(final Component component, final Alignment alignment) {
		this.add(component);
		this.expand(component);
		this.setAlignSelf(alignment, component);
		return (I) this;
	}

	default I withChildrenFlexGrow(final double grow, final Component... elements) {
		this.add(elements);
		this.setFlexGrow(grow, elements);
		return (I) this;
	}

	default I withAlignItems(final Alignment aligment) {
		this.setAlignItems(aligment);
		return (I) this;
	}

	default I withJustifyContentMode(final JustifyContentMode justifyContentMode) {
		this.setJustifyContentMode(justifyContentMode);
		return (I) this;
	}
}
