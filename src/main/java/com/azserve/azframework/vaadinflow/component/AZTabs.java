package com.azserve.azframework.vaadinflow.component;

import com.azserve.azframework.vaadinflow.component.fluent.FluentTabs;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;

@SuppressWarnings("serial")
public class AZTabs extends Tabs implements FluentTabs<AZTabs> {

	public AZTabs() {
		super();
	}

	public AZTabs(final boolean autoselect, final Tab... tabs) {
		super(autoselect, tabs);
	}

	public AZTabs(final Tab... tabs) {
		super(tabs);
	}
}
