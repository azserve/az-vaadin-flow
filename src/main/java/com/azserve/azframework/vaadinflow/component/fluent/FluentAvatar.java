package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.avatar.Avatar.AvatarI18n;
import com.vaadin.flow.component.avatar.AvatarVariant;
import com.vaadin.flow.server.AbstractStreamResource;

public interface FluentAvatar<I extends Avatar & FluentAvatar<I>>
		extends
		FluentComponent<I>,
		FluentHasSize<I>,
		FluentHasStyle<I>,
		FluentHasThemeVariant<I, AvatarVariant> {

	default I withI18n(final AvatarI18n i18n) {
		final I instance = this._castAsComponent();
		instance.setI18n(i18n);
		return instance;
	}

	default I withName(final String name) {
		final I instance = this._castAsComponent();
		instance.setName(name);
		return instance;
	}

	default I withAbbreviation(final String abbr) {
		final I instance = this._castAsComponent();
		instance.setAbbreviation(abbr);
		return instance;
	}

	default I withImage(final String url) {
		final I instance = this._castAsComponent();
		instance.setImage(url);
		return instance;
	}

	default I withImageResource(final AbstractStreamResource resource) {
		final I instance = this._castAsComponent();
		instance.setImageResource(resource);
		return instance;
	}

	default I withColorIndex(final Integer colorIndex) {
		final I instance = this._castAsComponent();
		instance.setColorIndex(colorIndex);
		return instance;
	}
}
