package com.azserve.azframework.vaadinflow.component.geolocation;

import java.util.ArrayList;
import java.util.List;

import com.azserve.azframework.vaadinflow.component.geolocation.PositionErrorEvent.PositionError;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.dom.DomEvent;
import com.vaadin.flow.dom.DomEventListener;
import com.vaadin.flow.dom.DomListenerRegistration;
import com.vaadin.flow.shared.Registration;

import elemental.json.JsonObject;
import elemental.json.JsonValue;

@Tag("az-geo-location")
@JsModule("./azframework/az-geo-location/az-geo-location.js")
@SuppressWarnings("serial")
public class GeoLocation extends Component implements HasValue<PositionValueChangeEvent, Position> {

	public static final String EVENT_DETAIL_CODE = "event.detail.code";
	public static final String EVENT_DETAIL_MESSAGE = "event.detail.message";
	private static final String ATTR_HIGH_ACCURACY = "high-accuracy";
	private static final String ATTR_TIMEOUT = "timeout";
	private static final String ATTR_MAX_AGE = "max-age";
	private static final String ATTR_WATCH = "watch";

	private final List<ValueChangeListener<? super PositionValueChangeEvent>> changeListeners = new ArrayList<>();
	private final List<PositionErrorListener> errorListeners = new ArrayList<>();
	private Position lastPosition;

	public GeoLocation() {
		final DomListenerRegistration locationRegistration = this.getElement().addEventListener("location", (DomEventListener) domEvent -> {
			final Position position = new Position(
					propertyValue(domEvent, "timestamp").longValue(),
					propertyValue(domEvent, "coords.latitude"),
					propertyValue(domEvent, "coords.longitude"),
					propertyValue(domEvent, "coords.altitude"),
					propertyValue(domEvent, "coords.accuracy"),
					propertyValue(domEvent, "coords.altitudeAccuracy"),
					propertyValue(domEvent, "coords.heading"),
					propertyValue(domEvent, "coords.speed"));

			final PositionValueChangeEvent event = new PositionValueChangeEvent(this.lastPosition, position, this);
			this.lastPosition = position;
			this.changeListeners.forEach(listener -> listener.valueChanged(event));
		});
		addEventData(locationRegistration, "coords.latitude");
		addEventData(locationRegistration, "coords.longitude");
		addEventData(locationRegistration, "coords.altitude");
		addEventData(locationRegistration, "coords.accuracy");
		addEventData(locationRegistration, "coords.altitudeAccuracy");
		addEventData(locationRegistration, "coords.heading");
		addEventData(locationRegistration, "coords.speed");
		addEventData(locationRegistration, "timestamp");

		final DomListenerRegistration errorRegistration = this.getElement().addEventListener("error", event -> {
			final JsonObject eventData = event.getEventData();
			final PositionErrorEvent positionErrorEvent = new PositionErrorEvent(
					PositionError.valueOfCode((int) eventData.getNumber(EVENT_DETAIL_CODE)),
					eventData.getString(EVENT_DETAIL_MESSAGE));
			this.errorListeners.forEach(listener -> listener.onError(positionErrorEvent));
		});
		errorRegistration.addEventData(EVENT_DETAIL_CODE);
		errorRegistration.addEventData(EVENT_DETAIL_MESSAGE);
	}

	private static Double propertyValue(final DomEvent event, final String key) {
		final JsonValue jsonValue = event.getEventData().get("event.detail." + key);
		return jsonValue == null ? null : Double.valueOf(jsonValue.asNumber());
	}

	private static void addEventData(final DomListenerRegistration registration, final String key) {
		registration.addEventData("event.detail." + key);
	}

	public boolean isHighAccuracy() {
		return Boolean.parseBoolean(this.getElement().getProperty(ATTR_HIGH_ACCURACY));
	}

	/**
	 * Indicates if the application would like to receive the best possible results.
	 * <br>
	 * <p>If {@code true} and if the device is able to provide a more accurate position, it will do so.</p>
	 * <p>
	 * Note that this can result in slower response times or increased power consumption
	 * (with a GPS chip on a mobile device for example).</p>
	 * <p>On the other hand, if {@code false}, the device can take the liberty to save resources
	 * by responding more quickly and/or using less power.</p>
	 *
	 * @param highAccuracy whether to receive high accuracy results, defaults to {@code false}.
	 */
	public void setHighAccuracy(final boolean highAccuracy) {
		this.getElement().setAttribute(ATTR_HIGH_ACCURACY, highAccuracy);
	}

	public int getTimeout() {
		return this.getElement().getProperty(ATTR_TIMEOUT, 0);
	}

	public void setTimeout(final int timeout) {
		this.getElement().setAttribute(ATTR_TIMEOUT, "" + timeout);
	}

	public int getMaxAge() {
		return this.getElement().getProperty(ATTR_MAX_AGE, 0);
	}

	public void setMaxAge(final int maxAge) {
		this.getElement().setAttribute(ATTR_MAX_AGE, "" + maxAge);
	}

	public boolean isWatching() {
		return Boolean.parseBoolean(this.getElement().getProperty(ATTR_WATCH));
	}

	public void startWatching() {
		this.getElement().setAttribute(ATTR_WATCH, true);
	}

	public void stopWatching() {
		this.getElement().setAttribute(ATTR_WATCH, false);
	}

	@Override
	public Position getValue() {
		return this.lastPosition;
	}

	@Override
	public void setValue(final Position value) {
		throw new IllegalArgumentException("Not Writable");
	}

	@Override
	public Registration addValueChangeListener(final ValueChangeListener<? super PositionValueChangeEvent> listener) {
		this.changeListeners.add(listener);
		return () -> this.changeListeners.remove(listener);
	}

	public Registration addErrorListener(final PositionErrorListener listener) {
		this.errorListeners.add(listener);
		return () -> this.errorListeners.remove(listener);
	}

	@Override
	public void setReadOnly(final boolean readOnly) {
		if (!readOnly) {
			throw new IllegalArgumentException("Not Writable");
		}
	}

	@Override
	public boolean isReadOnly() {
		return true;
	}

	@Override
	public void setRequiredIndicatorVisible(final boolean requiredIndicatorVisible) {
		if (requiredIndicatorVisible) {
			throw new IllegalArgumentException("Not Writable");
		}
	}

	@Override
	public boolean isRequiredIndicatorVisible() {
		return false;
	}
}
