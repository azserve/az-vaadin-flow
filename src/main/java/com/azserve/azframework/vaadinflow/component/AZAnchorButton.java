package com.azserve.azframework.vaadinflow.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.HasEnabled;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.HasText;
import com.vaadin.flow.component.HasTheme;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.AnchorTargetValue;
import com.vaadin.flow.server.AbstractStreamResource;

@JsModule("./azframework/az-anchor-button/az-anchor-button.js")
public class AZAnchorButton extends Composite<Anchor> implements HasEnabled, HasTheme, HasStyle, HasText {

	private static final long serialVersionUID = 7144092764762850855L;

	// TODO make a custom component instead of a theme override?

	private final Button button;

	public AZAnchorButton() {
		super();
		this.button = new Button();
		this.button.setWidthFull();
		this.button.setHeight(null);
		this.button.getElement().setAttribute("anchor", "");
		this.getElement().setAttribute("tabindex", "-1");
	}

	public AZAnchorButton(final AbstractStreamResource resource, final String text) {
		this();
		this.setHref(resource);
		this.setText(text);
	}

	public AZAnchorButton(final String href, final String text) {
		this();
		this.setHref(href);
		this.setText(text);
	}

	public AZAnchorButton(final String href, final String text, final Component icon) {
		this();
		this.setHref(href);
		this.setText(text);
		this.setIcon(icon);
	}

	public AZAnchorButton(final AbstractStreamResource resource, final String text, final Component icon) {
		this();
		this.setHref(resource);
		this.setText(text);
		this.setIcon(icon);
	}

	@Override
	protected Anchor initContent() {
		final Anchor anchor = super.initContent();
		anchor.add(this.button);
		return anchor;
	}

	public void setHref(final String href) {
		this.getContent().setHref(href);
	}

	public void setHref(final AbstractStreamResource resource) {
		this.getContent().setHref(resource);
	}

	public void removeHref() {
		this.getContent().removeHref();
	}

	@Override
	public String getText() {
		return this.button.getText();
	}

	@Override
	public void setText(final String text) {
		this.button.setText(text);
	}

	@Override
	public void addThemeName(final String themeName) {
		this.button.addThemeName(themeName);
	}

	@Override
	public boolean removeThemeName(final String themeName) {
		return this.button.removeThemeName(themeName);
	}

	public void addThemeVariants(final ButtonVariant... variants) {
		this.button.addThemeVariants(variants);
	}

	@Override
	public void addThemeNames(final String... themeNames) {
		this.button.addThemeNames(themeNames);
	}

	public void removeThemeVariants(final ButtonVariant... variants) {
		this.button.removeThemeVariants(variants);
	}

	@Override
	public void removeThemeNames(final String... themeNames) {
		this.button.removeThemeNames(themeNames);
	}

	public void setIcon(final Component icon) {
		this.button.setIcon(icon);
	}

	public void setIconAfterText(final boolean iconAfterText) {
		this.button.setIconAfterText(iconAfterText);
	}

	public boolean isIconAfterText() {
		return this.button.isIconAfterText();
	}

	@Override
	public void setThemeName(final String themeName) {
		this.button.setThemeName(themeName);
	}

	public void setTarget(final String target) {
		this.getContent().setTarget(target);
	}

	public void setTarget(final AnchorTargetValue target) {
		this.getContent().setTarget(target);
	}

	public AnchorTargetValue getTargetValue() {
		return this.getContent().getTargetValue();
	}

	protected Button getButton() {
		return this.button;
	}
}
