package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;

public interface FluentButton<I extends Button & FluentButton<I>>
		extends
		FluentComponent<I>,
		FluentFocusable<Button, I>,
		FluentHasText<I>,
		FluentHasSize<I>,
		FluentHasStyle<I>,
		FluentHasAutofocus<I>,
		FluentHasIcon<I>,
		FluentHasThemeVariant<I, ButtonVariant>,
		FluentHasTooltip<I>,
		FluentClickNotifier<Button, I> {

	default I withDisableOnClick(final boolean disableOnClick) {
		final I instance = this._castAsComponent();
		instance.setDisableOnClick(disableOnClick);
		return instance;
	}
}
