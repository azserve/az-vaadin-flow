package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.HasValidation;

@SuppressWarnings("unchecked")
public interface FluentHasValidation<I extends FluentHasValidation<I>>
		extends HasValidation {

	default I withErrorMessage(final java.lang.String errorMessage) {
		this.setErrorMessage(errorMessage);
		return (I) this;
	}

	default I withInvalid(final boolean invalid) {
		this.setInvalid(invalid);
		return (I) this;
	}
}