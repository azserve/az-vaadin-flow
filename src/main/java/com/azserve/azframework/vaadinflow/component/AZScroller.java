package com.azserve.azframework.vaadinflow.component;

import com.azserve.azframework.vaadinflow.component.fluent.FluentComponent;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasSize;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasStyle;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.Scroller;

public class AZScroller
		extends Scroller
		implements
		FluentComponent<AZScroller>,
		FluentHasSize<AZScroller>,
		FluentHasStyle<AZScroller> {

	private static final long serialVersionUID = -7458333565818912814L;

	public AZScroller() {
		super();
	}

	public AZScroller(final Component content, final ScrollDirection scrollDirection) {
		super(content, scrollDirection);
	}

	public AZScroller(final Component content) {
		super(content);
	}

	public AZScroller(final ScrollDirection scrollDirection) {
		super(scrollDirection);
	}

	public AZScroller withContent(final Component content) {
		this.setContent(content);
		return this;
	}

	public AZScroller withScrollDirection(final ScrollDirection scrollDirection) {
		this.setScrollDirection(scrollDirection);
		return this;
	}
}
