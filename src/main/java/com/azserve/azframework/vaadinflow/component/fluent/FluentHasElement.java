package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.HasElement;

@SuppressWarnings("unchecked")
public interface FluentHasElement<I extends FluentHasElement<I>>
		extends HasElement {

	default I withTitle(final String title) {
		this.getElement().setAttribute("title", title);
		return (I) this;
	}

	default I withAttribute(final String attribute, final String value) {
		this.getElement().setAttribute(attribute, value);
		return (I) this;
	}

	default void scrollIntoView(final boolean alignToTop) {
		this.getElement().callJsFunction("scrollIntoView", Boolean.valueOf(alignToTop));
	}
}
