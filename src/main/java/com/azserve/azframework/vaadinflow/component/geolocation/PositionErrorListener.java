package com.azserve.azframework.vaadinflow.component.geolocation;

import java.io.Serializable;
import java.util.EventListener;

public interface PositionErrorListener extends EventListener, Serializable {

	void onError(PositionErrorEvent event);
}