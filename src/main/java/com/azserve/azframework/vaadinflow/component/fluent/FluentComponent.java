package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.Component;

@SuppressWarnings("unchecked")
public interface FluentComponent<I extends Component & FluentComponent<I>>
		extends FluentHasElement<I> {

	void setId(String id);

	void setVisible(boolean visible);

	default I withId(final String id) {
		this.setId(id);
		return (I) this;
	}

	default I withVisible(final boolean visible) {
		this.setVisible(visible);
		return (I) this;
	}

	default I withTabIndexNegative() {
		this.getElement().setAttribute("tabindex", "-1");
		return (I) this;
	}

	default I _castAsComponent() {
		return (I) this;
	}
}
