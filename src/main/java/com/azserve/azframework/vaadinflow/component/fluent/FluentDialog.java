package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.dialog.Dialog.DialogCloseActionEvent;
import com.vaadin.flow.component.dialog.Dialog.DialogResizeEvent;
import com.vaadin.flow.component.dialog.Dialog.OpenedChangeEvent;
import com.vaadin.flow.component.dialog.DialogVariant;

public interface FluentDialog<I extends Dialog & FluentDialog<I>> extends
		FluentComponent<I>,
		FluentHasComponents<I>,
		FluentHasTheme<I>,
		FluentHasSize<I> {

	default I withCloseOnEsc(final boolean closeOnEsc) {
		final I thisComponent = this._castAsComponent();
		thisComponent.setCloseOnEsc(closeOnEsc);
		return thisComponent;
	}

	default I withCloseOnOutsideClick(final boolean closeOnOutsideClick) {
		final I thisComponent = this._castAsComponent();
		thisComponent.setCloseOnOutsideClick(closeOnOutsideClick);
		return thisComponent;
	}

	default I withDraggable(final boolean draggable) {
		final I thisComponent = this._castAsComponent();
		thisComponent.setDraggable(draggable);
		return thisComponent;
	}

	default I withHeaderTitle(final String title) {
		final I thisComponent = this._castAsComponent();
		thisComponent.setHeaderTitle(title);
		return thisComponent;
	}

	default I withHeader(final Component... components) {
		final I thisComponent = this._castAsComponent();
		thisComponent.getHeader().add(components);
		return thisComponent;
	}

	default I withFooter(final Component... components) {
		final I thisComponent = this._castAsComponent();
		thisComponent.getFooter().add(components);
		return thisComponent;
	}

	default I withModal(final boolean modal) {
		final I thisComponent = this._castAsComponent();
		thisComponent.setModal(modal);
		return thisComponent;
	}

	default I withResizable(final boolean resizable) {
		final I thisComponent = this._castAsComponent();
		thisComponent.setResizable(resizable);
		return thisComponent;
	}

	default I withThemeVariants(final DialogVariant... variants) {
		final I thisComponent = this._castAsComponent();
		thisComponent.addThemeVariants(variants);
		return thisComponent;
	}

	default I withOpenedChangedListener(final ComponentEventListener<OpenedChangeEvent<Dialog>> listener) {
		final I thisComponent = this._castAsComponent();
		thisComponent.addOpenedChangeListener(listener);
		return thisComponent;
	}

	default I withDialogCloseActionListener(final ComponentEventListener<DialogCloseActionEvent> listener) {
		final I thisComponent = this._castAsComponent();
		thisComponent.addDialogCloseActionListener(listener);
		return thisComponent;
	}

	default I withResizeListener(final ComponentEventListener<DialogResizeEvent> listener) {
		final I thisComponent = this._castAsComponent();
		thisComponent.addResizeListener(listener);
		return thisComponent;
	}
}
