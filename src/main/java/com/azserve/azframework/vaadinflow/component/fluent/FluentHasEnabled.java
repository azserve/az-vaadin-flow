package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.HasEnabled;

@SuppressWarnings("unchecked")
public interface FluentHasEnabled<I extends FluentHasEnabled<I>>
		extends HasEnabled, FluentHasElement<I> {

	default I withEnabled(final boolean enabled) {
		this.setEnabled(enabled);
		return (I) this;
	}
}
