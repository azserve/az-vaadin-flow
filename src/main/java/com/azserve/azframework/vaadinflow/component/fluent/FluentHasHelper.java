package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasHelper;

@SuppressWarnings("unchecked")
public interface FluentHasHelper<I extends FluentHasHelper<I>>
		extends HasHelper, FluentHasElement<I> {

	default I withHelperComponent(final Component component) {
		this.setHelperComponent(component);
		return (I) this;
	}

	default I withHelperText(final String helperText) {
		this.setHelperText(helperText);
		return (I) this;
	}
}
