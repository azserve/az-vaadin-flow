package com.azserve.azframework.vaadinflow.component;

import com.azserve.azframework.vaadinflow.component.fluent.FluentAccordion;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.accordion.AccordionPanel;

public class AZAccordion extends Accordion implements FluentAccordion<AZAccordion> {

	private static final long serialVersionUID = -3077542987587897946L;

	public AZAccordion() {
		super();
	}

	@Override
	public AZAccordion withPanel(final String summary, final Component content) {
		this.add(summary, content);
		return this;
	}

	@Override
	public AZAccordion withPanelOpenedAt(final int index) {
		this.open(index);
		return this;
	}

	public static class AZAccordionPanel extends AccordionPanel implements FluentAccordionPanel<AZAccordionPanel> {

		private static final long serialVersionUID = -8672233947534760766L;

		public AZAccordionPanel() {
			super();
		}

		public AZAccordionPanel(final Component summary, final Component content) {
			super(summary, content);
		}

		public AZAccordionPanel(final String summary, final Component content) {
			super(summary, content);
		}
	}
}
