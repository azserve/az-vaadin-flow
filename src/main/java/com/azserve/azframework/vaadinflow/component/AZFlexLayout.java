package com.azserve.azframework.vaadinflow.component;

import com.azserve.azframework.vaadinflow.component.fluent.FluentFlexLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.FlexLayout;

public class AZFlexLayout extends FlexLayout implements FluentFlexLayout<AZFlexLayout> {

	private static final long serialVersionUID = 7888790437945087662L;

	public AZFlexLayout() {
		super();
	}

	public AZFlexLayout(final Component... children) {
		super(children);
	}
}
