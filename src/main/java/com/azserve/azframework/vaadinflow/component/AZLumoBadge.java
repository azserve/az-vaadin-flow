package com.azserve.azframework.vaadinflow.component;

import com.azserve.azframework.vaadinflow.component.fluent.FluentComponent;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasIcon;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasSize;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasStyle;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasText;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Span;

/**
 * This is the Badge component that implements Java API for Lumo Badges.
 */
@JsModule("@vaadin/vaadin-lumo-styles/badge.js")
@Tag("az-lumo-badge")
public class AZLumoBadge extends Composite<Span>
		implements
		FluentComponent<AZLumoBadge>,
		FluentHasText<AZLumoBadge>,
		FluentHasIcon<AZLumoBadge>,
		FluentHasStyle<AZLumoBadge>,
		FluentHasSize<AZLumoBadge> {

	private static final long serialVersionUID = -854369535014845890L;

	/**
	 * Badge variants
	 */
	public enum BadgeVariant {

		/**
		 * Success type, usually green
		 */
		SUCCESS("success"),
		/**
		 * Error type, usually red
		 */
		ERROR("error"),
		/**
		 * Contrast type, usually black
		 */
		CONTRAST("contrast");

		private final String variant;

		BadgeVariant(final String variant) {
			this.variant = variant;
		}

		public String getVariantName() {
			return this.variant;
		}
	}

	private final Span span = new Span();
	private String text = "";
	private Component icon;
	private boolean iconFirst;
	private String html;

	public AZLumoBadge() {
		this.span.getElement().getThemeList().add("badge");
	}

	public AZLumoBadge(final String text) {
		this();
		this.setText(text);
	}

	/**
	 * Set the text content of the badge, remove all other content.
	 */
	@Override
	public void setText(final String text) {
		this.text = text;
		this.span.setText(text);
	}

	/**
	 * Set the badge variant type. {@link BadgeVariant}
	 *
	 * @param variant the variant type or {@code null}
	 *            to restore default variant.
	 */
	public void setVariant(final BadgeVariant variant) {
		for (final BadgeVariant v : BadgeVariant.values()) {
			this.span.getElement().getThemeList().set(v.getVariantName(), variant == v);
		}
	}

	public AZLumoBadge withVariant(final BadgeVariant variant) {
		this.setVariant(variant);
		return this;
	}

	/**
	 * Set Badge to be primary, more prominent color.
	 */
	public void setPrimary(final boolean primary) {
		this.span.getElement().getThemeList().set("primary", primary);
	}

	public AZLumoBadge withPrimary(final boolean primary) {
		this.setPrimary(primary);
		return this;
	}

	/**
	 * Set Badge to be pill shaped.
	 */
	public void setPill(final boolean pill) {
		this.span.getElement().getThemeList().set("pill", pill);
	}

	public AZLumoBadge withPill(final boolean pill) {
		this.setPill(pill);
		return this;
	}

	/**
	 * Set Badge to be small. Can be combined with primary and pill.
	 *
	 */
	public void setSmall(final boolean small) {
		this.span.getElement().getThemeList().set("small", small);
	}

	public AZLumoBadge withSmall(final boolean small) {
		this.setSmall(small);
		return this;
	}

	/**
	 * Set icon to be wrapped before the text.
	 *
	 * @param icon the icon component.
	 */
	@Override
	public void setIcon(final Component icon) {
		this.setIcon(icon, true);
	}

	/**
	 * Set icon to be wrapped before or after the text.
	 *
	 * @param icon the icon component.
	 * @param first for icon to appear before text.
	 */
	public void setIcon(final Component icon, final boolean first) {
		this.span.getElement().removeAllChildren();
		this.icon = icon;
		this.iconFirst = first;
		if (first) {
			this.add(icon);
			if (this.html != null) {
				this.add(new Html(this.html));
			} else {
				this.add(new Span(this.text));
			}
		} else {
			if (this.html != null) {
				this.add(new Html(this.html));
			} else {
				this.add(new Span(this.text));
			}
			this.add(icon);
		}
	}

	/**
	 * Set the text content of the badge as html, remove all other content.
	 * <p>
	 * Note: Use carefully, html is not sanitized.
	 */
	public void setHtml(final String html) {
		this.html = html;
		this.getElement().removeAllChildren();
		if (this.iconFirst && this.icon != null) {
			this.add(this.icon);
		}
		this.add(new Html(html));
		if (this.icon != null) {
			this.add(this.icon);
		}
	}

	public AZLumoBadge withHtml(final String innerHTML) {
		this.setHtml(innerHTML);
		return this;
	}

	@Override
	protected Span initContent() {
		return this.span;
	}

	private void add(final Component comp) {
		this.span.getElement().appendChild(comp.getElement());
	}
}