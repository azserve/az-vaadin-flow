package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.HasAriaLabel;

@SuppressWarnings("unchecked")
public interface FluentHasAriaLabel<I extends FluentHasAriaLabel<I>>
		extends HasAriaLabel, FluentHasElement<I> {

	default I withAriaLabel(final String ariaLabel) {
		this.setAriaLabel(ariaLabel);
		return (I) this;
	}
}
