package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.HasStyle;

@SuppressWarnings("unchecked")
public interface FluentHasStyle<I extends FluentHasStyle<I>>
		extends HasStyle, FluentHasElement<I> {

	default I withClassName(final String className) {
		this.addClassName(className);
		return (I) this;
	}

	default I withClassNames(final String... classNames) {
		this.addClassNames(classNames);
		return (I) this;
	}

	default I withStyle(final String name, final String value) {
		this.getStyle().set(name, value);
		return (I) this;
	}
}
