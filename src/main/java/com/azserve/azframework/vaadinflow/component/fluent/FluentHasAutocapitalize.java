package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.textfield.Autocapitalize;
import com.vaadin.flow.component.textfield.HasAutocapitalize;

@SuppressWarnings("unchecked")
public interface FluentHasAutocapitalize<I extends FluentHasAutocapitalize<I>>
		extends HasAutocapitalize, FluentHasElement<I> {

	default I withAutocapitalize(final Autocapitalize autocapitalize) {
		this.setAutocapitalize(autocapitalize);
		return (I) this;
	}
}
