package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.Component;

public interface FluentCommonInputField<C extends Component, T, I extends Component & FluentCommonInputField<C, T, I>>
		extends
		FluentCommonField<C, T, I>,
		FluentHasAllowedCharPattern<I>,
		FluentHasAutofocus<I>,
		FluentHasClearButton<I>,
		FluentFocusable<C, I>,
		FluentHasPrefixAndSuffix<I> {}
