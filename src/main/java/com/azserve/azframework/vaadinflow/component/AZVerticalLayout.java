package com.azserve.azframework.vaadinflow.component;

import com.azserve.azframework.vaadinflow.component.fluent.FluentVerticalLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class AZVerticalLayout extends VerticalLayout implements FluentVerticalLayout<AZVerticalLayout> {

	private static final long serialVersionUID = -61096511349878352L;

	public AZVerticalLayout() {
		super();
	}

	public AZVerticalLayout(final Component... children) {
		super(children);
	}
}
