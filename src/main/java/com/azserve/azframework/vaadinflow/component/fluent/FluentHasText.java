package com.azserve.azframework.vaadinflow.component.fluent;

import com.vaadin.flow.component.HasText;

@SuppressWarnings("unchecked")
public interface FluentHasText<I extends FluentHasText<I>>
		extends HasText, FluentHasElement<I> {

	default I withText(final String text) {
		this.setText(text);
		return (I) this;
	}

	default I withWhiteSpace(final WhiteSpace value) {
		this.setWhiteSpace(value);
		return (I) this;
	}
}
