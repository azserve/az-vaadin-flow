package com.azserve.azframework.vaadinflow.component;

import com.azserve.azframework.vaadinflow.component.fluent.FluentLabel;
import com.vaadin.flow.component.html.Label;

public class AZLabel extends Label implements FluentLabel<AZLabel> {

	private static final long serialVersionUID = -4826123716997918343L;

	public AZLabel() {
		super();
	}

	public AZLabel(final String text) {
		super(text);
	}
}
