package com.azserve.azframework.vaadinflow.component.fluent;

import java.util.Collection;

import com.vaadin.flow.component.avatar.AvatarGroup;
import com.vaadin.flow.component.avatar.AvatarGroup.AvatarGroupI18n;
import com.vaadin.flow.component.avatar.AvatarGroup.AvatarGroupItem;
import com.vaadin.flow.component.avatar.AvatarGroupVariant;
import com.vaadin.flow.server.AbstractStreamResource;

public interface FluentAvatarGroup<I extends AvatarGroup & FluentAvatarGroup<I>>
		extends
		FluentComponent<I>,
		FluentHasSize<I>,
		FluentHasStyle<I>,
		FluentHasThemeVariant<I, AvatarGroupVariant> {

	class FluentAvatarGroupItem extends AvatarGroupItem {

		private static final long serialVersionUID = 3629159293803410979L;

		public FluentAvatarGroupItem withName(final String name) {
			this.setName(name);
			return this;
		}

		public FluentAvatarGroupItem withAbbreviation(final String abbr) {
			this.setAbbreviation(abbr);
			return this;
		}

		public FluentAvatarGroupItem withImage(final String url) {
			this.setImage(url);
			return this;
		}

		public FluentAvatarGroupItem withImageResource(final AbstractStreamResource resource) {
			this.setImageResource(resource);
			return this;
		}

		public FluentAvatarGroupItem withColorIndex(final Integer colorIndex) {
			this.setColorIndex(colorIndex);
			return this;
		}
	}

	default I withI18n(final AvatarGroupI18n i18n) {
		final I instance = this._castAsComponent();
		instance.setI18n(i18n);
		return instance;
	}

	default I withMaxItemsVisible(final Integer max) {
		final I instance = this._castAsComponent();
		instance.setMaxItemsVisible(max);
		return instance;
	}

	default I withItems(final AvatarGroupItem... items) {
		final I instance = this._castAsComponent();
		instance.setItems(items);
		return instance;
	}

	default I withItems(final Collection<AvatarGroupItem> items) {
		final I instance = this._castAsComponent();
		instance.setItems(items);
		return instance;
	}
}
