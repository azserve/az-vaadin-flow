package com.azserve.azframework.vaadinflow.component.grid;

import java.util.ArrayList;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.slf4j.LoggerFactory;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.funct.Functions;
import com.azserve.azframework.vaadinflow.component.AZButton;
import com.azserve.azframework.vaadinflow.data.binder.AZBinder;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Focusable;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.editor.Editor;
import com.vaadin.flow.component.grid.editor.EditorImpl;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;

@SuppressWarnings("hiding")
public class AZEditableGrid<T> extends AZGrid<T> {

	private static final long serialVersionUID = 5510873783687064777L;

	@FunctionalInterface
	public interface InsertCallback<T> {

		/**
		 * Executes an action when the insert
		 * button is clicked.
		 * <p>
		 * Can return a new item to add to the grid.
		 * If not, an item can be added manually using the
		 * {@link AZGrid#addItems(Object...)} method.
		 *
		 * @return a new item to add to the grid or {@code null}
		 */
		T onInsert();
	}

	@FunctionalInterface
	public interface SaveCallback<T> {

		/**
		 * Executes an action when the save
		 * button for an edited item is clicked.
		 *
		 * @param item the item to be saved
		 * @param successCallback callback to be called if save succedees
		 */
		void save(T item, Runnable successCallback);
	}

	@FunctionalInterface
	public interface CancelCallback<T> {

		/**
		 * Executes an action when the cancel
		 * button for an edited item is clicked.
		 * <p>
		 * To confirm the item deletion the given
		 * callback {@code Runnable} must be called,
		 * otherwise {@link AZGrid#removeItems(Object...)}
		 * method may be used later.
		 *
		 * @param item the item to be deleted
		 * @param deleteCallback callback to perform the item deletion
		 */
		void onCancel(T item, Runnable cancelCallback);
	}

	@FunctionalInterface
	public interface DeleteCallback<T> {

		/**
		 * Executes an action when the delete
		 * button for an item is clicked.
		 * <p>
		 * To confirm the item deletion the given
		 * callback {@code Runnable} must be called,
		 * otherwise {@link AZGrid#removeItems(Object...)}
		 * method may be used later.
		 *
		 * @param item the item to be deleted
		 * @param deleteCallback callback to perform the item deletion
		 */
		void onDelete(T item, Runnable deleteCallback);
	}

	/**
	 * Kind of actions to perform on items.
	 */
	public enum Action {
		INSERT,
		EDIT,
		DELETE,
		SAVE,
		CANCEL;
	}

	private static final String COL_EDIT = "edit";
	private static final String COL_DELETE = "delete";

	private final AZBinder<T> binder;

	private InsertCallback<T> insertCallback;
	private Predicate<T> editablePredicate = Functions.acceptAll();
	private Predicate<T> deletablePredicate = Functions.acceptAll();
	private CancelCallback<T> cancelCallback = (item, callback) -> callback.run();
	private SaveCallback<T> saveCallback = (item, callback) -> callback.run();
	private DeleteCallback<T> deleteCallback = (item, callback) -> callback.run();
	private Consumer<T> recoverItemCallback = Functions.nullConsumer();

	private Button btnInsert;
	private boolean insertion;
	private boolean readOnly;

	private T unsavedItem;

	/**
	 * Creates a new instance backed by an internal empty {@code ArrayList}
	 * as data source.
	 *
	 * @param itemClass the bean type to use, not {@code null}
	 * @param autoCommit whether or not the internal binder
	 *            is unbuffered
	 */
	public AZEditableGrid(final Class<T> itemClass, final boolean autoCommit) {
		this.setItems(new ArrayList<>());
		this.binder = new AZBinder<>(itemClass);
		this.getEditor().setBinder(this.binder);
		this.getEditor().setBuffered(!autoCommit);
		this.getEditor().addCancelListener(e -> {
			if (this.unsavedItem != null) {
				final T _unsavedItem = this.unsavedItem;
				this.unsavedItem = null;
				this.recoverItemCallback.accept(_unsavedItem);
			}
		});
		this.getElement().addEventListener("keyup", e -> this.onCancel())
				.setFilter("event.key === 'Escape' || event.key === 'Esc'");
	}

	/**
	 * Gets the internal editor binder.
	 */
	public AZBinder<T> getBinder() {
		return this.binder;
	}

	public boolean isReadOnly() {
		return this.readOnly;
	}

	/**
	 * Sets the read-only status of this grid.
	 * When read-only the user can interact with it but does not
	 * see the insert/edit/delete buttons.
	 * <p>
	 * Note: if the editor is open any changes made
	 * will be discarded.
	 *
	 * @param readOnly a boolean value specifying whether the grid is put
	 *            read-only mode or not
	 */
	public void setReadOnly(final boolean readOnly) {
		this.readOnly = readOnly;

		this.cancel();

		if (this.btnInsert != null) {
			this.btnInsert.setVisible(!readOnly);
		}
		final Column<T> editColumn = this.getEditColumn();
		if (editColumn != null) {
			editColumn.setVisible(!readOnly);
		}
		final Column<T> insertDeleteColumn = this.getInsertDeleteColumn();
		if (insertDeleteColumn != null) {
			insertDeleteColumn.setVisible(!readOnly);
		}
	}

	/**
	 * Creates and adds an edit and a delete column.
	 *
	 * @throws IllegalStateException if the columns have already been added.
	 */
	public void addEditorColumns() {
		this.addEditorColumns(false);
	}

	/**
	 * Creates and adds an edit and a delete column.
	 *
	 * @param frozen columns frozen state
	 *
	 * @throws IllegalStateException if the columns have already been added.
	 */
	public void addEditorColumns(final boolean frozen) {

		if (this.btnInsert != null) {
			throw new IllegalStateException("Editor columns already created");
		}
		final Button _btnInsert = this.createButton(Action.INSERT);
		_btnInsert.setVisible(this.insertCallback != null);
		_btnInsert.addClickListener(e -> this.insert());

		final Button btnSave = this.createButton(Action.SAVE);
		btnSave.addClickListener(e -> this.save());

		final Button btnCancel = this.createButton(Action.CANCEL);
		btnCancel.addClickListener(e -> this.onCancel());

		final boolean firstColumns = this.getColumns().isEmpty();

		final Column<T> editCol = this.addEditCancelColumn(btnCancel);
		final Column<T> deleteCol = this.addInsertDeleteSaveColumn(_btnInsert, btnSave);

		if (frozen) {
			if (firstColumns) {
				editCol.setFrozen(true);
				deleteCol.setFrozen(true);
			} else {
				editCol.setFrozenToEnd(true);
				deleteCol.setFrozenToEnd(true);
			}
		}
		if (this.readOnly) {
			editCol.setVisible(false);
			deleteCol.setVisible(false);
			_btnInsert.setVisible(false);
		}

		this.btnInsert = _btnInsert;
	}

	/**
	 * Gets the column where the edit buttons
	 * are rendered.
	 */
	public Column<T> getEditColumn() {
		return this.getColumnByKey(COL_EDIT);
	}

	/**
	 * Gets the column where the insert and delete buttons
	 * are rendered.
	 */
	public Column<T> getInsertDeleteColumn() {
		return this.getColumnByKey(COL_DELETE);
	}

	/**
	 * Gets the predicate which tests if the editor
	 * can be opened for a specific item, not {@code null}.
	 */
	public Predicate<T> getEditablePredicate() {
		return this.editablePredicate;
	}

	/**
	 * Sets the predicate which tests if the editor
	 * can be opened for a specific item and the
	 * corresponding edit button is shown.
	 *
	 * @param editablePredicate the predicate, not {@code null}
	 */
	public void setEditablePredicate(final @NonNull Predicate<T> editablePredicate) {
		this.editablePredicate = Objects.requireNonNull(editablePredicate);
	}

	/**
	 * Gets the predicate which tests if a specific
	 * item can be removed from the grid and the
	 * corresponding delete button is shown,
	 * not {@code null}.
	 */
	public Predicate<T> getDeletablePredicate() {
		return this.deletablePredicate;
	}

	/**
	 * Sets the predicate which tests if a
	 * specific item can be removed from
	 * the grid and the corresponding delete
	 * button is shown.
	 *
	 * @param deletablePredicate the predicate, not {@code null}
	 */
	public void setDeletablePredicate(final @NonNull Predicate<T> deletablePredicate) {
		this.deletablePredicate = Objects.requireNonNull(deletablePredicate);
	}

	/**
	 * Gets the callback called when the insert button is clicked,
	 * may be {@code null}.
	 */
	public InsertCallback<T> getInsertCallback() {
		return this.insertCallback;
	}

	/**
	 * Sets the callback called when the insert button is clicked.
	 * <p>
	 * If the callback is {@code null} the insert button is not
	 * rendered.
	 *
	 * @param insertCallback the callback called when the
	 *            insert button is clicked or {@code null}.
	 */
	public void setInsertCallback(final InsertCallback<T> insertCallback) {
		this.insertCallback = insertCallback;
		if (this.btnInsert != null) {
			this.btnInsert.setVisible(insertCallback != null);
		}
	}

	/**
	 * Gets the callback called when the save button is clicked,
	 * not {@code null}.
	 */
	public SaveCallback<T> getSaveCallback() {
		return this.saveCallback;
	}

	/**
	 * Sets the callback called when the save button is clicked.
	 *
	 * @param saveCallback callback called on save, not {@code null}.
	 */
	public void setSaveCallback(final @NonNull Consumer<T> saveCallback) {
		Objects.requireNonNull(saveCallback, "Save button callback cannot be null");
		this.saveCallback = (item, callback) -> {
			try {
				saveCallback.accept(item);
			} catch (final Exception ex) {
				LoggerFactory.getLogger(AZEditableGrid.class).warn("An exception occurred during save", ex);
				return;
			}
			callback.run();
		};
	}

	/**
	 * Sets the callback called when the save button is clicked.
	 *
	 * @param saveCallback callback called on save, not {@code null}.
	 */
	public void setSaveCallback(final @NonNull SaveCallback<T> saveCallback) {
		this.saveCallback = Objects.requireNonNull(saveCallback, "Save button callback cannot be null");
	}

	/**
	 * Sets the callback called when an item is in an inconsistent state
	 * and some action must be taken to restore a consistent state.
	 *
	 * @param recoverItemCallback callback,not {@code null}.
	 */
	public void setRecoverItemCallback(final Consumer<T> recoverItemCallback) {
		this.recoverItemCallback = Objects.requireNonNull(recoverItemCallback, "Recover item callback cannot be null");
	}

	/**
	 * Gets the callback called when the cancel button is clicked
	 * or the Escape key is pressed, not {@code null}.
	 */
	public CancelCallback<T> getCancelCallback() {
		return this.cancelCallback;
	}

	/**
	 * Sets the callback called when the cancel button is clicked
	 * or the Escape key is pressed}.
	 *
	 * @param cancelCallback callback called on cancel, not {@code null}.
	 */
	public void setCancelCallback(final @NonNull CancelCallback<T> cancelCallback) {
		this.cancelCallback = Objects.requireNonNull(cancelCallback, "Cancel button callback cannot be null");
	}

	/**
	 * Gets the callback called when the delete button is clicked,
	 * not {@code null}.
	 */
	public DeleteCallback<T> getDeleteCallback() {
		return this.deleteCallback;
	}

	/**
	 * Sets the callback called when the delete button is clicked.
	 *
	 * @param deleteCallback callback called on delete, not {@code null}.
	 */
	public void setDeleteCallback(final @NonNull DeleteCallback<T> deleteCallback) {
		this.deleteCallback = Objects.requireNonNull(deleteCallback, "Delete button callback cannot be null");
	}

	/**
	 * Adds a new item to the grid through the
	 * {@code insertCallback} and opens the editor
	 * component for that item.
	 * <p>
	 * If the {@code insertCallback} is {@code null}
	 * or returns a {@code null} item nothing happens.
	 */
	public void insert() {
		if (this.insertCallback == null || this.readOnly) {
			return;
		}
		final T item = this.insertCallback.onInsert();
		if (item == null) {
			return;
		}
		this.cancel();
		this.getListDataView().addItem(item);
		this.edit(item);
		this.insertion = true;
	}

	/**
	 * Opens the editor component for the specified item.
	 * <p>
	 * If the editor is alredy open any changes made
	 * will be discarded before reopening the editor.
	 */
	public void edit(final T item) {
		this.cancel();
		// XXX workaround to solve error when adding a new item and the grid has already a lot of items
		// XXX see https://github.com/vaadin/flow-components/issues/1223
		this.getDataCommunicator().getKeyMapper().key(item);

		if (this.readOnly) {
			return;
		}

		this.getEditor().editItem(item);

		Focusable<?> firstFocusable = null;
		for (final Column<T> col : this.getColumns()) {
			final Component c = col.getEditorComponent();
			if (c != null && c instanceof Focusable<?>) {
				final Focusable<?> focusable = (Focusable<?>) c;
				if (c instanceof HasValue<?, ?> && ((HasValue<?, ?>) c).isEmpty()) {
					focusable.focus();
					return;
				}
				if (firstFocusable == null) {
					firstFocusable = focusable;
				}
			}
		}
		if (firstFocusable != null) {
			firstFocusable.focus();
		}
	}

	/**
	 * Removes the specified item from the grid.
	 * <p>
	 * If the editor is alredy open any changes made
	 * will be discarded before removing the item.
	 */
	public void delete(final T item) {
		if (this.readOnly) {
			return;
		}
		this.cancel();
		this.getListDataView().removeItem(item);
	}

	/**
	 * Discards any changes made in editor fields if
	 * the editor is open.
	 */
	public void cancel() {
		final Editor<T> editor = this.getEditor();
		final T item = editor.getItem();
		if (item != null) {
			if (this.insertion) {
				this.getListDataView().removeItem(item);
			} else {
				editor.cancel();
			}
		}
		this.insertion = false;
	}

	@Override
	protected final Editor<T> createEditor() {
		return new EditorImpl<>(this, this.getPropertySet()) {

			private static final long serialVersionUID = 1L;

			@Override
			public void closeEditor() {
				final boolean buffered = this.isBuffered();
				try {
					// XXX prevents error: "Buffered editor should be closed using save() or cancel()"
					this.setBuffered(false);
					super.closeEditor();
				} finally {
					this.setBuffered(buffered);
				}
			}
		};
	}

	/**
	 * Creates the insert/delete/save column.
	 * <p>
	 * Should be called inside {@linkplain #addEditorColumns(boolean)}.
	 */
	protected Column<T> addInsertDeleteSaveColumn(final Button btnInsert, final Button btnSave) {
		return this.addComponentColumn(item -> {
			if (this.getDeletablePredicate().test(item)) {
				final Button btnDelete = this.createButton(Action.DELETE);
				btnDelete.addClickListener(e -> {
					this.deleteCallback.onDelete(item, () -> this.getListDataView().removeItem(item));
				});
				return btnDelete;
			}
			return new Span();
		}).setEditorComponent(btnSave)
				.setFlexGrow(0)
				.setWidth("5em")
				.setKey(COL_DELETE)
				.setHeader(btnInsert)
				.setTextAlign(ColumnTextAlign.CENTER);
	}

	/**
	 * Creates the edit/cancel column.
	 * <p>
	 * Should be called inside {@linkplain #addEditorColumns(boolean)}.
	 */
	protected Column<T> addEditCancelColumn(final Button btnCancel) {
		return this.addComponentColumn(item -> {
			if (this.getEditablePredicate().test(item)) {
				final Button btnEdit = this.createButton(Action.EDIT);
				btnEdit.addClickListener(e -> this.edit(item));
				return btnEdit;
			}
			return new Span();
		}).setEditorComponent(btnCancel)
				.setFlexGrow(0)
				.setWidth("5em")
				.setKey(COL_EDIT)
				.setTextAlign(ColumnTextAlign.CENTER);
	}

	/**
	 * Creates the button for the specified kind of action.
	 *
	 * @param action the kind of action.
	 * @return the button for the specific action, not {@code null}.
	 */
	protected Button createButton(final Action action) {
		final AZButton btn = new AZButton().withThemeVariants(ButtonVariant.LUMO_ICON, ButtonVariant.LUMO_SMALL);
		switch (action) {
			case CANCEL:
				btn.setIcon(VaadinIcon.CLOSE.create());
				break;
			case DELETE:
				btn.setIcon(VaadinIcon.TRASH.create());
				btn.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
				break;
			case EDIT:
				btn.setIcon(VaadinIcon.PENCIL.create());
				break;
			case INSERT:
				btn.setIcon(VaadinIcon.PLUS.create());
				btn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
				break;
			case SAVE:
				btn.setIcon(VaadinIcon.CHECK.create());
				btn.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SUCCESS);
				break;
		}
		return btn;
	}

	protected void save() {
		this.saveAndThen(Functions.NULL_RUNNABLE);
	}

	protected void saveAndThen(final Runnable callback) {
		if (this.getEditor().isOpen() && this.getEditor().isBuffered()) {
			this.getBinder().validate();
			final T item = this.getEditor().getItem();
			if (this.getBinder().writeBeanIfValid(item)) {
				this.unsavedItem = item;
				this.saveCallback.save(item, () -> {
					this.unsavedItem = null;
					this.insertion = false;
					this.getEditor().closeEditor();
					callback.run();
				});
			}
		} else {
			this.insertion = false;
		}
	}

	private void onCancel() {
		final T item = this.getEditor().getItem();
		if (item != null) {
			this.cancelCallback.onCancel(item, this::cancel);
		}
	}
}
