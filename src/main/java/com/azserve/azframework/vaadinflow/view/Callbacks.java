package com.azserve.azframework.vaadinflow.view;

import java.util.Set;
import java.util.function.Consumer;

import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.HasValue.ValueChangeEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;

public final class Callbacks {

	private Callbacks() {}

	public static <T> void setOnSingleSelectionChange(final Grid<T> grid, final Consumer<T> callback) {
		grid.addSelectionListener(e -> callback.accept(e.getFirstSelectedItem().orElse(null)));
	}

	public static <T> void setOnSingleSelectionChangeFromClient(final Grid<T> grid, final Consumer<T> callback) {
		grid.addSelectionListener(e -> {
			if (e.isFromClient()) {
				callback.accept(e.getFirstSelectedItem().orElse(null));
			}
		});
	}

	public static <T> void setOnMultiSelectionChange(final Grid<T> grid, final Consumer<Set<T>> callback) {
		grid.addSelectionListener(e -> callback.accept(e.getAllSelectedItems()));
	}

	public static <T> void setOnMultiSelectionChangeFromClient(final Grid<T> grid, final Consumer<Set<T>> callback) {
		grid.addSelectionListener(e -> {
			if (e.isFromClient()) {
				callback.accept(e.getAllSelectedItems());
			}
		});
	}

	public static <E extends ValueChangeEvent<V>, V> void setOnValueChange(final HasValue<E, V> field, final Runnable callback) {
		field.addValueChangeListener(e -> callback.run());
	}

	public static <E extends ValueChangeEvent<V>, V> void setOnValueChangeFromClient(final HasValue<E, V> field, final Runnable callback) {
		field.addValueChangeListener(e -> {
			if (e.isFromClient()) {
				callback.run();
			}
		});
	}

	public static <E extends ValueChangeEvent<V>, V> void setOnValueChange(final HasValue<E, V> field, final Consumer<V> callback) {
		field.addValueChangeListener(e -> callback.accept(e.getValue()));
	}

	public static <E extends ValueChangeEvent<V>, V> void setOnValueChangeFromClient(final HasValue<E, V> field, final Consumer<V> callback) {
		field.addValueChangeListener(e -> {
			if (e.isFromClient()) {
				callback.accept(e.getValue());
			}
		});
	}

	public static void setOnClick(final Button button, final Runnable callback) {
		button.addClickListener(e -> callback.run());
	}
}
