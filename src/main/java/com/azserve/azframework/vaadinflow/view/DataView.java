package com.azserve.azframework.vaadinflow.view;

import com.azserve.azframework.display.DataDisplay;
import com.azserve.azframework.presenter.DataListPresenter.DataOperation;
import com.azserve.azframework.presenter.DataPresenter;
import com.azserve.azframework.vaadinflow.data.binder.AZBinder;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.data.binder.ValidationException;

public abstract class DataView<C extends Component, D extends DataDisplay<T>, T>
		extends AbstractView<C, D> implements DataDisplay<T> {

	private static final long serialVersionUID = -4931001247283889427L;

	private final AZBinder<T> binder;

	public DataView(final DataPresenter<D, T> presenter, final Class<T> dataClass) {
		super(presenter);
		this.binder = this.newBinder(dataClass);
	}

	@Override
	public void readData(final T data, final DataOperation operation) {
		this.binder.readBean(data);
		this.binder.setReadOnly(DataOperation.VIEW == operation);
	}

	@Override
	public boolean writeData(final T data) {
		try {
			this.binder.writeBean(data);
			return true;
		} catch (final ValidationException e) {
			this.showError(e);
			return false;
		}
	}

	@Override
	public boolean isUnsaved(final T data) {
		return this.binder.hasChanges();
	}

	@Override
	public void onSuccessfulSave(final T data, final DataOperation operation, final Runnable callback) {
		this.notifyMessage("Dato salvato", Type.INFO);
		this.getUI().ifPresent(ui -> this.navigateBack(ui, false));
		callback.run();
	}

	@Override
	public void onFailedSave(final T data, final DataOperation operation, final Exception ex, final Runnable callback) {
		this.showError(ex, callback);
	}

	protected AZBinder<T> newBinder(final Class<T> beanType) {
		return new AZBinder<>(beanType);
	}

	protected AZBinder<T> getBinder() {
		return this.binder;
	}
}