package com.azserve.azframework.vaadinflow.view;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.azserve.azframework.display.Display;
import com.azserve.azframework.presenter.AbstractPresenter;
import com.azserve.azframework.vaadinflow.router.RoutingUtils;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.BeforeLeaveEvent;
import com.vaadin.flow.router.BeforeLeaveObserver;
import com.vaadin.flow.router.Location;
import com.vaadin.flow.router.NavigationTrigger;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.RouteParameters;

public abstract class AbstractView<C extends Component, D extends Display>
		extends Composite<C>
		implements BeforeEnterObserver, BeforeLeaveObserver {

	private static final long serialVersionUID = 2294169487818480499L;

	private Dialog parentDialog;
	private NavigationTrigger navigationTrigger;
	private Location routeLocation;

	private final AbstractPresenter<D> presenter;

	public AbstractView(final AbstractPresenter<D> presenter) {
		this.presenter = presenter;
	}

	AbstractPresenter<D> getPresenter() {
		return this.presenter;
	}

	@SuppressWarnings("unchecked")
	private void startPresenter() {
		this.presenter.setDisplay((D) this);
	}

	public void startPresenter(final Map<String, List<String>> parameters) {
		this.startPresenter();
		this.getPresenter().setParameters(parameters);
	}

	@Override
	public void beforeEnter(final BeforeEnterEvent event) {
		this.routeLocation = event.getLocation();
		this.navigationTrigger = event.getTrigger();
		this.startPresenter(this.readParameters(event));
	}

	@Override
	public void beforeLeave(final BeforeLeaveEvent event) {
		this.getPresenter().onLeave(event.postpone()::proceed);
	}

	public Dialog attachToDialog() {
		final Dialog dialog = this.createDialog();
		dialog.addDialogCloseActionListener(e -> this.presenter.onLeave(dialog::close));
		dialog.open();
		this.parentDialog = dialog;
		this.startPresenter();
		return dialog;
	}

	public String getPageTitle() {
		final PageTitle pageTitle = this.getClass().getAnnotation(PageTitle.class);
		return pageTitle != null ? pageTitle.value() : "";
	}

	@Override
	protected abstract C initContent();

	protected Dialog createDialog() {
		return new Dialog(this);
	}

	protected final Dialog getParentDialog() {
		return this.parentDialog;
	}

	protected NavigationTrigger getNavigationTrigger() {
		return this.navigationTrigger;
	}

	protected final void navigateBack(final UI ui) {
		this.navigateBack(ui, true);
	}

	protected final void navigateBack(final UI ui, final boolean checkBeforeLeaving) {
		final Runnable proceedCallback = this.getNavigateBackProceedCallback(ui);
		if (checkBeforeLeaving) {
			this.presenter.onLeave(proceedCallback);
		} else {
			proceedCallback.run();
		}
	}

	protected Runnable getNavigateBackProceedCallback(final UI ui) {
		if (this.parentDialog != null) {
			return this.parentDialog::close;
		}
		if (this.navigationTrigger == null) {
			// not in a navigation state
			return () -> this.getParent()
					.filter(HasComponents.class::isInstance)
					.map(HasComponents.class::cast)
					.ifPresent(parent -> parent.remove(this));
		}
		if (this.isNavigateBackToHistory()) {
			return ui.getPage().getHistory()::back;
		}
		return () -> this.navigateBackToDefaultView(ui);
	}

	protected boolean isNavigateBackToHistory() {
		return NavigationTrigger.PAGE_LOAD != this.navigationTrigger
				&& NavigationTrigger.CLIENT_SIDE != this.navigationTrigger;
	}

	protected void navigateBackToDefaultView(final UI ui) {
		final Class<? extends Component> defaultBackView = this.getDefaultBackView();
		if (defaultBackView != null) {
			ui.navigate(defaultBackView);
		}
	}

	protected Class<? extends Component> getDefaultBackView() {
		return null;
	}

	protected Location getLocation() {
		return this.routeLocation;
	}

	protected Map<String, List<String>> readParameters(final BeforeEnterEvent event) {
		final Map<String, List<String>> result = new HashMap<>();
		result.putAll(RoutingUtils.decodeParameters(event.getLocation().getQueryParameters().getParameters()));
		this.readRouteParameters(event.getRouteParameters(), result);
		return result;
	}

	protected void readRouteParameters(final RouteParameters routeParameters, final Map<String, List<String>> result) {
		for (final String key : routeParameters.getParameterNames()) {
			routeParameters.get(key).ifPresent(value -> result.put(key, List.of(value)));
		}
	}
}