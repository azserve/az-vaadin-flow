package com.azserve.azframework.vaadinflow.view;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.display.DataListDisplay;
import com.azserve.azframework.presenter.DataListPresenter;
import com.azserve.azframework.vaadinflow.component.grid.AZGrid;
import com.azserve.azframework.vaadinflow.router.RoutingUtils;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.QueryParameters;

public abstract class DataListViewRoute<C extends Component, D extends DataListDisplay<I>, I>
		extends AbstractView<C, D> implements DataListDisplay<I> {

	private static final long serialVersionUID = 8732784028300815876L;

	private final AZGrid<I> grid;

	public DataListViewRoute(final DataListPresenter<I, D> presenter) {
		this(presenter, new AZGrid<>());
	}

	protected DataListViewRoute(final DataListPresenter<I, D> presenter, final AZGrid<I> grid) {
		super(presenter);
		this.grid = grid;
	}

	protected AZGrid<I> getGrid() {
		return this.grid;
	}

	@Override
	public void openData(final I sourceItem, final Map<String, List<String>> parameters) {
		RoutingUtils.navigate(UI.getCurrent(), this.getDataViewClass(sourceItem), new QueryParameters(parameters));
	}

	@Override
	public Optional<I> getSelectedItem() {
		return this.grid.getSelectionModel().getFirstSelectedItem();
	}

	@Override
	public Collection<I> getSelectedItems() {
		return this.grid.getSelectedItems();
	}

	@Override
	public void setItems(final @NonNull List<I> items) {
		this.grid.setItems(items);
	}

	@Override
	public void askDelete(final @NonNull I item, final Runnable confirmCallback) {
		confirmCallback.run();
	}

	@Override
	public void onSuccessfulDelete(final I item) {
		this.notifyMessage("Dato eliminato", Type.INFO);
	}

	protected abstract @NonNull Class<? extends DataView<?, ?, ?>> getDataViewClass(I item);
}
