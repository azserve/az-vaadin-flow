package com.azserve.azframework.vaadinflow.field;

import com.azserve.azframework.vaadinflow.component.fluent.FluentCommonField;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasDataView;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasListDataView;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ItemLabelGenerator;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.radiobutton.dataview.RadioButtonGroupDataView;
import com.vaadin.flow.component.radiobutton.dataview.RadioButtonGroupListDataView;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.function.SerializablePredicate;

@SuppressWarnings("serial")
public class AZRadioButtonGroup<T> extends RadioButtonGroup<T>
		implements
		FluentCommonField<RadioButtonGroup<T>, T, AZRadioButtonGroup<T>>,
		FluentHasListDataView<AZRadioButtonGroup<T>, T, RadioButtonGroupListDataView<T>>,
		FluentHasDataView<AZRadioButtonGroup<T>, T, Void, RadioButtonGroupDataView<T>> {

	public AZRadioButtonGroup<T> withItemLabelGenerator(final ItemLabelGenerator<T> itemLabelGenerator) {
		this.setItemLabelGenerator(itemLabelGenerator);
		return this;
	}

	public AZRadioButtonGroup<T> withItemEnabledProvider(final SerializablePredicate<T> itemEnabledProvider) {
		this.setItemEnabledProvider(itemEnabledProvider);
		return this;
	}

	public AZRadioButtonGroup<T> withRenderer(final ComponentRenderer<? extends Component, T> renderer) {
		this.setRenderer(renderer);
		return this;
	}

	public AZRadioButtonGroup<T> withRequired(final boolean required) {
		this.setRequired(required);
		return this;
	}
}