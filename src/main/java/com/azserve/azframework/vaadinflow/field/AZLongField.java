package com.azserve.azframework.vaadinflow.field;

import java.util.Locale;
import java.util.Objects;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;

@JsModule("./azframework/az-number-field/az-long-field.js")
@Tag("az-long-field")
public class AZLongField extends AZNumberField<AZLongField, Long> {

	private static final long serialVersionUID = 168911375884036838L;

	public AZLongField(final String label) {
		this();
		this.setLabel(label);
	}

	public AZLongField() {
		super((field, s) -> {
			if (s == null || s.trim().isEmpty()) {
				return null;
			}
			try {
				return Long.valueOf(s);
			} catch (@SuppressWarnings("unused") final Exception ex) {
				field.setPresentationValue(null);
				return null;
			}
		}, (field, value) -> Objects.toString(value, ""));
	}

	@Override
	protected void configureField(final Locale locale) {
		this.setPattern("^[-+]?\\d*$");
	}

	@Override
	public String getErrorMessage() {
		return super.getErrorMessageString();
	}

	@Override
	public void setErrorMessage(final String errorMessage) {
		super.setErrorMessage(errorMessage);
	}

	@Override
	public boolean isInvalid() {
		return this.isInvalidBoolean();
	}

	@Override
	public void setInvalid(final boolean invalid) {
		super.setInvalid(invalid);
	}

	@Override
	public String getLabel() {
		return this.getLabelString();
	}

	@Override
	public void setLabel(final String label) {
		super.setLabel(label);
	}
}
