package com.azserve.azframework.vaadinflow.field;

import com.azserve.azframework.vaadinflow.component.fluent.FluentCommonInputField;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasAutocapitalize;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasAutocomplete;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasValueChangeMode;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;

@SuppressWarnings("serial")
public class AZTextField extends TextField
		implements
		FluentCommonInputField<TextField, String, AZTextField>,
		FluentHasAutocapitalize<AZTextField>,
		FluentHasAutocomplete<AZTextField>,
		FluentHasValueChangeMode<AZTextField> {

	public AZTextField() {
		super();
	}

	public AZTextField(final String label, final String initialValue, final String placeholder) {
		super(label, initialValue, placeholder);
	}

	public AZTextField(final String label, final String initialValue, final ValueChangeListener<? super ComponentValueChangeEvent<TextField, String>> listener) {
		super(label, initialValue, listener);
	}

	public AZTextField(final String label, final String placeholder) {
		super(label, placeholder);
	}

	public AZTextField(final String label, final ValueChangeListener<? super ComponentValueChangeEvent<TextField, String>> listener) {
		super(label, listener);
	}

	public AZTextField(final String label) {
		super(label);
	}

	public AZTextField(final ValueChangeListener<? super ComponentValueChangeEvent<TextField, String>> listener) {
		super(listener);
	}

	public AZTextField withMaxLength(final int maxLength) {
		this.setMaxLength(maxLength);
		return this;
	}

	public AZTextField withMinLength(final int minLength) {
		this.setMinLength(minLength);
		return this;
	}

	public AZTextField withPattern(final String pattern) {
		this.setPattern(pattern);
		return this;
	}

	public AZTextField withPlaceholder(final String placeholder) {
		this.setPlaceholder(placeholder);
		return this;
	}

	public AZTextField withThemeVariants(final TextFieldVariant... variants) {
		this.addThemeVariants(variants);
		return this;
	}
}
