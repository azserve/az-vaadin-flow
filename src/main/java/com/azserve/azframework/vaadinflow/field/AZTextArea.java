package com.azserve.azframework.vaadinflow.field;

import com.azserve.azframework.vaadinflow.component.fluent.FluentCommonInputField;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasAutocapitalize;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasAutocomplete;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasValueChangeMode;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextAreaVariant;

@SuppressWarnings("serial")
public class AZTextArea extends TextArea
		implements
		FluentCommonInputField<TextArea, String, AZTextArea>,
		FluentHasAutocapitalize<AZTextArea>,
		FluentHasAutocomplete<AZTextArea>,
		FluentHasValueChangeMode<AZTextArea> {

	public AZTextArea() {
		super();
	}

	public AZTextArea(final String label, final String initialValue, final String placeholder) {
		super(label, initialValue, placeholder);
	}

	public AZTextArea(final String label, final String initialValue, final ValueChangeListener<? super ComponentValueChangeEvent<TextArea, String>> listener) {
		super(label, initialValue, listener);
	}

	public AZTextArea(final String label, final String placeholder) {
		super(label, placeholder);
	}

	public AZTextArea(final String label, final ValueChangeListener<? super ComponentValueChangeEvent<TextArea, String>> listener) {
		super(label, listener);
	}

	public AZTextArea(final String label) {
		super(label);
	}

	public AZTextArea(final ValueChangeListener<? super ComponentValueChangeEvent<TextArea, String>> listener) {
		super(listener);
	}

	public AZTextArea withMaxLength(final int maxLength) {
		this.setMaxLength(maxLength);
		return this;
	}

	public AZTextArea withMinLength(final int minLength) {
		this.setMinLength(minLength);
		return this;
	}

	public AZTextArea withPattern(final String pattern) {
		this.setPattern(pattern);
		return this;
	}

	public AZTextArea withPlaceholder(final String placeholder) {
		this.setPlaceholder(placeholder);
		return this;
	}

	public AZTextArea withThemeVariants(final TextAreaVariant... variants) {
		this.addThemeVariants(variants);
		return this;
	}
}
