package com.azserve.azframework.vaadinflow.field;

import java.util.Collection;

import com.azserve.azframework.vaadinflow.component.fluent.FluentCommonInputField;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasLazyDataView;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasListDataView;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasThemeVariant;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.ItemLabelGenerator;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.combobox.ComboBoxVariant;
import com.vaadin.flow.component.combobox.dataview.ComboBoxLazyDataView;
import com.vaadin.flow.component.combobox.dataview.ComboBoxListDataView;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.data.renderer.Renderer;

@SuppressWarnings("serial")
@Tag("az-combo-box")
@JsModule("./azframework/az-combo-box/az-combo-box.js")
public class AZComboBox<T> extends ComboBox<T> implements
		FluentCommonInputField<ComboBox<T>, T, AZComboBox<T>>,
		FluentHasListDataView<AZComboBox<T>, T, ComboBoxListDataView<T>>,
		FluentHasLazyDataView<AZComboBox<T>, T, String, ComboBoxLazyDataView<T>>,
		FluentHasThemeVariant<AZComboBox<T>, ComboBoxVariant> {

	public AZComboBox() {
		super();
	}

	public AZComboBox(final int pageSize) {
		super(pageSize);
	}

	public AZComboBox(final String label, final Collection<T> items) {
		super(label, items);
	}

	@SafeVarargs
	public AZComboBox(final String label, final T... items) {
		super(label, items);
	}

	public AZComboBox(final String label) {
		super(label);
	}

	public AZComboBox<T> withAllowCustomValue(final boolean allowCustomValue) {
		this.setAllowCustomValue(allowCustomValue);
		return this;
	}

	public AZComboBox<T> withAutoOpen(final boolean autoOpen) {
		this.setAutoOpen(autoOpen);
		return this;
	}

	public AZComboBox<T> withItemLabelGenerator(final ItemLabelGenerator<T> itemLabelGenerator) {
		this.setItemLabelGenerator(itemLabelGenerator);
		return this;
	}

	public AZComboBox<T> withPageSize(final int pageSize) {
		this.setPageSize(pageSize);
		return this;
	}

	public AZComboBox<T> withPopupWidth(final String popupWidth) {
		this.getStyle().set("--vaadin-combo-box-overlay-width", popupWidth);
		return this;
	}

	public AZComboBox<T> withPopupWidth(final float width, final Unit unit) {
		return this.withPopupWidth(HasSize.getCssSize(width, unit));
	}

	public AZComboBox<T> withRenderer(final Renderer<T> renderer) {
		this.setRenderer(renderer);
		return this;
	}
}
