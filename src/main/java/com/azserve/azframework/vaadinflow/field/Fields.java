package com.azserve.azframework.vaadinflow.field;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

import java.util.List;

import com.azserve.azframework.common.enums.EnumWithDescription;
import com.vaadin.flow.component.checkbox.CheckboxGroup;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.data.renderer.TextRenderer;

public final class Fields {

	public static AZSelect<Boolean> booleanSelect(final boolean trueFirst, final String trueLabel, final String falseLabel, final String nullLabel) {
		final AZSelect<Boolean> field = new AZSelect<>();
		field.setItems(booleanItems(trueFirst));
		field.setItemLabelGenerator(b -> b != null ? b.booleanValue() ? trueLabel : falseLabel : nullLabel);
		field.setEmptySelectionAllowed(true);
		field.setEmptySelectionCaption(nullLabel);
		return field;
	}

	public static AZSelect<Boolean> booleanSelect(final boolean trueFirst, final String trueLabel, final String falseLabel) {
		final AZSelect<Boolean> field = new AZSelect<>();
		field.setItems(booleanItems(trueFirst));
		field.setItemLabelGenerator(b -> b.booleanValue() ? trueLabel : falseLabel);
		return field;
	}

	public static <E extends Enum<E>> AZSelect<E> enumSelect(final Class<E> enumClass, final String label) {
		return enumSelect(enumClass).withLabel(label);
	}

	public static <E extends Enum<E>> AZSelect<E> enumSelect(final Class<E> enumClass) {
		final AZSelect<E> field = new AZSelect<>();
		if (EnumWithDescription.class.isAssignableFrom(enumClass)) {
			field.setItemLabelGenerator(Fields::enumDescription);
		}
		field.setItems(enumClass.getEnumConstants());
		return field;
	}

	public static AZComboBox<Boolean> booleanComboBox(final boolean trueFirst, final String trueLabel, final String falseLabel) {
		final AZComboBox<Boolean> field = new AZComboBox<>();
		field.setItems(booleanItems(trueFirst));
		field.setItemLabelGenerator(b -> b.booleanValue() ? trueLabel : falseLabel);
		return field;
	}

	public static <E extends Enum<E>> AZComboBox<E> enumComboBox(final Class<E> enumClass, final String label) {
		return enumComboBox(enumClass).withLabel(label);
	}

	public static <E extends Enum<E>> AZComboBox<E> enumComboBox(final Class<E> enumClass) {
		final AZComboBox<E> field = new AZComboBox<>();
		if (EnumWithDescription.class.isAssignableFrom(enumClass)) {
			field.setItemLabelGenerator(Fields::enumDescription);
		}
		field.setItems(enumClass.getEnumConstants());
		return field;
	}

	public static <E extends Enum<E>> AZMultiSelectComboBox<E> enumMultiSelectComboBox(final Class<E> enumClass, final String label) {
		return enumMultiSelectComboBox(enumClass).withLabel(label);
	}

	public static <E extends Enum<E>> AZMultiSelectComboBox<E> enumMultiSelectComboBox(final Class<E> enumClass) {
		final AZMultiSelectComboBox<E> field = new AZMultiSelectComboBox<>();
		if (EnumWithDescription.class.isAssignableFrom(enumClass)) {
			field.setItemLabelGenerator(Fields::enumDescription);
		}
		field.setItems(enumClass.getEnumConstants());
		return field;
	}

	public static RadioButtonGroup<Boolean> booleanRadioButtonGroup(final boolean trueFirst, final String trueLabel, final String falseLabel) {
		final RadioButtonGroup<Boolean> field = new RadioButtonGroup<>();
		field.setItems(booleanItems(trueFirst));
		field.setRenderer(new TextRenderer<>(b -> b.booleanValue() ? trueLabel : falseLabel));
		return field;
	}

	public static <E extends Enum<E>> RadioButtonGroup<E> enumRadioButtonGroup(final Class<E> enumClass, final String label) {
		final RadioButtonGroup<E> field = enumRadioButtonGroup(enumClass);
		field.setLabel(label);
		return field;
	}

	public static <E extends Enum<E>> RadioButtonGroup<E> enumRadioButtonGroup(final Class<E> enumClass) {
		final RadioButtonGroup<E> field = new RadioButtonGroup<>();
		if (EnumWithDescription.class.isAssignableFrom(enumClass)) {
			field.setRenderer(new TextRenderer<>(Fields::enumDescription));
		}
		field.setItems(enumClass.getEnumConstants());
		return field;
	}

	public static CheckboxGroup<Boolean> booleanCheckBoxGroup(final boolean trueFirst, final String trueLabel, final String falseLabel) {
		final CheckboxGroup<Boolean> field = new CheckboxGroup<>();
		field.setItems(booleanItems(trueFirst));
		field.setItemLabelGenerator(b -> b.booleanValue() ? trueLabel : falseLabel);
		return field;
	}

	public static <E extends Enum<E>> CheckboxGroup<E> enumCheckBoxGroup(final Class<E> enumClass, final String label) {
		final CheckboxGroup<E> field = enumCheckBoxGroup(enumClass);
		field.setLabel(label);
		return field;
	}

	public static <E extends Enum<E>> CheckboxGroup<E> enumCheckBoxGroup(final Class<E> enumClass) {
		final CheckboxGroup<E> field = new CheckboxGroup<>();
		if (EnumWithDescription.class.isAssignableFrom(enumClass)) {
			field.setItemLabelGenerator(Fields::enumDescription);
		}
		field.setItems(enumClass.getEnumConstants());
		return field;
	}

	public static <E extends Enum<E>> AZChipComboBox<E> enumChipComboBox(final Class<E> enumClass, final String label) {
		final AZChipComboBox<E> chipField = enumChipComboBox(enumClass);
		chipField.setLabel(label);
		return chipField;
	}

	public static <E extends Enum<E>> AZChipComboBox<E> enumChipComboBox(final Class<E> enumClass) {
		final AZChipComboBox<E> chipField = new AZChipComboBox<>();
		chipField.getField().setItems(enumClass.getEnumConstants());
		if (EnumWithDescription.class.isAssignableFrom(enumClass)) {
			chipField.getField().setItemLabelGenerator(Fields::enumDescription);
		}
		return chipField;
	}

	public static <E extends Enum<E>> AZChipSelect<E> enumChipSelect(final Class<E> enumClass, final String label) {
		final AZChipSelect<E> chipField = enumChipSelect(enumClass);
		chipField.setLabel(label);
		return chipField;
	}

	public static <E extends Enum<E>> AZChipSelect<E> enumChipSelect(final Class<E> enumClass) {
		final AZChipSelect<E> chipField = new AZChipSelect<>();
		chipField.getField().setItems(enumClass.getEnumConstants());
		if (EnumWithDescription.class.isAssignableFrom(enumClass)) {
			chipField.getField().setItemLabelGenerator(Fields::enumDescription);
		}
		return chipField;
	}

	private static String enumDescription(final Enum<?> enumConstant) {
		return enumConstant != null ? ((EnumWithDescription) enumConstant).description() : "";
	}

	private static List<Boolean> booleanItems(final boolean trueFirst) {
		return trueFirst ? List.of(TRUE, FALSE) : List.of(FALSE, TRUE);
	}

	private Fields() {
		throw new AssertionError();
	}
}
