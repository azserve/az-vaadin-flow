package com.azserve.azframework.vaadinflow.field;

public class AZChipComboBox<T> extends AZChipField<T, AZComboBox<T>> {

	private static final long serialVersionUID = -8218073402556196531L;

	public AZChipComboBox() {
		super(new AZComboBox<>());
		this.setItemLabelGenerator(item -> this.getField().getItemLabelGenerator().apply(item));
	}

	public AZChipComboBox(final String label) {
		super(label, new AZComboBox<>());
		this.setItemLabelGenerator(item -> this.getField().getItemLabelGenerator().apply(item));
	}

	public AZChipComboBox(final String label, final AZComboBox<T> field) {
		super(label, field);
		this.setItemLabelGenerator(item -> this.getField().getItemLabelGenerator().apply(item));
	}
}
