package com.azserve.azframework.vaadinflow.field;

import java.util.Locale;

import com.azserve.azframework.vaadinflow.component.fluent.FluentCommonInputField;
import com.vaadin.flow.component.textfield.GeneratedVaadinTextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.function.SerializableBiFunction;

public abstract class AZNumberField<F extends AZNumberField<F, N>, N extends Number> extends GeneratedVaadinTextField<F, N>
		implements FluentCommonInputField<F, N, F> {

	private static final long serialVersionUID = -1221206086781255547L;

	protected AZNumberField(final SerializableBiFunction<F, String, N> presentationToModel, final SerializableBiFunction<F, N, String> modelToPresentation) {
		super(null, null, String.class, presentationToModel, modelToPresentation);
		this.setPreventInvalidInput(true);
		this.setSynchronizedEvent(ValueChangeMode.eventForMode(ValueChangeMode.ON_BLUR, "value-changed"));
		this.addAttachListener(e -> this.configureField(e.getUI().getLocale()));
	}

	/**
	 * @param locale locale
	 */
	protected void configureField(final Locale locale) {}

	@Override
	public String getErrorMessage() {
		return super.getErrorMessageString();
	}

	@Override
	public void setErrorMessage(final String errorMessage) {
		super.setErrorMessage(errorMessage);
	}

	@Override
	public boolean isInvalid() {
		return this.isInvalidBoolean();
	}

	@Override
	public void setInvalid(final boolean invalid) {
		super.setInvalid(invalid);
	}

	@Override
	public String getLabel() {
		return this.getLabelString();
	}

	@Override
	public void setLabel(final String label) {
		super.setLabel(label);
	}

	public boolean isAutofocus() {
		return this.isAutofocusBoolean();
	}

	@Override
	public void setAutofocus(final boolean autofocus) {
		super.setAutofocus(autofocus);
	}

	public boolean isAutoselect() {
		return super.isAutoselectBoolean();
	}

	@Override
	public void setAutoselect(final boolean autoselect) {
		super.setAutoselect(autoselect);
	}

	public boolean isRequired() {
		return this.isRequiredBoolean();
	}

	@Override
	public void setRequired(final boolean required) {
		super.setRequired(required);
	}

	@Override
	public void setPlaceholder(final String placeholder) {
		super.setPlaceholder(placeholder);
	}

	public String getPlaceholder() {
		return this.getPlaceholderString();
	}
}
