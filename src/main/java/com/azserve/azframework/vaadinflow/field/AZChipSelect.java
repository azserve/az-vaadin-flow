package com.azserve.azframework.vaadinflow.field;

import static java.util.Objects.requireNonNullElseGet;

public class AZChipSelect<T> extends AZChipField<T, AZSelect<T>> {

	private static final long serialVersionUID = -8218073402556196531L;

	public AZChipSelect() {
		super(new AZSelect<>());
		this.setItemLabelGenerator(item -> requireNonNullElseGet(
				this.getField().getItemLabelGenerator(), this::getItemLabelGenerator)
						.apply(item));
	}

	public AZChipSelect(final String label) {
		super(label, new AZSelect<>());
		this.setItemLabelGenerator(item -> requireNonNullElseGet(
				this.getField().getItemLabelGenerator(), this::getItemLabelGenerator)
						.apply(item));
	}
}
