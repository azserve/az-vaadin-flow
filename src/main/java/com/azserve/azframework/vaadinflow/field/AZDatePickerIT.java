package com.azserve.azframework.vaadinflow.field;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import com.azserve.azframework.vaadinflow.component.fluent.FluentDatePicker;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dependency.JsModule;

@SuppressWarnings("serial")
@Tag("az-date-picker-it")
@JsModule("./azframework/az-date-picker-it/az-date-picker-it.js")
public class AZDatePickerIT extends DatePicker
		implements
		FluentDatePicker<AZDatePickerIT> {

	public AZDatePickerIT() {
		super();
		this.setAutoOpen(false);
	}

	public AZDatePickerIT(final LocalDate initialDate, final ValueChangeListener<ComponentValueChangeEvent<DatePicker, LocalDate>> listener) {
		super(initialDate, listener);
		this.setAutoOpen(false);
	}

	public AZDatePickerIT(final LocalDate initialDate) {
		super(initialDate);
		this.setAutoOpen(false);
	}

	public AZDatePickerIT(final String label, final LocalDate initialDate, final ValueChangeListener<ComponentValueChangeEvent<DatePicker, LocalDate>> listener) {
		super(label, initialDate, listener);
		this.setAutoOpen(false);
	}

	public AZDatePickerIT(final String label, final LocalDate initialDate) {
		super(label, initialDate);
		this.setAutoOpen(false);
	}

	public AZDatePickerIT(final String label, final ValueChangeListener<ComponentValueChangeEvent<DatePicker, LocalDate>> listener) {
		super(label, listener);
		this.setAutoOpen(false);
	}

	public AZDatePickerIT(final String label) {
		super(label);
		this.setAutoOpen(false);
	}

	public AZDatePickerIT(final ValueChangeListener<ComponentValueChangeEvent<DatePicker, LocalDate>> listener) {
		super(listener);
		this.setAutoOpen(false);
	}

	@Override
	protected void onAttach(final AttachEvent attachEvent) {
		super.onAttach(attachEvent);
		this.setLocale(Locale.ITALY);
		this.setI18n(createDatePickerI18n());
		attachEvent.getUI().beforeClientResponse(this, context -> this.getElement().executeJs("this.initCustomParser()"));
	}

	public static DatePickerI18n createDatePickerI18n() {
		// i giorni della settimana devono partire da domenica ma l'enum DayOfWeek parte da lunedì
		final DayOfWeek[] daysOfWeek = DayOfWeek.values(); // values() fornisce una copia delle costanti
		for (int i = daysOfWeek.length - 2; i >= 0; i--) {
			daysOfWeek[i + 1] = daysOfWeek[i];
		}
		daysOfWeek[0] = DayOfWeek.SUNDAY;
		final List<String> weekdays = new ArrayList<>(daysOfWeek.length);
		final List<String> weekdaysShort = new ArrayList<>(daysOfWeek.length);
		for (final DayOfWeek element : daysOfWeek) {
			weekdays.add(element.getDisplayName(TextStyle.FULL, Locale.ITALY));
			weekdaysShort.add(element.getDisplayName(TextStyle.SHORT, Locale.ITALY));
		}
		return new DatePickerI18n()
				// .setDateFormats("dd/MM/yyyy", "d", "d/M", "d/M/yy", "d M", "d M yy")
				// .setReferenceDate(LocalDate.now().plusMonths(1))
				.setCancel("Annulla")
				.setToday("Oggi")
				.setFirstDayOfWeek(1)
				.setMonthNames(EnumSet.allOf(Month.class).stream().map(m -> m.getDisplayName(TextStyle.FULL, Locale.ITALY)).collect(Collectors.toList()))
				.setWeekdays(weekdays)
				.setWeekdaysShort(weekdaysShort);
	}
}
