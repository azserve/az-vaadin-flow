package com.azserve.azframework.vaadinflow.field;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.JsModule;

@JsModule("./azframework/az-number-field/az-decimal-field.js")
@Tag("az-decimal-field")
public class AZDecimalField extends AZNumberField<AZDecimalField, BigDecimal> {

	private static final long serialVersionUID = 168911375884036838L;

	private final DecimalFormat formatter = new DecimalFormat("0.##########");
	private DecimalFormatSymbols symbols;

	public AZDecimalField(final String label) {
		this();
		this.setLabel(label);
	}

	public AZDecimalField() {
		super((field, s) -> {
			if (s == null || s.trim().isEmpty()) {
				return null;
			}
			field.ensureFieldConfigured();
			try {
				final BigDecimal value = new BigDecimal(s.replace(field.symbols.getDecimalSeparator(), '.'));
				final String formattedValue = field.formatter.format(value);
				return (BigDecimal) field.formatter.parse(formattedValue);
			} catch (final @SuppressWarnings("unused") Exception ex) {
				field.setPresentationValue(null);
				return null;
			}
		}, (field, value) -> {
			if (value != null) {
				field.ensureFieldConfigured();
				return field.formatter.format(value);
			}
			return "";
		});

		this.configureField(UI.getCurrent().getLocale());
		this.formatter.setParseBigDecimal(true);
		this.addBlurListener(e -> this.setPresentationValue(this.getValue()));
	}

	private void ensureFieldConfigured() {
		if (this.symbols == null) {
			this.configureField(this.getLocale());
		}
	}

	@Override
	protected void configureField(final Locale locale) {
		this.symbols = DecimalFormatSymbols.getInstance(locale);
		this.formatter.setDecimalFormatSymbols(this.symbols);
		final char decimalSeparator = this.symbols.getDecimalSeparator();
		final String decSeparatorRegex = decimalSeparator == '.' ? "" : String.valueOf(decimalSeparator);
		this.setPattern("^[\\-]{0,1}\\d*[\\." + decSeparatorRegex + "]?\\d*$");
	}

	public DecimalFormat getFormatter() {
		return this.formatter;
	}
}
