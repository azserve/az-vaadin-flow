package com.azserve.azframework.vaadinflow.field;

import static java.util.Objects.requireNonNull;
import static java.util.Objects.requireNonNullElse;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ItemLabelGenerator;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.function.SerializableBiFunction;
import com.vaadin.flow.theme.lumo.LumoUtility;

public class AZChipField<T, C extends AbstractField<? super C, T>> extends CustomField<Set<T>> {

	private static final long serialVersionUID = -1180913657310696281L;

	public interface ItemChipGenerator<T> extends SerializableBiFunction<T, Runnable, Component> {

		@Override
		Component apply(T item, Runnable removeItemAction);
	}

	protected static <T> Component createDefaultChip(final T item, final ItemLabelGenerator<T> itemLabelGenerator, final Runnable removeItemAction) {
		final Icon removeIcon = new Icon("lumo", "cross");
		removeIcon.getStyle().set("cursor", "pointer");
		removeIcon.addClickListener(e -> removeItemAction.run());
		final Span chip = new Span(new Span(requireNonNullElse(itemLabelGenerator.apply(item), "")), removeIcon);
		chip.getStyle().set("cursor", "default");
		chip.addClassNames(
				LumoUtility.Margin.Left.XSMALL,
				LumoUtility.Gap.SMALL);
		chip.getElement().getThemeList().set("badge", true);
		chip.getElement().getThemeList().set("pill", true);
		return chip;
	}

	private final C field;
	private Map<T, Component> items;

	private ItemLabelGenerator<T> itemLabelGenerator = Object::toString;
	private ItemChipGenerator<T> itemChipGenerator = (item, removeItemAction) -> createDefaultChip(
			item,
			this.getItemLabelGenerator(),
			removeItemAction);

	public AZChipField(final C field) {
		super(Set.of());
		this.field = field;
		this.add(this.field);
	}

	public AZChipField(final String label, final C field) {
		this(field);
		this.setLabel(label);
	}

	public C getField() {
		return this.field;
	}

	public void refreshItems() {
		this.setPresentationValue(this.getValue());
	}

	public ItemLabelGenerator<T> getItemLabelGenerator() {
		return this.itemLabelGenerator;
	}

	public void setItemLabelGenerator(final ItemLabelGenerator<T> itemLabelGenerator) {
		requireNonNull(itemLabelGenerator, "The item label generator can not be null");
		this.itemLabelGenerator = itemLabelGenerator;
	}

	public ItemChipGenerator<T> getItemChipGenerator() {
		return this.itemChipGenerator;
	}

	public void setItemChipGenerator(final ItemChipGenerator<T> itemChipGenerator) {
		requireNonNull(itemChipGenerator, "The item chip generator can not be null");
		this.itemChipGenerator = itemChipGenerator;
	}

	@Override
	protected Set<T> generateModelValue() {
		this.field.getOptionalValue().ifPresent(this::addItem);
		this.field.clear();
		return new HashSet<>(this.items.keySet());
	}

	@Override
	protected void setPresentationValue(final Set<T> newPresentationValue) {
		if (this.items != null) {
			this.remove(this.items.values().stream().toArray(Component[]::new));
		}
		this.items = new LinkedHashMap<>();
		newPresentationValue.forEach(this::addItemUnsafe);
	}

	@Override
	public Set<T> getEmptyValue() {
		return Set.of();
	}

	@Override
	protected void updateValue() {
		// from client-side when `this.field` value changes (the only child field of this custom-field)
		super.updateValue();
	}

	protected String generateChipLabel(final T item) {
		return this.getItemLabelGenerator().apply(item);
	}

	private void addItem(final T itemValue) {
		if (this.items == null) {
			this.items = new LinkedHashMap<>();
		}
		if (!this.items.containsKey(itemValue)) {
			this.addItemUnsafe(itemValue);
			// super.updateValue();
		}
	}

	private void addItemUnsafe(final T item) {
		requireNonNull(item);
		final Component chip = this.getItemChipGenerator().apply(item, () -> this.removeItem(item));
		this.items.put(item, chip);
		this.add(chip);
	}

	private void removeItem(final T item) {
		final Component chip = this.items.remove(item);
		if (chip != null) {
			this.remove(chip);
		}
		super.updateValue();
	}
}
