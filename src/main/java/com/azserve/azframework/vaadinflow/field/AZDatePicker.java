package com.azserve.azframework.vaadinflow.field;

import java.time.LocalDate;

import com.azserve.azframework.vaadinflow.component.fluent.FluentDatePicker;
import com.vaadin.flow.component.datepicker.DatePicker;

public class AZDatePicker extends DatePicker
		implements
		FluentDatePicker<AZDatePicker> {

	private static final long serialVersionUID = -517631464330670421L;

	public AZDatePicker() {
		super();
		this.setAutoOpen(false);
	}

	public AZDatePicker(final LocalDate initialDate, final ValueChangeListener<ComponentValueChangeEvent<DatePicker, LocalDate>> listener) {
		super(initialDate, listener);
		this.setAutoOpen(false);
	}

	public AZDatePicker(final LocalDate initialDate) {
		super(initialDate);
		this.setAutoOpen(false);
	}

	public AZDatePicker(final String label, final LocalDate initialDate, final ValueChangeListener<ComponentValueChangeEvent<DatePicker, LocalDate>> listener) {
		super(label, initialDate, listener);
		this.setAutoOpen(false);
	}

	public AZDatePicker(final String label, final LocalDate initialDate) {
		super(label, initialDate);
		this.setAutoOpen(false);
	}

	public AZDatePicker(final String label, final ValueChangeListener<ComponentValueChangeEvent<DatePicker, LocalDate>> listener) {
		super(label, listener);
		this.setAutoOpen(false);
	}

	public AZDatePicker(final String label) {
		super(label);
		this.setAutoOpen(false);
	}

	public AZDatePicker(final ValueChangeListener<ComponentValueChangeEvent<DatePicker, LocalDate>> listener) {
		super(listener);
		this.setAutoOpen(false);
	}
}
