package com.azserve.azframework.vaadinflow.field;

import java.util.Collection;
import java.util.Set;

import com.azserve.azframework.vaadinflow.component.fluent.FluentCommonInputField;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasLazyDataView;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasListDataView;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasThemeVariant;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.ItemLabelGenerator;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.combobox.MultiSelectComboBox;
import com.vaadin.flow.component.combobox.MultiSelectComboBoxI18n;
import com.vaadin.flow.component.combobox.MultiSelectComboBoxVariant;
import com.vaadin.flow.component.combobox.dataview.ComboBoxLazyDataView;
import com.vaadin.flow.component.combobox.dataview.ComboBoxListDataView;
import com.vaadin.flow.data.renderer.Renderer;

public class AZMultiSelectComboBox<T> extends MultiSelectComboBox<T> implements
		FluentCommonInputField<MultiSelectComboBox<T>, Set<T>, AZMultiSelectComboBox<T>>,
		FluentHasListDataView<AZMultiSelectComboBox<T>, T, ComboBoxListDataView<T>>,
		FluentHasLazyDataView<AZMultiSelectComboBox<T>, T, String, ComboBoxLazyDataView<T>>,
		FluentHasThemeVariant<AZMultiSelectComboBox<T>, MultiSelectComboBoxVariant> {

	private static final long serialVersionUID = 9116829556090459484L;

	public AZMultiSelectComboBox() {
		super();
	}

	public AZMultiSelectComboBox(final int pageSize) {
		super(pageSize);
	}

	public AZMultiSelectComboBox(final String label, final Collection<T> items) {
		super(label, items);
	}

	@SafeVarargs
	public AZMultiSelectComboBox(final String label, final T... items) {
		super(label, items);
	}

	@SafeVarargs
	public AZMultiSelectComboBox(final String label, final ValueChangeListener<ComponentValueChangeEvent<MultiSelectComboBox<T>, Set<T>>> listener, final T... items) {
		super(label, listener, items);
	}

	public AZMultiSelectComboBox(final String label, final ValueChangeListener<ComponentValueChangeEvent<MultiSelectComboBox<T>, Set<T>>> listener) {
		super(label, listener);
	}

	public AZMultiSelectComboBox(final String label) {
		super(label);
	}

	public AZMultiSelectComboBox(final ValueChangeListener<ComponentValueChangeEvent<MultiSelectComboBox<T>, Set<T>>> listener) {
		super(listener);
	}

	public AZMultiSelectComboBox<T> withAllowCustomValue(final boolean allowCustomValue) {
		this.setAllowCustomValue(allowCustomValue);
		return this;
	}

	public AZMultiSelectComboBox<T> withAutoOpen(final boolean autoOpen) {
		this.setAutoOpen(autoOpen);
		return this;
	}

	public AZMultiSelectComboBox<T> withAutoExpand(final AutoExpandMode autoExpandMode) {
		this.setAutoExpand(autoExpandMode);
		return this;
	}

	public AZMultiSelectComboBox<T> withI18n(final MultiSelectComboBoxI18n i18n) {
		this.setI18n(i18n);
		return this;
	}

	public AZMultiSelectComboBox<T> withItemLabelGenerator(final ItemLabelGenerator<T> itemLabelGenerator) {
		this.setItemLabelGenerator(itemLabelGenerator);
		return this;
	}

	public AZMultiSelectComboBox<T> withPageSize(final int pageSize) {
		this.setPageSize(pageSize);
		return this;
	}

	public AZMultiSelectComboBox<T> withPopupWidth(final String popupWidth) {
		this.getStyle().set("--vaadin-multi-select-combo-box-overlay-width", popupWidth);
		return this;
	}

	public AZMultiSelectComboBox<T> withPopupWidth(final float width, final Unit unit) {
		return this.withPopupWidth(HasSize.getCssSize(width, unit));
	}

	public AZMultiSelectComboBox<T> withRenderer(final Renderer<T> renderer) {
		this.setRenderer(renderer);
		return this;
	}

	public AZMultiSelectComboBox<T> withSelectedItemsOnTop(final boolean selectedItemsOnTop) {
		this.setSelectedItemsOnTop(selectedItemsOnTop);
		return this;
	}

}
