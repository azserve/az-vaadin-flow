package com.azserve.azframework.vaadinflow.field;

import com.azserve.azframework.vaadinflow.component.fluent.FluentClickNotifier;
import com.azserve.azframework.vaadinflow.component.fluent.FluentComponent;
import com.azserve.azframework.vaadinflow.component.fluent.FluentFocusable;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasLabel;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasSize;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasStyle;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasTooltip;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasValueAndElement;
import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.checkbox.Checkbox;

@SuppressWarnings("serial")
public class AZCheckbox extends Checkbox implements
		FluentComponent<AZCheckbox>,
		FluentFocusable<Checkbox, AZCheckbox>,
		FluentHasLabel<AZCheckbox>,
		FluentHasSize<AZCheckbox>,
		FluentHasStyle<AZCheckbox>,
		FluentHasTooltip<AZCheckbox>,
		FluentHasValueAndElement<AZCheckbox, ComponentValueChangeEvent<Checkbox, Boolean>, Boolean>,
		FluentClickNotifier<Checkbox, AZCheckbox> {

	public AZCheckbox() {
		super();
	}

	public AZCheckbox(final boolean initialValue) {
		super(initialValue);
	}

	public AZCheckbox(final String labelText, final boolean initialValue) {
		super(labelText, initialValue);
	}

	public AZCheckbox(final String label, final ValueChangeListener<ComponentValueChangeEvent<Checkbox, Boolean>> listener) {
		super(label, listener);
	}

	public AZCheckbox(final String labelText) {
		super(labelText);
	}

	public AZCheckbox withLabelComponent(final Component component) {
		this.setLabelComponent(component);
		return this;
	}

	public boolean isChecked() {
		return this.getValue().booleanValue();
	}

	public void setChecked(final boolean checked) {
		this.setValue(Boolean.valueOf(checked));
	}

	public AZCheckbox withChecked(final boolean checked) {
		this.setChecked(checked);
		return this;
	}
}
