package com.azserve.azframework.vaadinflow.field;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

import com.azserve.azframework.vaadinflow.component.fluent.FluentCustomField;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.timepicker.TimePicker;

@SuppressWarnings("serial")
public class AZDateTimePickerIT extends CustomField<LocalDateTime>
		implements FluentCustomField<LocalDateTime, AZDateTimePickerIT> {

	private final AZDatePickerIT datePicker = new AZDatePickerIT();
	private final TimePicker timePicker = new TimePicker();
	private LocalDateTime value;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public AZDateTimePickerIT() {
		this.getStyle().set("white-space", "nowrap");
		this.datePicker.getStyle().set("margin-right", "2px");
		this.datePicker.setWidth("9em");
		this.timePicker.setWidth("6em");
		this.add(new Div(this.datePicker, this.timePicker));

		final ValueChangeListener listener = e -> {
			if (e.isFromClient()) {
				this.updateFromFields();
			}
		};
		this.datePicker.addValueChangeListener(e -> {
			if (e.getValue() != null) {
				if (e.isFromClient()) {
					if (this.timePicker.isEmpty()) {
						if (LocalDate.now().equals(e.getValue())) {
							this.timePicker.setValue(LocalTime.now().truncatedTo(ChronoUnit.MINUTES));
						} else {
							this.timePicker.setValue(LocalTime.MIDNIGHT);
						}
					}
				} else if (this.timePicker.isEmpty()) {
					this.timePicker.setValue(LocalTime.now().truncatedTo(ChronoUnit.MINUTES));
				}
			} else {
				this.timePicker.clear();
			}
		});
		this.timePicker.addValueChangeListener(e -> {
			if (e.getValue() != null) {
				if (this.datePicker.isEmpty()) {
					this.datePicker.setValue(LocalDate.now());
				}
			} else {
				this.datePicker.clear();
			}
		});
		this.timePicker.addValueChangeListener(listener);
		this.datePicker.addValueChangeListener(listener);
	}

	public AZDateTimePickerIT(final String label) {
		this();
		this.setLabel(label);
	}

	@Override
	public void setReadOnly(final boolean readOnly) {
		super.setReadOnly(readOnly);
		this.datePicker.setReadOnly(readOnly);
		this.timePicker.setReadOnly(readOnly);
	}

	public boolean isTimeVisible() {
		return this.timePicker.isVisible();
	}

	public void setTimeVisible(final boolean timeVisible) {
		this.timePicker.setVisible(timeVisible);
	}

	public void setMinDate(final LocalDate minDate) {
		this.datePicker.setMin(minDate);
	}

	public LocalDate getMinDate() {
		return this.datePicker.getMin();
	}

	public void setMaxDate(final LocalDate maxDate) {
		this.datePicker.setMax(maxDate);
	}

	public LocalDate getMaxDate() {
		return this.datePicker.getMax();
	}

	@Override
	protected LocalDateTime generateModelValue() {
		return this.value;
	}

	@Override
	protected void updateValue() {
		// NO-OP
	}

	@Override
	protected void setPresentationValue(final LocalDateTime newValue) {
		if (newValue != null) {
			this.datePicker.setValue(newValue.toLocalDate());
			this.timePicker.setValue(newValue.toLocalTime());
		} else {
			this.datePicker.clear();
			this.timePicker.clear();
		}
		this.value = newValue;
	}

	private void updateFromFields() {
		try {
			this.value = LocalDateTime.of(this.datePicker.getValue(), this.timePicker.getValue());
		} catch (@SuppressWarnings("unused") final Exception ex) {
			this.value = null;
		}
		this.setModelValue(this.value, true);
	}
}
