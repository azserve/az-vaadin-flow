package com.azserve.azframework.vaadinflow.field;

import com.azserve.azframework.vaadinflow.component.fluent.FluentCommonField;
import com.azserve.azframework.vaadinflow.component.fluent.FluentFocusable;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasPrefixAndSuffix;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ItemLabelGenerator;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.select.SelectVariant;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.function.SerializablePredicate;

@SuppressWarnings("serial")
public class AZSelect<T> extends Select<T>
		implements
		FluentCommonField<Select<T>, T, AZSelect<T>>,
		FluentFocusable<Select<T>, AZSelect<T>>,
		FluentHasPrefixAndSuffix<AZSelect<T>> {

	public AZSelect() {
		super();
	}

	public AZSelect(final String label) {
		super();
		this.setLabel(label);
	}

	@SafeVarargs
	public AZSelect(final String label, final T... items) {
		super();
		this.setLabel(label);
		this.setItems(items);
	}

	public AZSelect<T> withRenderer(final ComponentRenderer<? extends Component, T> renderer) {
		this.setRenderer(renderer);
		return this;
	}

	public AZSelect<T> withTextRenderer(final ItemLabelGenerator<T> itemLabelGenerator) {
		this.setTextRenderer(itemLabelGenerator);
		return this;
	}

	public AZSelect<T> withEmptySelectionAllowed(final boolean emptySelectionAllowed) {
		this.setEmptySelectionAllowed(emptySelectionAllowed);
		return this;
	}

	public AZSelect<T> withEmptySelectionCaption(final String emptySelectionCaption) {
		this.setEmptySelectionCaption(emptySelectionCaption);
		return this;
	}

	public AZSelect<T> withItemEnabledProvider(final SerializablePredicate<T> itemEnabledProvider) {
		this.setItemEnabledProvider(itemEnabledProvider);
		return this;
	}

	public AZSelect<T> withItemLabelGenerator(final ItemLabelGenerator<T> itemLabelGenerator) {
		this.setItemLabelGenerator(itemLabelGenerator);
		return this;
	}

	public AZSelect<T> withThemeVariants(final SelectVariant... variants) {
		this.addThemeVariants(variants);
		return this;
	}
}
