package com.azserve.azframework.vaadinflow.field;

import com.azserve.azframework.vaadinflow.component.fluent.FluentHasEnabled;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasMenuItems;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasSize;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasStyle;
import com.azserve.azframework.vaadinflow.component.fluent.FluentHasTheme;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.menubar.MenuBar;

public class AZMenuBar extends MenuBar implements
		FluentHasMenuItems<AZMenuBar>,
		FluentHasSize<AZMenuBar>,
		FluentHasStyle<AZMenuBar>,
		FluentHasTheme<AZMenuBar>,
		FluentHasEnabled<AZMenuBar> {

	private static final long serialVersionUID = 5016172005276598669L;

	public AZMenuBar withItem(final String text) {
		this.addItem(text);
		return this;
	}

	public AZMenuBar withItem(final String text, final String tooltipText) {
		this.addItem(text, tooltipText);
		return this;
	}

	public AZMenuBar withItem(final Component component, final String tooltipText, final ComponentEventListener<ClickEvent<MenuItem>> clickListener) {
		this.addItem(component, tooltipText, clickListener);
		return this;
	}

	public AZMenuBar withItem(final String text, final String tooltipText, final ComponentEventListener<ClickEvent<MenuItem>> clickListener) {
		this.addItem(text, tooltipText, clickListener);
		return this;
	}

	public AZMenuBar withI18n(final MenuBarI18n i18n) {
		this.setI18n(i18n);
		return this;
	}

	public AZMenuBar withOpenOnHover(final boolean openOnHover) {
		this.setOpenOnHover(openOnHover);
		return this;
	}

	public AZMenuBar withReverseCollapseOrder(final boolean reverseCollapseOrder) {
		this.setReverseCollapseOrder(reverseCollapseOrder);
		return this;
	}
}
