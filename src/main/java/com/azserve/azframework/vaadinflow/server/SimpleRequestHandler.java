package com.azserve.azframework.vaadinflow.server;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.servlet.ServletContext;

import com.azserve.azframework.common.annotation.Nullable;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.server.RequestHandler;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.StreamResourceWriter;
import com.vaadin.flow.server.VaadinRequest;
import com.vaadin.flow.server.VaadinResponse;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinServletRequest;
import com.vaadin.flow.server.VaadinServletService;
import com.vaadin.flow.server.VaadinSession;

@SuppressWarnings("serial")
public class SimpleRequestHandler implements RequestHandler {

	private static final Logger LOGGER = System.getLogger(SimpleRequestHandler.class.getName());

	public static SimpleRequestHandler ofPath(final String relativePath, final @Nullable Component lifeCycleComponent) {
		final VaadinSession session = Objects.requireNonNull(VaadinSession.getCurrent(), "VaadinSession is still not available");
		return ofPath(session, relativePath, lifeCycleComponent);
	}

	public static SimpleRequestHandler ofPath(final VaadinSession session, final String relativePath, final @Nullable Component lifeCycleComponent) {
		final VaadinService service = session.getService();
		if (!(service instanceof VaadinServletService)) {
			throw new IllegalStateException("Session not bound to a servlet");
		}
		final String contextPath = ((VaadinServletService) service).getServlet().getServletContext().getContextPath();
		final SimpleRequestHandler requestHandler = new SimpleRequestHandler(session, contextPath, relativePath);
		if (lifeCycleComponent != null) {
			lifeCycleComponent.addDetachListener(e -> requestHandler.unregister());
		}
		return requestHandler;
	}

	private class StreamResourceData {

		private final StreamResource resource;
		private final boolean singleRequest;

		StreamResourceData(final StreamResource resource, final boolean singleRequest) {
			this.resource = resource;
			this.singleRequest = singleRequest;
		}

		@Override
		public String toString() {
			return "StreamResourceData [resource=" + this.resource + ", singleRequest=" + this.singleRequest + "]";
		}
	}

	private final VaadinSession session;
	private final String contextPath;
	private final Map<String, StreamResourceData> resources = new HashMap<>();
	private final String subPath;

	private SimpleRequestHandler(final VaadinSession session, final String contextPath, final String relativePath) {
		Objects.requireNonNull(session, "session is null");
		Objects.requireNonNull(relativePath, "contextPath is null");
		Objects.requireNonNull(relativePath, "relativePath is null");
		if (relativePath.startsWith("/")) {
			throw new IllegalArgumentException("relativePath must be relative");
		}
		this.session = session;
		this.contextPath = contextPath;
		this.subPath = "/" + relativePath;

		this.register();
	}

	public void register() {
		if (this.session.getRequestHandlers().contains(this)) {
			throw new IllegalStateException("This request handler is already registered");
		}
		LOGGER.log(Level.DEBUG, "Registering request handler {0}", this);
		this.session.addRequestHandler(this);
	}

	public void unregister() {
		LOGGER.log(Level.DEBUG, "Unregistering request handler {0}", this);
		this.clearResources();
		this.session.removeRequestHandler(this);
	}

	public String registerResource(final StreamResource resource, final boolean singleRequest) {
		final String url = this.generateUrl(resource.getName());
		this.resources.put(url, new StreamResourceData(resource, singleRequest));
		return url;
	}

	public void clearResources() {
		this.resources.clear();
	}

	public String generateUrl(final String path) {
		if (path.startsWith("/")) {
			throw new IllegalArgumentException("Resource path must be relative");
		}
		return this.contextPath + this.subPath + path;
	}

	@Override
	public boolean handleRequest(final VaadinSession requestSession, final VaadinRequest request, final VaadinResponse response) throws IOException {
		final String pathInfo = this.contextPath + request.getPathInfo();
		if (!pathInfo.startsWith(this.contextPath + this.subPath)) {
			return false;
		}
		final StreamResourceData resourceData = this.resources.get(pathInfo);
		LOGGER.log(Level.INFO, "Handling request {0} for {1}", resourceData, pathInfo);
		if (resourceData == null) {
			return false;
		}
		final StreamResource resource = resourceData.resource;
		StreamResourceWriter writer;
		requestSession.lock();
		try {
			final ServletContext context = ((VaadinServletRequest) request).getServletContext();
			response.setContentType(resource.getContentTypeResolver().apply(resource, context));
			response.setCacheTime(resource.getCacheTime());
			writer = resource.getWriter();
			if (writer == null) {
				throw new IOException("Resource produced null stream");
			}
		} finally {
			if (resourceData.singleRequest) {
				this.resources.remove(pathInfo);
			}
			requestSession.unlock();
		}
		try (OutputStream outputStream = response.getOutputStream()) {
			try {
				writer.accept(outputStream, requestSession);
			} catch (final Exception ex) {
				LOGGER.log(Level.ERROR, "Error writing http response, path = {0}", pathInfo, ex);
				throw new IOException(ex);
			}
		}
		return true;
	}
}
