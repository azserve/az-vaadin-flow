package com.azserve.azframework.vaadinflow.server;

import javax.servlet.ServletException;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.server.VaadinServletService;

public final class FlowServerUtils {

	public static void logout() throws ServletException {
		final UI ui = UI.getCurrent();
		ui.getPage().reload();
		ui.getSession().close();
		VaadinServletService.getCurrentServletRequest().logout();
	}

	private FlowServerUtils() {
		throw new AssertionError();
	}
}
