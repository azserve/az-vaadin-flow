package com.azserve.azframework.vaadinflow.router;

import static java.util.Objects.requireNonNull;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.azserve.azframework.common.annotation.NonNullByDefault;
import com.azserve.azframework.common.annotation.Nullable;
import com.azserve.azframework.display.parameter.DisplayParameter;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.page.Page;
import com.vaadin.flow.internal.JsonUtils;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.router.RouteParameters;

import elemental.json.Json;
import elemental.json.JsonArray;

@NonNullByDefault
public final class RoutingUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(RoutingUtils.class);

	public static void navigate(final UI ui, final Class<? extends Component> navigationTarget, final Map<String, String> params) {
		navigate(ui, navigationTarget, QueryParameters.simple(params));
	}

	public static void navigate(final UI ui, final Class<? extends Component> navigationTarget, final QueryParameters queryParameters) {
		requireNonNull(navigationTarget);
		requireNonNull(queryParameters);
		final RouteConfiguration configuration = RouteConfiguration.forRegistry(ui.getInternals().getRouter().getRegistry());
		ui.navigate(configuration.getUrl(navigationTarget), queryParameters);
	}

	public static void navigate(final UI ui, final Class<? extends Component> navigationTarget, final RouteParameters routeParameters, final QueryParameters queryParameters) {
		requireNonNull(navigationTarget);
		requireNonNull(routeParameters);
		requireNonNull(queryParameters);
		final RouteConfiguration configuration = RouteConfiguration.forRegistry(ui.getInternals().getRouter().getRegistry());
		ui.navigate(configuration.getUrl(navigationTarget, routeParameters), queryParameters);
	}

	public static Map<String, List<String>> decodeParameters(final Map<String, List<String>> parameters) {
		final Map<String, List<String>> params = new HashMap<>();
		for (final Entry<String, List<String>> entry : parameters.entrySet()) {
			final String key = entry.getKey();
			final List<String> values = new ArrayList<>(entry.getValue().size());
			for (final String value : entry.getValue()) {
				try {
					values.add(URLDecoder.decode(value, StandardCharsets.UTF_8.name()));
				} catch (final UnsupportedEncodingException ex) {
					LOGGER.warn("Cannot decode parameter {}", key, ex);
				}
			}
			params.put(key, values);
		}
		return params;
	}

	public static void updateQueryParameters(
			final Page page,
			final QueryParameters parametersToAdd,
			final Collection<String> parametersToRemove) {
		final JsonArray toRemove = Stream.concat(parametersToAdd.getParameters().keySet().stream(),
				parametersToRemove.stream())
				.map(Json::create).collect(JsonUtils.asArray());

		page.executeJs("const url = new URL(location);$0.forEach(key => url.searchParams.delete(key));"
				+ "if ($1) {const addSearchParams = new URLSearchParams($1);addSearchParams.forEach((value,key) => url.searchParams.append(key,value));}"
				+ "history.replaceState({}, \"\", url);", toRemove, parametersToAdd.getQueryString());
	}

	public static void updateDisplayQueryParameters(
			final Page page,
			final QueryParameters parametersToAdd,
			final Collection<DisplayParameter<?>> parametersToRemove) {

		updateQueryParameters(
				page,
				parametersToAdd,
				parametersToRemove.stream().map(DisplayParameter::getName).collect(Collectors.toList()));
	}

	public static <T> void updateQueryParameter(final UI ui, final DisplayParameter<T> parameter, final @Nullable T value) {
		updateQueryParameter(ui, parameter.getName(), value == null ? List.of() : List.of(parameter.serialize(value)));
	}

	public static void updateQueryParameter(final UI ui, final String key, final List<String> values) {
		ui.getPage().executeJs("const url = new URL(location);"
				+ "url.searchParams.delete($0);"
				+ "$1.forEach(param => url.searchParams.append($0, param));"
				+ "history.replaceState({}, \"\", url);", key, JsonUtils.listToJson(values));
	}

	private RoutingUtils() {
		throw new AssertionError();
	}
}
