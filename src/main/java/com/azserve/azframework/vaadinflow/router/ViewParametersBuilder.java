package com.azserve.azframework.vaadinflow.router;

import static java.util.stream.Collectors.toMap;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.azserve.azframework.common.lang.StringUtils;
import com.azserve.azframework.display.parameter.DisplayParameter;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.RouteParameters;

public class ViewParametersBuilder {

	private final Map<String, List<String>> parameters = new HashMap<>();

	public ViewParametersBuilder() {}

	public ViewParametersBuilder add(final String key, final String value, final boolean encode) {
		if (!StringUtils.isNullOrBlank(value)) {
			final List<String> list = this.parameters.computeIfAbsent(key, k -> new ArrayList<>());
			if (encode) {
				try {
					list.add(URLEncoder.encode(value, StandardCharsets.UTF_8.name()));
				} catch (final UnsupportedEncodingException ex) {
					throw new RuntimeException(ex);
				}
			} else {
				list.add(value);
			}
		}
		return this;
	}

	public <T> ViewParametersBuilder add(final DisplayParameter<T> param, final T value, final boolean encode) {
		return this.add(param.getName(), value == null ? null : param.serialize(value), encode);
	}

	public ViewParametersBuilder add(final DisplayParameter<String> param, final String value) {
		return this.add(param.getName(), value == null ? null : param.serialize(value), true);
	}

	public <T> ViewParametersBuilder add(final DisplayParameter<T> param, final T value) {
		return this.add(param.getName(), value == null ? null : param.serialize(value), false);
	}

	public ViewParametersBuilder add(final String key, final String value) {
		return this.add(key, value, true);
	}

	public ViewParametersBuilder add(final String key, final Object value) {
		return this.add(key, Objects.toString(value, null), false);
	}

	public ViewParametersBuilder addAll(final String key, final Collection<String> values, final boolean encode) {
		if (values != null && !values.isEmpty()) {
			final List<String> list = this.parameters.computeIfAbsent(key, k -> new ArrayList<>());
			if (encode) {
				for (final String value : values) {
					if (!StringUtils.isNullOrBlank(value)) {
						try {
							list.add(URLEncoder.encode(value, StandardCharsets.UTF_8.name()));
						} catch (final UnsupportedEncodingException ex) {
							throw new RuntimeException(ex);
						}
					}
				}
			} else {
				for (final String value : values) {
					if (!StringUtils.isNullOrBlank(value)) {
						list.add(value);
					}
				}
			}
		}
		return this;
	}

	public ViewParametersBuilder addAll(final String key, final Collection<String> values) {
		return this.addAll(key, values, true);
	}

	public ViewParametersBuilder addAllObj(final String key, final Collection<?> values) {
		if (values != null && !values.isEmpty()) {
			final List<String> list = this.parameters.computeIfAbsent(key, k -> new ArrayList<>());
			for (final Object value : values) {
				if (value != null) {
					list.add(value.toString());
				}
			}
		}
		return this;
	}

	public ViewParametersBuilder addAll(final DisplayParameter<String> param, final Collection<String> values, final boolean encode) {
		return this.addAll(param.getName(), values, encode);
	}

	public ViewParametersBuilder addAll(final DisplayParameter<String> param, final Collection<String> values) {
		return this.addAll(param, values, true);
	}

	public <T> ViewParametersBuilder addAllObj(final DisplayParameter<T> param, final Collection<T> values) {
		if (values != null && !values.isEmpty()) {
			final List<String> list = this.parameters.computeIfAbsent(param.getName(), k -> new ArrayList<>());
			for (final T value : values) {
				if (value != null) {
					list.add(param.serialize(value));
				}
			}
		}
		return this;
	}

	/**
	 * Creates a query parameters holder based on
	 * the current builder state map.
	 */
	public QueryParameters buildQueryParameters() {
		return new QueryParameters(this.parameters);
	}

	/**
	 * Creates a route parameters holder based on
	 * the current builder state map.
	 * <p>
	 * NOTE: since a route parameter can not have a list of
	 * values an {@code IllegalStateException} is thrown
	 * in case of more than one value.
	 * In case of zero values the parameter is omitted.
	 */
	public RouteParameters buildRouteParameters() {
		return new RouteParameters(this.parameters.entrySet()
				.stream()
				.filter(entry -> !entry.getValue().isEmpty())
				.collect(toMap(Map.Entry::getKey, entry -> {
					if (entry.getValue().size() > 1) {
						throw new IllegalStateException("More than one value found");
					}
					return entry.getValue().get(0);
				})));
	}
}
