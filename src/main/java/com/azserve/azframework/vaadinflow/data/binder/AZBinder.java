package com.azserve.azframework.vaadinflow.data.binder;

import java.lang.annotation.Target;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;

import javax.validation.constraints.NotBlank;
import javax.validation.metadata.BeanDescriptor;
import javax.validation.metadata.ConstraintDescriptor;
import javax.validation.metadata.PropertyDescriptor;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.lang.StringUtils;
import com.azserve.azframework.common.util.BeanProperty;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.Focusable;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.HasEnabled;
import com.vaadin.flow.component.HasText;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.data.binder.BeanPropertySet.NestedBeanPropertyDefinition;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.BindingValidationStatusHandler;
import com.vaadin.flow.data.binder.ErrorLevel;
import com.vaadin.flow.data.binder.ErrorMessageProvider;
import com.vaadin.flow.data.binder.PropertyDefinition;
import com.vaadin.flow.data.binder.RequiredFieldConfigurator;
import com.vaadin.flow.data.binder.RequiredFieldConfiguratorUtil;
import com.vaadin.flow.data.binder.Validator;
import com.vaadin.flow.data.converter.Converter;
import com.vaadin.flow.data.validator.BeanValidator;
import com.vaadin.flow.function.SerializableFunction;
import com.vaadin.flow.function.SerializablePredicate;

@SuppressWarnings("serial")
public class AZBinder<BEAN> extends BeanValidationBinder<BEAN> {

	public interface AZBindingBuilder<BEAN, TARGET> extends BindingBuilder<BEAN, TARGET> {

		// XXX all methods are reimplemented in order to archieve the correct return type

		@Override
		AZBindingBuilder<BEAN, TARGET> withValidator(Validator<? super TARGET> validator);

		@Override
		default AZBindingBuilder<BEAN, TARGET> withValidator(final SerializablePredicate<? super TARGET> predicate, final String message) {
			return (AZBindingBuilder<BEAN, TARGET>) BindingBuilder.super.withValidator(predicate, message);
		}

		@Override
		default AZBindingBuilder<BEAN, TARGET> withValidator(final SerializablePredicate<? super TARGET> predicate, final String message, final ErrorLevel errorLevel) {
			return (AZBindingBuilder<BEAN, TARGET>) BindingBuilder.super.withValidator(predicate, message, errorLevel);
		}

		@Override
		default AZBindingBuilder<BEAN, TARGET> withValidator(final SerializablePredicate<? super TARGET> predicate, final ErrorMessageProvider errorMessageProvider) {
			return (AZBindingBuilder<BEAN, TARGET>) BindingBuilder.super.withValidator(predicate, errorMessageProvider);
		}

		@Override
		default AZBindingBuilder<BEAN, TARGET> withValidator(final SerializablePredicate<? super TARGET> predicate, final ErrorMessageProvider errorMessageProvider, final ErrorLevel errorLevel) {
			return (AZBindingBuilder<BEAN, TARGET>) BindingBuilder.super.withValidator(predicate, errorMessageProvider, errorLevel);
		}

		@Override
		<NEWTARGET> AZBindingBuilder<BEAN, NEWTARGET> withConverter(Converter<TARGET, NEWTARGET> converter);

		@Override
		default <NEWTARGET> AZBindingBuilder<BEAN, NEWTARGET> withConverter(final SerializableFunction<TARGET, NEWTARGET> toModel, final SerializableFunction<NEWTARGET, TARGET> toPresentation) {
			return (AZBindingBuilder<BEAN, NEWTARGET>) BindingBuilder.super.withConverter(toModel, toPresentation);
		}

		@Override
		default <NEWTARGET> AZBindingBuilder<BEAN, NEWTARGET> withConverter(final SerializableFunction<TARGET, NEWTARGET> toModel, final SerializableFunction<NEWTARGET, TARGET> toPresentation, final String errorMessage) {
			return (AZBindingBuilder<BEAN, NEWTARGET>) BindingBuilder.super.withConverter(toModel, toPresentation, errorMessage);
		}

		@Override
		default AZBindingBuilder<BEAN, TARGET> withNullRepresentation(final TARGET nullRepresentation) {
			return (AZBindingBuilder<BEAN, TARGET>) BindingBuilder.super.withNullRepresentation(nullRepresentation);
		}

		@Override
		default AZBindingBuilder<BEAN, TARGET> withStatusLabel(final HasText label) {
			return (AZBindingBuilder<BEAN, TARGET>) BindingBuilder.super.withStatusLabel(label);
		}

		@Override
		AZBindingBuilder<BEAN, TARGET> withValidationStatusHandler(BindingValidationStatusHandler handler);

		@Override
		default AZBindingBuilder<BEAN, TARGET> asRequired(final String errorMessage) {
			return (AZBindingBuilder<BEAN, TARGET>) BindingBuilder.super.asRequired(errorMessage);
		}

		@Override
		default AZBindingBuilder<BEAN, TARGET> asRequired() {
			return (AZBindingBuilder<BEAN, TARGET>) BindingBuilder.super.asRequired();
		}

		@Override
		AZBindingBuilder<BEAN, TARGET> asRequired(ErrorMessageProvider errorMessageProvider);

		@Override
		AZBindingBuilder<BEAN, TARGET> asRequired(Validator<TARGET> customRequiredValidator);

		default Binding<BEAN, TARGET> bind(final BeanProperty<? super BEAN, ?, ?, TARGET> property) {
			return this.bind(property.getNestedName());
		}

		default Binding<BEAN, TARGET> bindReadOnly(final BeanProperty<? super BEAN, ?, ?, TARGET> property) {
			return this.bindReadOnly(property.getNestedName());
		}
	}

	public static class AZBindingBuilderImpl<BEAN, FIELDVALUE, TARGET>
			extends BindingBuilderImpl<BEAN, FIELDVALUE, TARGET>
			implements AZBindingBuilder<BEAN, TARGET> {

		protected AZBindingBuilderImpl(final Binder<BEAN> binder, final HasValue<?, FIELDVALUE> field, final Converter<FIELDVALUE, TARGET> converterValidatorChain, final BindingValidationStatusHandler statusHandler) {
			super(binder, field, converterValidatorChain, statusHandler);
		}

		@Override
		public AZBindingBuilder<BEAN, TARGET> asRequired(final ErrorMessageProvider errorMessageProvider) {
			return (AZBindingBuilder<BEAN, TARGET>) super.asRequired(errorMessageProvider);
		}

		@Override
		public AZBindingBuilder<BEAN, TARGET> asRequired(final Validator<TARGET> customRequiredValidator) {
			return (AZBindingBuilder<BEAN, TARGET>) super.asRequired(customRequiredValidator);
		}

		@Override
		public <NEWTARGET> AZBindingBuilder<BEAN, NEWTARGET> withConverter(final Converter<TARGET, NEWTARGET> converter) {
			return (AZBindingBuilder<BEAN, NEWTARGET>) super.withConverter(converter);
		}

		@Override
		public AZBindingBuilder<BEAN, TARGET> withValidationStatusHandler(final BindingValidationStatusHandler handler) {
			return (AZBindingBuilder<BEAN, TARGET>) super.withValidationStatusHandler(handler);
		}

		@Override
		public AZBindingBuilder<BEAN, TARGET> withValidator(final Validator<? super TARGET> validator) {
			return (AZBindingBuilder<BEAN, TARGET>) super.withValidator(validator);
		}
	}

	public final class FocusableFields {

		public void focusFirst() {
			this.all().findFirst().ifPresent(Focusable::focus);
		}

		public void focusFirstEmptyRequired() {
			this.emptyRequired().findFirst().ifPresent(Focusable::focus);
		}

		/**
		 * Returns the fields this binder has been bound to which are
		 * focusable, enabled and not read-only.
		 * <p>
		 * Usage examples:
		 *
		 * <pre>{@code
		 * binder.getFocusableFields()
		 * 		.all()
		 * 		.findFirst()
		 * 		.ifPresent(Focusable::focus);
		 *
		 * binder.getFocusableFields()
		 * 		.all()
		 * 		.filter(field -> field != fieldToExcludeFromFocus)
		 * 		.findFirst()
		 * 		.ifPresent(Focusable::focus);
		 * }</pre>
		 */
		public Stream<Focusable<?>> all() {
			return AZBinder.this.getBindings().stream()
					.filter(this::isFieldFocusableAndWritable)
					.map(Binding::getField)
					.<Focusable<?>> map(Focusable.class::cast);
		}

		/**
		 * Returns the fields this binder has been bound to which are
		 * focusable, enabled and not read-only.
		 * <p>
		 * Usage examples:
		 *
		 * <pre>{@code
		 * binder.getFocusableFields()
		 * 		.filtered(field -> field != fieldToExcludeFromFocus && !field.isEmpty())
		 * 		.findFirst()
		 * 		.ifPresent(Focusable::focus);
		 * }</pre>
		 */
		public Stream<Focusable<?>> filtered(final Predicate<HasValue<?, ?>> filter) {
			return AZBinder.this.getBindings().stream()
					.filter(this::isFieldFocusableAndWritable)
					.filter(binding -> filter.test(binding.getField()))
					.map(Binding::getField)
					.<Focusable<?>> map(Focusable.class::cast);
		}

		/**
		 * Returns the fields this binder has been bound to which are
		 * focusable, enabled, not read-only, required but empty.
		 * <p>
		 * Usage examples:
		 *
		 * <pre>{@code
		 * binder.getFocusableFields()
		 * 		.emptyRequired()
		 * 		.findFirst()
		 * 		.ifPresent(Focusable::focus);
		 *
		 * binder.getFocusableFields()
		 * 		.emptyRequired()
		 * 		.filter(field -> field != fieldToExcludeFromFocus)
		 * 		.findFirst()
		 * 		.ifPresent(Focusable::focus);
		 * }</pre>
		 */
		public Stream<Focusable<?>> emptyRequired() {
			return AZBinder.this.getBindings().stream()
					.filter(this::isFieldFocusableAndWritable)
					.filter(binding -> binding.isAsRequiredEnabled() && binding.getField().isEmpty())
					.map(Binding::getField)
					.<Focusable<?>> map(Focusable.class::cast);
		}

		private boolean isFieldFocusableAndWritable(final Binding<?, ?> binding) {
			if (!binding.isReadOnly()) {
				final HasValue<?, ?> field = binding.getField();
				if (field instanceof Focusable) {
					if (field instanceof HasEnabled) {
						return ((HasEnabled) field).isEnabled();
					}
					if (field instanceof HasElement) {
						return ((HasElement) field).getElement().isEnabled();
					}
				}
			}
			return false;
		}
	}

	private final Class<BEAN> beanType;
	private final FocusableFields focusableFields = new FocusableFields();
	private BinderFieldConfigurator fieldConfigurator = BinderFieldConfigurator.DEFAULT;
	private boolean trimStrings;

	public AZBinder(final Class<BEAN> beanType) {
		super(beanType);
		this.beanType = beanType;

		// @NotBlank annotation management
		this.setRequiredConfigurator(RequiredFieldConfigurator.DEFAULT.chain((annotation,
				binding) -> annotation.annotationType().equals(NotBlank.class)
						&& RequiredFieldConfiguratorUtil.testConvertedDefaultValue(
								// XXX assuming @NotBlank always applied to String fields
								binding, value -> value == null || value instanceof String && ((String) value).trim().isEmpty())));
	}

	public FocusableFields getFocusableFields() {
		return this.focusableFields;
	}

	public boolean isTrimStrings() {
		return this.trimStrings;
	}

	public void setTrimStrings(final boolean trimStrings) {
		this.trimStrings = trimStrings;
	}

	public Class<BEAN> getBeanType() {
		return this.beanType;
	}

	public BinderFieldConfigurator getFieldConfigurator() {
		return this.fieldConfigurator;
	}

	public void setFieldConfigurator(final @NonNull BinderFieldConfigurator fieldConfigurator) {
		this.fieldConfigurator = Objects.requireNonNull(fieldConfigurator, "Field configurator cannot be null");
	}

	public <TARGET> Binding<BEAN, TARGET> bind(final HasValue<?, TARGET> field, final BeanProperty<? super BEAN, ?, ?, TARGET> property) {
		return this.forField(field).bind(property.getNestedName());
	}

	public <NEWTARGET, TARGET> Binding<BEAN, NEWTARGET> bind(final HasValue<?, TARGET> field, final BeanProperty<? super BEAN, ?, ?, NEWTARGET> property,
			final SerializableFunction<TARGET, NEWTARGET> toModel, final SerializableFunction<NEWTARGET, TARGET> toPresentation) {
		return this.forField(field)
				.withConverter(toModel, toPresentation)
				.bind(property.getNestedName());
	}

	public Binding<BEAN, String> bindString(final HasValue<?, String> field, final BeanProperty<? super BEAN, ?, ?, String> property) {
		return this.bind(field, property,
				s -> s == null || s.isBlank() ? null : s.strip(),
				s -> s == null ? "" : s.strip());
	}

	public <TARGET> Binding<BEAN, TARGET> bindReadOnly(final HasValue<?, TARGET> field, final BeanProperty<? super BEAN, ?, ?, TARGET> property) {
		return super.bindReadOnly(field, property.getNestedName());
	}

	@Override
	public <FIELDVALUE> AZBindingBuilder<BEAN, FIELDVALUE> forField(final HasValue<?, FIELDVALUE> field) {
		return (AZBindingBuilder<BEAN, FIELDVALUE>) super.forField(field);
	}

	public Optional<Binding<BEAN, ?>> getBinding(final BeanProperty<? super BEAN, ?, ?, Target> property) {
		return super.getBinding(property.getName());
	}

	@Override
	protected <FIELDVALUE, TARGET> BindingBuilder<BEAN, TARGET> doCreateBinding(final HasValue<?, FIELDVALUE> field, final Converter<FIELDVALUE, TARGET> converter, final BindingValidationStatusHandler handler) {
		return new AZBindingBuilderImpl<>(this, field, converter, handler);
	}

	@Override
	@SuppressWarnings("unchecked")
	protected BindingBuilder<BEAN, ?> configureBinding(final BindingBuilder<BEAN, ?> binding, final PropertyDefinition<BEAN, ?> definition) {
		// XXX super.configureBinding() finds the actualBeanType and instantiates a BeanValidator using private methods
		// we copied some pieces of code from super.configureBinding() to access the BeanValidator
		// final BindingBuilder<BEAN, ?> binding = super.configureBinding(origBinding, definition);

		if (this.isTrimStrings()
				&& String.class == definition.getType()
				&& binding.getField() instanceof AbstractField
				&& ((AbstractField<?, ?>) binding.getField()).getEmptyValue() instanceof String /* XXX should match TextField, EmailField and TextArea */
				&& !(binding.getField() instanceof PasswordField)) {
			((BindingBuilder<BEAN, String>) binding).withConverter(StringUtils::trimToNull, StringUtils::trimToEmpty);
		}

		final Class<?> actualBeanType;
		if (definition instanceof NestedBeanPropertyDefinition) {
			actualBeanType = ((NestedBeanPropertyDefinition<?, ?>) definition).getParent().getType();
		} else {
			actualBeanType = this.beanType;
		}

		final BeanValidator validator = new BeanValidator(actualBeanType, definition.getTopLevelName());
		this.configureFromConstraintDescriptors(binding, definition, validator);
		return binding.withValidator(validator);
	}

	private void configureFromConstraintDescriptors(final BindingBuilder<BEAN, ?> binding,
			final PropertyDefinition<BEAN, ?> definition, final BeanValidator validator) {

		final Class<?> propertyHolderType = definition.getPropertyHolderType();
		final BeanDescriptor descriptor = validator.getJavaxBeanValidator().getConstraintsForClass(propertyHolderType);
		final PropertyDescriptor propertyDescriptor = descriptor.getConstraintsForProperty(definition.getTopLevelName());
		if (propertyDescriptor == null) {
			return;
		}

		final Set<ConstraintDescriptor<?>> constraintDescriptors = propertyDescriptor.getConstraintDescriptors();
		final RequiredFieldConfigurator requiredConfigurator = this.getRequiredConfigurator();
		if (requiredConfigurator != null && constraintDescriptors.stream()
				.map(ConstraintDescriptor::getAnnotation)
				.anyMatch(constraint -> requiredConfigurator.test(constraint, binding))) {
			binding.getField().setRequiredIndicatorVisible(true);
			this.fieldConfigurator.configureRequired(binding.getField());
		}

		if (this.fieldConfigurator != null) {
			final HasValue<?, ?> field = binding.getField();
			for (final ConstraintDescriptor<?> constraintDescription : constraintDescriptors) {
				this.fieldConfigurator.configureFromAnnotation(field, constraintDescription.getAnnotation());
			}
		}
	}
}
