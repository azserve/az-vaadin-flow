package com.azserve.azframework.vaadinflow.data.binder;

import java.lang.annotation.Annotation;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Future;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import com.azserve.azframework.validation.TemporalRange;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;

public interface BinderFieldConfigurator {

	BinderFieldConfigurator DEFAULT = new BinderFieldConfigurator() {

		@Override
		public void configureFromAnnotation(final HasValue<?, ?> field, final Annotation annotation) {
			final Class<? extends Annotation> type = annotation.annotationType();
			if (Size.class == type) {
				final Size size = (Size) annotation;
				if (field instanceof TextField) {
					if (size.min() > 0) {
						((TextField) field).setMinLength(size.min());
					}
					if (size.max() < Integer.MAX_VALUE) {
						((TextField) field).setMaxLength(size.max());
					}
				} else if (field instanceof TextArea) {
					if (size.min() > 0) {
						((TextArea) field).setMinLength(size.min());
					}
					if (size.max() < Integer.MAX_VALUE) {
						((TextArea) field).setMaxLength(size.max());
					}
				}
			} else if (field instanceof NumberField) {
				this.configureNumberField((NumberField) field, annotation);
			} else if (field instanceof DatePicker) {
				this.configureDatePicker((DatePicker) field, annotation);
			} else if (field instanceof DateTimePicker) {
				this.configureDateTimePicker((DateTimePicker) field, annotation);
			}
		}

		private void configureNumberField(final NumberField field, final Annotation annotation) {
			if (Min.class == annotation.annotationType()) {
				field.setMin(((Min) annotation).value());
			} else if (Max.class == annotation.annotationType()) {
				field.setMax(((Max) annotation).value());
			} else if (DecimalMin.class == annotation.annotationType()) {
				field.setMin(new BigDecimal(((DecimalMin) annotation).value()).doubleValue());
			} else if (DecimalMax.class == annotation.annotationType()) {
				field.setMax(new BigDecimal(((DecimalMax) annotation).value()).doubleValue());
			}
		}

		private void configureDatePicker(final DatePicker field, final Annotation annotation) {
			if (Past.class == annotation.annotationType()) {
				field.setMax(LocalDate.now());
			} else if (Future.class == annotation.annotationType()) {
				field.setMin(LocalDate.now().plusDays(1));
			} else if (TemporalRange.class == annotation.annotationType()) {
				final TemporalRange temporalRange = (TemporalRange) annotation;
				final int back = temporalRange.back();
				final int forward = temporalRange.forward();
				if (back >= 0 || forward >= 0) {
					final ChronoUnit unit = temporalRange.unit();
					if (back >= 0) {
						field.setMin(LocalDateTime.now().minus(back, unit).toLocalDate());
					}
					if (forward >= 0) {
						final LocalDate today = LocalDate.now();
						final LocalDate rangeStart = LocalDateTime.now().plus(forward, unit).toLocalDate();
						field.setMax(rangeStart.isEqual(today) ? rangeStart.plusDays(1) : rangeStart);
					}
				}
			}
		}

		private void configureDateTimePicker(final DateTimePicker field, final Annotation annotation) {
			if (Past.class == annotation.annotationType()) {
				field.setMax(LocalDateTime.now().minusSeconds(1).withNano(0));
			} else if (Future.class == annotation.annotationType()) {
				field.setMin(LocalDateTime.now().plusSeconds(1).withNano(0));
			} else if (TemporalRange.class == annotation.annotationType()) {
				final TemporalRange temporalRange = (TemporalRange) annotation;
				final int back = temporalRange.back();
				final int forward = temporalRange.forward();
				if (back >= 0 || forward >= 0) {
					final ChronoUnit unit = temporalRange.unit();
					if (back >= 0) {
						field.setMin(LocalDateTime.now().minus(back, unit));
					}
					if (forward >= 0) {
						field.setMax(LocalDateTime.now().plus(forward, unit));
					}
				}
			}
		}

		@Override
		public void configureRequired(final HasValue<?, ?> field) {
			if (field instanceof Select) {
				((Select<?>) field).setEmptySelectionAllowed(false);
			}
		}
	};

	void configureFromAnnotation(HasValue<?, ?> field, Annotation annotation);

	void configureRequired(HasValue<?, ?> field);
}
