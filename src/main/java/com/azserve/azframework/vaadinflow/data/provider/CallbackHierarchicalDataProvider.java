package com.azserve.azframework.vaadinflow.data.provider;

import java.io.Serializable;
import java.util.stream.Stream;

import com.vaadin.flow.data.provider.hierarchy.AbstractHierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery;
import com.vaadin.flow.function.SerializableFunction;
import com.vaadin.flow.function.SerializablePredicate;

public class CallbackHierarchicalDataProvider<T, F> extends AbstractHierarchicalDataProvider<T, F> {

	private static final long serialVersionUID = 3110659373337525442L;

	private final boolean inMemory;
	private final ChildrenCountCallback<T, F> childrenCountCallback;
	private final SerializableFunction<HierarchicalQuery<T, F>, Stream<T>> childrenFetchCallback;
	private final SerializablePredicate<T> hasChildrenCallback;

	public CallbackHierarchicalDataProvider(final boolean inMemory, final ChildrenCountCallback<T, F> childrenCountCallback, final SerializableFunction<HierarchicalQuery<T, F>, Stream<T>> childrenFetchCallback, final SerializablePredicate<T> hasChildrenCallback) {
		this.inMemory = inMemory;
		this.childrenCountCallback = childrenCountCallback;
		this.childrenFetchCallback = childrenFetchCallback;
		this.hasChildrenCallback = hasChildrenCallback;
	}

	@Override
	public int getChildCount(final HierarchicalQuery<T, F> query) {
		return this.childrenCountCallback.countChildren(query);
	}

	@Override
	public Stream<T> fetchChildren(final HierarchicalQuery<T, F> query) {
		return this.childrenFetchCallback.apply(query);
	}

	@Override
	public boolean hasChildren(final T item) {
		return this.hasChildrenCallback.test(item);
	}

	@Override
	public boolean isInMemory() {
		return this.inMemory;
	}

	public interface ChildrenCountCallback<T, F> extends Serializable {

		int countChildren(HierarchicalQuery<T, F> query);

	}
}
