package com.azserve.azframework.vaadinflow.data.provider;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery;

public final class DataProviderUtils {

	public static <T> Stream<T> fetchAll(final DataProvider<T, ?> dataProvider) {
		if (dataProvider instanceof HierarchicalDataProvider) {
			return fetchAll((HierarchicalDataProvider<T, ?>) dataProvider);
		}
		return dataProvider.fetch(new Query<>());
	}

	public static <T> Stream<T> fetchAll(final HierarchicalDataProvider<T, ?> dataProvider) {
		return fetchAllDescendants(null, dataProvider);
	}

	public static <T> Stream<T> fetchAllDescendants(final T parent, final HierarchicalDataProvider<T, ?> dataProvider) {
		if (parent != null && !dataProvider.hasChildren(parent)) {
			return Stream.empty();
		}
		final List<T> children = dataProvider
				.fetchChildren(new HierarchicalQuery<>(null, parent))
				.collect(Collectors.toList());
		if (children.isEmpty()) {
			return Stream.empty();
		}
		return children.stream().flatMap(child -> Stream.<T> concat(Stream.of(child), fetchAllDescendants(child, dataProvider)));
	}

	private DataProviderUtils() {
		throw new AssertionError();
	}
}
