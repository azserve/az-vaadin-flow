package com.azserve.azframework.vaadinflow.theme.lumo;

public enum LumoMediaSize {

	/** Small size: 640px */
	SMALL("sm", 640),
	/** Medium size: 768px */
	MEDIUM("md", 768),
	/** Large size: 1024px */
	LARGE("lg", 1024),
	/** Extra-Large size: 1280px */
	XLARGE("xl", 1280),
	/** Extra-Extra-Large size: 1536px */
	XXLARGE("2xl", 1536);

	private final String prefix;
	private final int sizeInPixels;

	LumoMediaSize(final String prefix, final int sizeInPixels) {
		this.prefix = prefix;
		this.sizeInPixels = sizeInPixels;
	}

	public int sizeInPixels() {
		return this.sizeInPixels;
	}

	public String sizeString() {
		return this.sizeInPixels + "px";
	}

	public String prefix() {
		return this.prefix;
	}
}
